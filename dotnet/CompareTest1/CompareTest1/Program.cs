

namespace CompareTest1
{

class C1 {
    readonly string _s;
    public C1(string s) { _s = s; }

    // デフォルト `==` operator は、同一性で判定. `ReferenceEquals()`
    // デフォルト `Equals()` も, 同一性で判定。
}

readonly struct S1 {
    public readonly string _s;
    public S1(string s) { _s = s; }

    // 構造体はデフォルト `==` operator が定義されない
    // デフォルト `Equals()` は定義されている. すべてのプロパティが `Equals()` で判定して同じか.
}

// デフォルト `==` operator は, すべてのプロパティが `Equals()` で判定して同じか.
record Baz(string s);


// Equals() だけを実装
class C2 {
    public readonly string _s;
    public C2(string s) { _s = s; }

    // `Equals()` を定義しても, `operator ==` で使われるわけではない(!)
    public override bool Equals(object? obj) {
        if (obj == null) return false;
        if (obj is C2 otherC) return _s.Equals(otherC._s);
        if (obj is S1 otherS) return _s.Equals(otherS._s);
        throw new InvalidCastException(nameof(obj));
    }

    // `Equals()` をオーバライドするときは, 必ず GetHashCode() も必要 (MUST)
    // `Equals()` だけだと, Dictionary<> の key として使えない
    public override int GetHashCode()
    {
        return _s.GetHashCode();
    }
}


// SortedSet<>: IComparable の実装が必要 or SortedSet<> コンストラクタに IComparer<T> オブジェクトを渡す
class C3 : IComparable<C3> {
    public readonly string _s;
    public C3(string s) { _s = s; }

    // @override IComparable<C3>
    public int CompareTo(C3? other)
    {
        if (other == null) return 1; // C# ルール上, null は例外ではなく "最小"
        return _s.CompareTo(other._s);
    }

    // 比較演算子は、デフォルトでは定義されない
    public static bool operator <(C3 left, C3 right) { return left.CompareTo(right) < 0; }
    public static bool operator >(C3 left, C3 right) { return left.CompareTo(right) > 0; }
    public static bool operator <=(C3 left, C3 right) { return left.CompareTo(right) <= 0; }
    public static bool operator >=(C3 left, C3 right) { return left.CompareTo(right) >= 0; }
}


// IEquatable<> だけ実装
class C4 : IEquatable<C4> {
    public readonly string _s;
    public C4(string s) { _s = s; }

    // この場合も、別に非ジェネリック版も必要
    public override bool Equals(object? obj) {
        if (obj is C4) throw new ArgumentException("internal error");
        return Equals(obj as C4);
    }

    // @override IEquatable<C4>
    public bool Equals(C4? other) {
        if (other == null) return false;
        return _s.Equals(other._s);
    }

    public override int GetHashCode() {
        return _s.GetHashCode();
    }
}


internal class Program
{
    static void test1() {
        // 内容が同じ
        C1 a = new C1("a"), b = new C1("a");
        S1 x = new S1("a"), y = new S1("a");
        Baz s = new("a"), t = new("a");

        //=> a == b: False, a.Equals(b): False, x.Equals(y): True, a == boxed x: False, s == t: True
        Console.WriteLine($"a == b: {a == b}, a.Equals(b): {a.Equals(b)}, x.Equals(y): {x.Equals(y)}, a == boxed x: {a == (object) x}, s == t: {s == t}");
    }

    static void test2() {
        C2 a = new C2("a"), b = new C2("a");
        S1 x = new S1("a"), y = new S1("a");

        // a == b が False のママなのに注意。
        //=> a == b: False, a.Equals(b): True, a == boxed x: False, a.Equals(x): True
        Console.WriteLine($"a == b: {a == b}, a.Equals(b): {a.Equals(b)}, a == boxed x: {a == (object) x}, a.Equals(x): {a.Equals(x)}");
    }

    static void test3() {
        Dictionary<C2, string> d = new Dictionary<C2, string>();
        HashSet<C2> h = new HashSet<C2>();
        SortedSet<C3> s = new SortedSet<C3>();

        d.Add(new C2("a"), "a1"); 
        if (!d.ContainsKey(new C2("a"))) d.Add(new C2("a"), "a2"); // 重複のときは例外
        h.Add(new C2("a"));       h.Add(new C2("a")); // 戻り値 = true で追加された
        s.Add(new C3("a"));       s.Add(new C3("a")); // 戻り値 = true で追加された

        foreach (var kv in d) Console.WriteLine($"d: k = {kv.Key._s}, v = {kv.Value}");    
        foreach (var v in h) Console.WriteLine($"h: v = {v}");
        foreach (var v in s) Console.WriteLine($"s: v = {v}");

        C3 a = new C3("a"), b = new C3("a");
        // Equals() は CompareTo() の結果を見ていない(!)
        //=> a == b: False, a.Equals(b): False, a <= b: True          
        Console.WriteLine($"a == b: {a == b}, a.Equals(b): {a.Equals(b)}, a <= b: {a <= b}");
    }


    static void test4() {
        string sa = "a", sb = null, sc = "a";
        // string `operator <` デフォルト実装がない
        // sb.CompareTo(sa) はヌルポ。そりゃそうだ。
        // `string#==` は同一性。`CompareTo()` は null が最小.
        // sa == sb: False, sb == sa: False, sa.CompareTo(sb): 1  
        Console.WriteLine($"sa == sb: {sa == sb}, sb == sa: {sb == sa}, sa.CompareTo(sb): {sa.CompareTo(sb)}");

        //=> 0, 1, -1, 0    同値性, null は最小, 左辺が null も同じ. null 同士は同値.
        Console.WriteLine($"{String.Compare(sa, sc)}, {String.Compare(sa, sb)}, {String.Compare(sb, sa)}, {String.Compare(sb, sb)}");
    }


    static void test_iequatable() {
        C4 a = new C4("a"), aa = new C4("a");
        C1 x = new C1("a");

        // 違う型を渡したときも無限ループしないか?  OK
        //=> True, False
        Console.WriteLine($"{a.Equals(aa)}, {a.Equals(x)}");
    }


    static void Main(string[] args) {
        test1();
        test2();
        test3();
        test4();
        test_iequatable();
    }
}
}
