
# 同値性, 比較可能性


## 同一性・同値性

C# class では, `operator ==` と `Equals()` が独立。デフォルト実装は, 両方とも同一性の判定.

実装側:
 - 同値の比較にしたい場合, `Equals()` と `GetHashCode()` の両方をオーバライドすること (MUST)。
   後者をオーバライドしなくてもコンパイルエラーにならないが、`Dictionary<>` などが正常に動かない.

 - `operator ==` は、勝手に `Equals()` を利用してくれはしない。陽にオーバライドしないと、挙動が変わらない。
   ただ, `operator ==` を override すると, null チェックをいちいち `ReferenceEquals()` と書かないといけなく、大変。
   `==` は同一性の比較と割り切って、デフォルト実装のままにするのがいい。
 
 - 非ジェネリックな `Object#Equals()` に加えて, 必ず `IEquatable<>` インタフェイスを実装すること。パフォーマンスが大きく異なる。
   See <a href="https://qiita.com/cactuaroid/items/f277a097ecf51eb247c0">IEquatableを完全に理解する</a>

 - `IEquatable<>` インタフェイスは `Equals()` メソッドしかない。惑わされず, `GetHashCode()` もオーバライドすること (MUST)。
 
 

利用側:
 - 値が一致か調べるときは, `operator ==` ではなく, `Equals()` を使うこと.

 - デフォルト比較以外の条件にしたい場合は, `IEqualityComparer<T>` インタフェイスを実装する比較オブジェクトをつくればよい。
   こちらは `Equals()` と `GetHashCode()` メンバを持つ.
   `Dictionary<TKey,TValue>`, `HashSet<T>` オブジェクトに渡すことができる。
 
 

## 順序づけ性

C# の `null` は3値論理ではない。`null` が最小値になるように返す。`null == null` は `True`.

実装側:
 - `SortedSet<>` を使う場合は, オブジェクトの順序付けが必要。

 - 特定フィールドだけでソートしたいなど、オブジェクトの順序と意味付けが異なる場合などは, 
   `IComparer<T>` インタフェイスを実装し, コンテナクラスにわたせばよい

 - オブジェクトに大小関係を持たせるクラスは, `IComparable<T>` インタフェイスの実装が必要. `CompareTo()` のオーバライド。
 
 - `operator <` などを自動生成してくれない。何なんだ。こちらも陽にオーバライド必要。
 
 - デフォルト `Equals()` 実装も、勝手に `CompareTo()` の結果を利用してくれるわけではない。

 - 挙動が矛盾しないように, `IEquatable<T>` インタフェイス, `Equals()` も (ともちろん `GetHashCode()`も) 陽にオーバライドすること。


 
利用側:
 - デフォルト比較以外にしたい場合は, 比較オブジェクト `IComparer<T>` を渡すようにする。`Compare()` メソッド。
 
 
