
// 一般化していく

// class のデフォルト実装 
//   `operator ==` は同一性, `Equals()` も同一性.
// 同値性がほしいのは Dictionary<>, HashSet<> などの key にしたい場合が多い.
//   この場合は, 順序付けは不要.

// `operator ==` の再定義は、ついうっかり再帰呼び出しになってしまうので注意。
//  -> むしろ, 再定義しないと割り切ったほうがいい


namespace comparer
{

// 同値性
//  - `IEqualityComparer<T>` インタフェイスを実装し, コンテナクラスにわたすことで
//    都度切り替えられるようにできる
//  - デフォルト実装を override する場合は, IEquatable<T> インタフェイスを実装
//    同値性を都度切り替えたいケースはあまりなさそう。基本はこちら。
//  - `operator ==` も変更したい場合は, 陽に override 必要.

partial class Hoge : IEquatable<Hoge> {
    int    _id;
    string _s;

    public Hoge(int id, string s) {
        this._id = id;
        this._s = s;
    }

    // abstract class にしても, インタフェイスメンバの実装が必須。実装省略できない
    // @override IEquatable<Hoge>
    public bool Equals(Hoge? other) {
        if (ReferenceEquals(other, null)) return false;
        // デフォルト実装は, `id` だけ、みたいにしないほうがよい
        return _id == other._id && _s == other._s;
    }

    // これも実装必須
    public override int GetHashCode() { return _id.GetHashCode(); }
}


// ボイラープレート
// .NET 7 から `IEqualityOperators<>` インタフェイスができたが、不要か
partial class Hoge : IEquatable<Hoge>
{
    public override bool Equals(object? other) {
        if (other is Hoge) throw new ArgumentException("internal error");
        return Equals(other as Hoge); // `as`: Hoge はクラスでなければならない
    }

    // 仮引数から `?` を取っても、実際には呼び出される.
    public static bool operator == (Hoge? left, Hoge? right) {
        if ( ReferenceEquals(left, null) ) // スタックオーバフローしないように
            return ReferenceEquals(right, null) ? true : false; // C# は3値論理ではない
        return left.Equals(right) ;
    }

    // `==` から自動生成してくれない
    public static bool operator != (Hoge? left, Hoge? right) {
        return !(left == right);
    }
}


// 順序づけ
//  - ソートしたい場合、都度切り替えたい場合も多そう
//    IComparer<T> インタフェイスを実装し, コンテナクラスにわたす
//  - オブジェクトに順序付けを付けるには, IComparable<T> インスタフェイスを実装する
//  - この場合, 挙動が矛盾しないように, IEquatable<T> も実装しろ


// つど自由に決められる。例えば, ここでは `id` だけで比較
class FugaComparer : IComparer<Fuga>
{
    // @override IComparer<Fuga>
    public int Compare(Fuga? x, Fuga? y) {
        if (x == null)
            return y == null ? 0 : -1;
        return y == null ? 1 : (x.Id - y.Id);
    }
}


partial class Fuga : IComparable<Fuga>, IEquatable<Fuga>
{
    public int Id { get; protected set; }
    string _s;

    public Fuga(int id, string s) {
        this.Id = id;
        this._s = s;
    }

    // @override IComparable<Fuga>
    public int CompareTo(Fuga? other) {
        if (ReferenceEquals(other, null)) return 1;  // C# は3値論理ではない

        if (Id != other.Id) return Id - other.Id;
        return String.Compare(_s, other._s);
    }

    // これも実装必要. 忘れるな
    public override int GetHashCode() { return HashCode.Combine(Id, _s); }
}


// ボイラープレート
partial class Fuga : IComparable<Fuga>, IEquatable<Fuga>
{
    // @override IEquatable<Fuga>
    public bool Equals(Fuga? other) {
        return CompareTo(other) == 0;
    }

    public override bool Equals(object? other) {
        if (other is Fuga) throw new ArgumentException("internal error");
        return Equals(other as Fuga); // `as`: Hoge はクラスでなければならない
    }

    public static bool operator == (Fuga? left, Fuga? right) {
        return Compare(left, right) == 0;
    }

    // 自動生成してくれない
    public static bool operator != (Fuga? left, Fuga? right) {
        return !(left == right);
    }

    // left が null のケースがある
    public static int Compare(Fuga? left, Fuga? right) {
        if ( ReferenceEquals(left, null) ) // スタックオーバフローしないように
            return ReferenceEquals(right, null) ? 0 : -1;
        return left.CompareTo(right);
    }

    // 比較演算子は、デフォルトでは定義されない
    public static bool operator <(Fuga? left, Fuga? right) { return Compare(left, right) < 0; }
    public static bool operator >(Fuga? left, Fuga? right) { return Compare(left, right) > 0; }
    public static bool operator <=(Fuga? left, Fuga? right) { return Compare(left, right) <= 0; }
    public static bool operator >=(Fuga? left, Fuga? right) { return Compare(left, right) >= 0; }
}


internal class Program
{
    // 汎用版が動くか?
    static void test5() {
        Hoge a = new Hoge(1, "a"), aa = new Hoge(1, "a"),
             b = new Hoge(2, "b"), c = null;

        //=> True, True, False, False, False, False
        Console.WriteLine($"{a.Equals(aa)}, {a == aa}, {a != aa}, {a == b}, {a == c}, {c == a}");
    }

    static void test6() {
        Fuga a = new Fuga(1, "a"), aa = new Fuga(1, "a"),
             b = new Fuga(2, "b"), c = null;

        //=> True, True, False, True, False, True
        Console.WriteLine($"{a.Equals(aa)}, {a == aa}, {a < aa}, {a < b}, {a < c}, {c < a}");
    }

    static void Main(string[] args)
    {
        test5();
        test6();
    }
}

}
