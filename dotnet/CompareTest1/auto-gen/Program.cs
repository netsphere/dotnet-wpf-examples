using ComparableGenerator;

// See https://zenn.dev/nuits_jp/articles/comparable-generator-release-1-2-0

namespace auto_gen
{

// partial 修飾が必要
[Comparable]
partial class Employee {
    [CompareBy(Priority = 2)]
    public string FirstName { get; set; }

    [CompareBy(Priority = 1)]
    public string LastName { get; set; }
}


internal class Program
{
    static void Main(string[] args)
    {
        Employee employee1 = new Employee() {LastName = "Tanaka", FirstName = "Taro"};
        Employee employee2 = new Employee() {LastName = "Tanaka", FirstName = "Taro"};
        Employee e_null = null;

        //=> False, True   `==` の自動生成ではない
        Console.WriteLine($"{employee1 == employee2}, {employee1 != employee2}");

        //=> True
        Console.WriteLine($"{e_null == e_null}");

        // `operator <` などの自動生成ではない。`CompareTo()` を生成
        //=> 0, 1
        Console.WriteLine($"{employee1.CompareTo(employee2)}, {employee1.CompareTo(e_null)}");
    }
}

}
