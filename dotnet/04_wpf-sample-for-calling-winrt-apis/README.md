
# WPF Desktop アプリから UWP アプリ用の API "Windows Runtime APIs" (WinRT APIs) を呼び出す

Forked from https://github.com/biac/codezine/tree/master/UwpForefront/UF02/

解説記事: [超簡単! WPFなどの.NETのアプリからUWPのAPIを使う ～日本語の読み仮名を取得するAPIを題材に](https://codezine.jp/article/detail/10654)




## NuGet パッケージ

現代は, Microsoft.Windows.SDK.Contracts パッケージを使う。


## Package identity

多くの WinRT API は package identity が必要。 -- 例えば, `Windows.Storage` 名前空間など。
デスクトップアプリでこれらの API を使うには, MSIX package にパッケージするか, *sparse* MSIX package を作る。

例えば, [Set up your desktop application for MSIX packaging in Visual Studio](https://docs.microsoft.com/en-us/windows/msix/desktop/desktop-to-uwp-packaging-dot-net)




## WinRT APIs

このサンプルで使っているもの

 - `Windows.Globalization.JapanesePhoneticAnalyzer` class (introduced in 10.0.10240.0)

 - `Windows.Data.Text.TextReverseConversionGenerator` class (introduced in 10.0.10240.0 - for Xbox, see UWP features that aren't yet supported on Xbox)

 - `Windows.Data.Text.TextConversionGenerator` class (introduced in 10.0.10240.0 - for Xbox, see UWP features that aren't yet supported on Xbox)

 - `Windows.Devices.Geolocation.Geolocator` class (introduced in 10.0.10240.0)


このサンプルでは使っていないが、押さえておきたい

 - `Windows.UI.Xaml.Controls.InkCanvas` class

 - 適応性のあるインタラクティブなトースト通知
 




## UWP の登場と新しい Windows OS 実行環境

Windows Runtime (WinRT) API は、Windows ストアアプリ～ UWP アプリのためのAPI.「Windows の API」という言い方は、既存のデスクトップアプリ向けと誤認しやすい。

UWP アプリ向けとデスクトップアプリ向けは、Windows API が別物になっている。一番基礎となる Win32 API で, `WINAPI_PARTITION_DESKTOP` マクロ (デスクトップアプリのみ) か `WINAPI_PARTITION_APP` マクロ (デスクトップアプリと UWP 両方から呼び出せる) で, すべてのAPIが区別されている。

例えば, `C:\Program Files\Microsoft Visual Studio\2022\Community\SDK\ScopeCppSDK\vc15\SDK\include\um\fileapi.h` で, `CreateFile2()` は両用だが `CreateFile()` は [desktop apps only] になっている。
マクロは `c:/Program Files/Microsoft Visual Studio/2022/Community/SDK/ScopeCppSDK/vc15/SDK/include/shared/winapifamily.h`

滅多にないが、開発者が UWP かどうかで処理を分けるには、次のようにする。

```c++
#if !WINDOWS_UWP
    // Win32/Desktop code, including Windows App SDK code
#else
    // UWP code
#endif
```

または, 次のようにする。`WINAPI_FAMILY_PC_APP` と `WINAPI_FAMILY_PHONE_APP` があるので、ターゲットがデスクトップか、で判定する.

```c++
#if (WINAPI_FAMILY == WINAPI_FAMILY_DESKTOP_APP)
    // Win32/Desktop code, including Windows App SDK code
#else
    // UWP code
#endif
```


実行環境も別物で、UWP アプリは AppContainer サンドボックス環境で動作していた。



## C#

開発する C# 言語は共通でも、その上のライブラリは, デスクトップ向けが .NET Framework + WPF (or WinForms), UWP 向けが WinRT API と Windows UI Library (WinUI) だった。

2015年に Windows ストアアプリがリニューアルして UWP が登場したときは、無責任な記者や有象無象がこれからは UWP が未来だと喧伝したが、商業的には大失敗だったのは全員が知っている。

UWP 向けだけの機能があったので、Windows 10 v1803から, UWP の discontinue に向けて、非UWP アプリからも WinRT API を呼び出せるようになった。
非UWP 実行環境でも UWP クラスライブラリの一部を動作可能にしたのだろう。
.NET Framework または .NET Core 3.x までは, 追加 NuGet パッケージが必要だった. 
  `<PackageReference Include="Microsoft.Windows.SDK.Contracts">`

これが, ターゲットフレームワーク ".NET 6.0" 以降, ターゲット OS バージョンを 10.0.17763.0 (v1809) 以降だと、何もしなくても WinRT API が呼び出せるようになった。
.NET 5.0 の破壊的変更 (CoreCLR の変更) で、`Microsoft.Windows.SDK.Contracts` パッケージへの参照は削除しなければならない。<code>.winmd</code> ファイルを定義するライブラリを使う場合は, 代わりに, `Microsoft.Windows.CsWinRT` パッケージをインストールする。これが C#/WinRT projection を生成する。


