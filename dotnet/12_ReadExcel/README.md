
# Excel ファイルを読み込む

ライブラリには、次のものがある。意外とたくさんある

 - ClosedXML - .NET library for reading, manipulating and writing Excel 2007+ (.xlsx, .xlsm) files.
    + Excel 2007+ (`.xlsx` ファイル) のみ. `.xls` ファイルを開こうとすると例外.
    + パッケージ: ClosedXML   netstandard2.1
    + ライセンス: MIT license
    + 解説記事  例えば <a href="https://atmarkit.itmedia.co.jp/ait/articles/1810/24/news016.html">ExcelなしでExcelファイルを操作するには？（ClosedXML編）［.NET 4.0、C#／VB］</a>
     
 - <i>Open XML SDK for Office</i> - tools for working with Office Word, Excel, and PowerPoint documents.
    +  Microsoft 製. ドキュメントの作成、修正、検索と置換など。
    + パッケージ: DocumentFormat.OpenXml   .NET 6.0, .NET Framework
    + ライセンス: MIT license
    + 解説記事   例えば <a href="https://atmarkit.itmedia.co.jp/ait/articles/1810/17/news022.html">ExcelなしでExcelファイルを操作するには？（純正SDK編）［.NET 3.5、C#／VB］</a>

 - NPOI - a .NET library that can read/write Office formats without Microsoft Office installed. No COM+, no interop.
    + 昔からあるライブラリ。Excel 2003/2007 files (`.xls` ファイルも対応) の読み書きが可能。Word 2003/2007 (`.doc`, `.docx`)
    + パッケージ: NPOI   .NET 6.0, .NET Framework 4.7.2
    + ライセンス: Apache-2.0

 - SpreadsheetLight
    + 最後のリリースが 2020年11月。開発が低迷か。
    + ライセンス: MIT
    
 - EPPlus - Excel spreadsheets for .NET
    + ライセンス: ソースコードは公開されているが、商用.

 - ExcelDataReader - reading Microsoft Excel files (2.0-2021, 365).
    + CSVファイルと Excel 2.0以降のフォーマットに対応。
    + ライセンス: MIT



## ClosedXML


`new XLWorkbook(dlg.FileName)` でファイルを読み込む

`XLWorkbook#Worksheets` プロパティにシートの一覧


