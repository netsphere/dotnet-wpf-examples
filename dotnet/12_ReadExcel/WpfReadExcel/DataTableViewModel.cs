using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;


namespace WpfReadExcel
{

internal class DataTableViewModel : MVVM.BindableBase
{
    // コンストラクタ
    public DataTableViewModel()
    {
        _dataTable = new DataTable();
        _sheets = new ObservableCollection<IXLWorksheet>();
    }

    // コンボボックスに bind
    private ObservableCollection<IXLWorksheet> _sheets;
    public ObservableCollection<IXLWorksheet> Sheets {
        get => _sheets;
        set {
            SetPropertyAndRaise(ref _sheets, value);
        }
    }

    // Point! こちらは bind できない。
    //   IBindingList, IList または IEnumerable のいずれも実装していないため.  
    private readonly DataTable _dataTable;

    // DataGrid.SelectedItem プロパティに bind する
    private DataRowView _selectedRow;
    public DataRowView SelectedRow {
        get => _selectedRow;
        set {
            SetPropertyAndRaise(ref _selectedRow, value);
        }
    }

    // こちらを <DataGrid> に bind する.
    // `DataView` 型は `IBindingList` インタフェイスを実装しているため, 暗黙に
    // `BindingListCollectionView` で wrap される
    public DataView DataTableView => new DataView(_dataTable);

    public void NotifyTableUpdate()
    {
        // ここは DataView のほう. DataView を bind しているため。
        // 名前も明示的に指定.
        RaisePropertyChanged(nameof(DataTableView));
    }


    ////////////////////////////////////////////////////////////
    // Command Handlers

}

}
