using DocumentFormat.OpenXml.Office2010.CustomUI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace WpfReadExcel
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
public partial class MyApp : Application
{
    private void Application_Startup(object sender, StartupEventArgs e)
    {
        var wnd = new MainWindow();

        // Menu
        wnd.CommandBindings.Add(
                new CommandBinding(ApplicationCommands.Close, OnFileExit));

        wnd.Show();
    }

    // File -> 終了
    private void OnFileExit(object sender, ExecutedRoutedEventArgs e)
    {
        // TODO: ここで、保存しますか? などを出す
        Application.Current.Shutdown();
    }
}
}
