using ClosedXML.Excel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;


namespace WpfReadExcel
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
public partial class MainWindow : Window
{
    // フルパス
    string? _fileName;

    XLWorkbook? workbook;

    public MainWindow()
    {
        InitializeComponent();
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
        var dlg = new OpenFileDialog();
        // Message=Extension 'xls' is not supported. Supported extensions are '.xlsx', '.xlsm', '.xltx' and '.xltm'.
        //dlg.Filter = "Excelファイル (*.xls;*.xlsx)|*.xls;*.xlsx";
        dlg.Filter = "Excelファイル (*.xlsx)|*.xlsx";
        if (dlg.ShowDialog() == true) {
            workbook = new XLWorkbook(dlg.FileName);
            _fileName = dlg.FileName;

            updateSheetsCombo();
        }
    }

    void updateSheetsCombo()
    {
        var vm = (DataTableViewModel) DataContext;
        vm.Sheets.Clear();

        foreach(var s in workbook.Worksheets)
            vm.Sheets.Add(s);

        cbSheet.SelectedItem = vm.Sheets[0];
    }

    // コンボボックスの選択変更
    private void cbSheet_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        var vm = (DataTableViewModel) DataContext;

        // カラムを動的に変更する
        var dataTable = vm.DataTableView.Table;
        dataTable.Columns.Clear();
        dataTable.Rows.Clear();  // これも必要!

        var sheet = (IXLWorksheet) cbSheet.SelectedItem;
        if (sheet == null) 
            return;

        foreach(var wk_c in sheet.Columns() ) {
            // 列番号, 行番号とも 1 始まり
            var col_name = wk_c.Cell(1).Value.ToString();
            var col = new DataColumn { 
                        ColumnName = "col" + dataTable.Columns.Count + 1,
                        Caption = col_name };
            dataTable.Columns.Add( col );
        } 
        //dataTable.Columns.Add("foo");
        //dataTable.Columns.Add("hage");
        foreach (IXLRow wk_r in sheet.Rows() ) {
            var row = dataTable.NewRow();
            for (int i = 0; i < wk_r.Cells().Count(); ++i)
                row[i] = wk_r.Cell(i + 1).Value.ToString();  // 1始まり              
            dataTable.Rows.Add(row);
        }
        vm.NotifyTableUpdate(); // これが必要
    }

    // ColumnName はユニークでなければならない。ヘッダの表示は Caption を使う
    private void dgTable_AutoGeneratingColumn(object sender, 
                                        DataGridAutoGeneratingColumnEventArgs e)
    {
        // DataGridTextColumn 型など.
        var column = (DataGridBoundColumn) e.Column;
        var view = (DataView) ((DataGrid) sender).ItemsSource;

        var dataColumn = view.Table.Columns.OfType<DataColumn>()
                                    .FirstOrDefault((c) => c.ColumnName == e.PropertyName);
        if (dataColumn != null) 
            column.Header = dataColumn.Caption;
    }
}

}
