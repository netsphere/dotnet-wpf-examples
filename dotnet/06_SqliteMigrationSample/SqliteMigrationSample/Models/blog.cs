using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqliteMigrationSample.Models
{

// Code-First: このクラスは手書きする。
[Table("blogs")]
public class Blog
{
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    [Required]
    public string Url { get; set; }

    // Get のみ
    public ICollection<Post> Posts { get; }
}

[Table("posts")]
public class Post
{
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    [Required]
    public string Title { get; set; }

    [Required(AllowEmptyStrings = true)]
    public string Content { get; set; }

    [Column("blog_id"), Required]
    public int BlogId { get; set; }

    public Blog Blog { get; set; }
}

}
