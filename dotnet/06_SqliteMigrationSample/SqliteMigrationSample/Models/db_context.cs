
using Microsoft.EntityFrameworkCore;


namespace SqliteMigrationSample.Models
{

class MyDbContext: DbContext
{
    // ここでファイル名を指定.
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite(@"Data Source=blogging.sqlite");
    }

    public DbSet<Blog> Blogs { get; set; }
    public DbSet<Post> Posts { get; set; }
}

}
