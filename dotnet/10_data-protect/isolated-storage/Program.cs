﻿
// Data Protection API (DPAPI

using System.IO.IsolatedStorage;
using System.Reflection;


namespace data_protect
{

internal class Program
{
    private static
    string isolatedPath(IsolatedStorageFile isolated)
    {
        var type = typeof(IsolatedStorageFile);
        var prop = type.GetProperty("RootDirectory",
                               BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetProperty);
        // C#: "??" null 合体演算子は, null でないときは左辺、null のときは右辺を返す
        return prop ?.GetValue(isolated) ?.ToString() ?? "null";
    }

    private static
    string testWriting(IsolatedStorageFile isolated)
    {
        using (var outputStream = new IsolatedStorageFileStream("text_file.txt",
                                            FileMode.Create, FileAccess.Write, isolated)) {
            using (StreamWriter writer = new StreamWriter(outputStream)) {
                writer.WriteLine("I have a pen. I have an apple. Apple pen! I have a pen. I have a pineapple. Pineapple pen! Apple-pen... pineapple-pen... Pen-Pineapple-Apple-Pen!");
            }
        }
        return isolatedPath(isolated);
    }

    private static
    void testIsolatedStorage()
    {
        // 順に試す
        IsolatedStorageFile file;
        try {
            file = IsolatedStorageFile.GetMachineStoreForApplication();
            Console.WriteLine("MachineStoreForApplication:" + testWriting(file) + "\r\n");
        } catch { }
        try {
            file = IsolatedStorageFile.GetMachineStoreForAssembly();
            Console.WriteLine("MachineStoreForAssembly:" + testWriting(file) + "\r\n");
        } catch { }
        try {
            file = IsolatedStorageFile.GetMachineStoreForDomain();
            Console.WriteLine("MachineStoreForDomain:" + testWriting(file) + "\r\n");
        } catch { }

        try {
            file = IsolatedStorageFile.GetUserStoreForApplication();
            Console.WriteLine("UserStoreForApplication:" + testWriting(file) + "\r\n");
        } catch { }
        try {
            file = IsolatedStorageFile.GetUserStoreForAssembly();
            Console.WriteLine("UserStoreForAssembly:" + testWriting(file) + "\r\n");
        } catch { }
        try {
            file = IsolatedStorageFile.GetUserStoreForDomain();
            Console.WriteLine("UserStoreForDomain:" + testWriting(file) + "\r\n");
        } catch { }
        try {
            file = IsolatedStorageFile.GetUserStoreForSite();
            Console.WriteLine("UserStoreForSite:" + testWriting(file) + "\r\n");
        } catch { }
    }


    static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");

        //        IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication();
        testIsolatedStorage();
    }
}

}
