﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace UwpIsolatedStorage1
{
    /// <summary>
    /// それ自体で使用できる空白ページまたはフレーム内に移動できる空白ページ。
    /// </summary>
public sealed partial class MainPage : Page
{
    public MainPage()
    {
        this.InitializeComponent();
    }

    private static
    string isolatedPath(IsolatedStorageFile isolated)
    {
        var type = typeof(IsolatedStorageFile);
        var prop = type.GetProperty("RootDirectory",
                               BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetProperty);
        // C#: "??" null 合体演算子は, null でないときは左辺、null のときは右辺を返す
        return prop ?.GetValue(isolated) ?.ToString() ?? "null";
    }

    private static
    string testWriting(IsolatedStorageFile isolated)
    {
        using (var outputStream = new IsolatedStorageFileStream("text_file.txt",
                                            FileMode.Create, FileAccess.Write, isolated)) {
            using (StreamWriter writer = new StreamWriter(outputStream)) {
                writer.WriteLine("I have a pen. I have an apple. Apple pen! I have a pen. I have a pineapple. Pineapple pen! Apple-pen... pineapple-pen... Pen-Pineapple-Apple-Pen!");
            }
        }
        return isolatedPath(isolated);
    }

    private void Page_Loaded(object sender, RoutedEventArgs e)
    {
        // 順に試す
        IsolatedStorageFile file;
        try {
            file = IsolatedStorageFile.GetMachineStoreForApplication();
            textBox.Text += "MachineStoreForApplication:" + testWriting(file) + "\r\n";
        } catch { }
        try {
            file = IsolatedStorageFile.GetMachineStoreForAssembly();
            textBox.Text += "MachineStoreForAssembly:" + testWriting(file) + "\r\n";
        } catch { }
        try {
            file = IsolatedStorageFile.GetMachineStoreForDomain();
            textBox.Text += "MachineStoreForDomain:" + testWriting(file) + "\r\n";
        } catch { }

        try {
            file = IsolatedStorageFile.GetUserStoreForApplication();
            textBox.Text += "UserStoreForApplication:" + testWriting(file) + "\r\n";
        } catch { }
        try {
            file = IsolatedStorageFile.GetUserStoreForAssembly();
            textBox.Text += "UserStoreForAssembly:" + testWriting(file) + "\r\n";
        } catch { }
        try {
            file = IsolatedStorageFile.GetUserStoreForDomain();
            textBox.Text += "UserStoreForDomain:" + testWriting(file) + "\r\n";
        } catch { }
        try {
            file = IsolatedStorageFile.GetUserStoreForSite();
            textBox.Text += "UserStoreForSite:" + testWriting(file) + "\r\n";
        } catch { }
    }
}
}
