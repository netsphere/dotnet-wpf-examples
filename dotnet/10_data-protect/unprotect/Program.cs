﻿using System.Security.Cryptography;
using System.Text;

/*
別のプロセスで開いて、復号できる。
悪意のあるプログラムを実行できると、データが奪取できる。
 * */

namespace unprotect
{

internal class Program
{
    static byte[] entropy = {9, 8, 7, 6, 5};

    static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");

        var fstream = new FileStream("C:\\Windows\\Temp\\hoge", FileMode.Open);
        byte[] inBuffer = new byte[4096];
        fstream.Read(inBuffer, 0, inBuffer.Length);
        fstream.Close();

        byte[] decrypt = ProtectedData.Unprotect(inBuffer, entropy, 
                                                 DataProtectionScope.CurrentUser);
        Console.WriteLine("Decrypted data: " + UnicodeEncoding.UTF8.GetString(decrypt));
    }
}

}