﻿
using System.Security.Cryptography;
using System.Text;

namespace protect
{

internal class Program
{
    static byte[] entropy = {9, 8, 7, 6, 5};

    static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");

        // 暗号化するデータ
        byte[] bytes = UnicodeEncoding.UTF8.GetBytes("This is some data of any length.");

        // Encrypt the data
        byte[] encrypted = ProtectedData.Protect(bytes, entropy, 
                                                 DataProtectionScope.CurrentUser );

        // ファイルに書き込む
        var writer = new FileStream("C:\\Windows\\Temp\\hoge", FileMode.Create);
        writer.Write(encrypted, 0, encrypted.Length);
        writer.Close();
    }
}

}
