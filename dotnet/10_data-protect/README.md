 
# ローカルに秘密情報を保存するには

結論:
 - ローカルに秘密情報を安全に保存することはできない。
 - 特に、伝統的な Windows アプリには、アプリケィション間でデータ隔離する考え方がない。
 - UWP アプリも、伝統的な Windows アプリ側からデータを取り出すことができて、守れていない。



## Isolated storage

`System.IO.IsolatedStorage` 名前空間. 保存先が 7種類あるが、.NET では最後の `ForSite()` だけ実行時エラーになる。

UWP でサポートされているのは 3種類だけ。それ以外は, コンパイルできるが、実行時例外になる!

|メソッド                           |.NET               |UWP              |
|-----------------------------------|-------------------|-----------------|
|`.GetMachineStoreForApplication()` |.NET Framework 2.0 |x UWP            |
|`.GetMachineStoreForAssembly()`    |.NET Framework 2.0 |x UWP            |
|`.GetMachineStoreForDomain()`      |.NET Framework 2.0 |x UWP            |
|`.GetUserStoreForApplication()`    |.NET Framework 2.0 |◯ UWP           |
|`.GetUserStoreForAssembly()`       |.NET Framework 1.1 |◯ ドキュメントが誤り。UWP OK |
|`.GetUserStoreForDomain()`         |.NET Framework 1.1 |◯ ドキュメントが誤り。UWP OK |
|`.GetUserStoreForSite()`           |x .NET Framework 4.0 |x UWP            |


NIAP が発行する [Protection Profile for Application Software](https://www.niap-ccevs.org/Profile/Info.cfm?PPID=462&id=462) では、次のように定めている:

<blockquote>
Platforms: Microsoft Windows...
  <p>The evaluator shall verify that all certificates are stored in the Windows
    Certificate Store. The evaluator shall verify that other credentials, like
    passwords, are stored in the Windows Credential Manager or stored using the
    Data Protection API (DPAPI). For Windows Universal Applications, the 
    evaluator shall verify that the application is using the <code>ProtectData</code> class
    and storing credentials in <code>IsolatedStorage</code>.
</blockquote>

UWP: <code>IsolatedStorage</code> と <code>ProtectData</code> クラスを組み合わせることを求めている。



### .NET 

 - 保存されるデータは暗号化されない!
 - ほかのアプリケーションからも普通に開ける!

|種別                         |保存先                                          |
|-----------------------------|------------------------------------------------|
|`MachineStoreForApplication` |C:\ProgramData\IsolatedStorage\nji0i2bs.u5q\wnsmbny3.1ng\Url.wceofzimsoftw3w0ysd1p3jp0e2bhw2a\AppFiles\   |
|`MachineStoreForAssembly`    |C:\ProgramData\IsolatedStorage\nji0i2bs.u5q\wnsmbny3.1ng\Url.wceofzimsoftw3w0ysd1p3jp0e2bhw2a\AssemFiles\ |
|`MachineStoreForDomain`      |C:\ProgramData\IsolatedStorage\nji0i2bs.u5q\wnsmbny3.1ng\Url.wceofzimsoftw3w0ysd1p3jp0e2bhw2a\Url.wceofzimsoftw3w0ysd1p3jp0e2bhw2a\Files\    |
|`UserStoreForApplication`    |C:\Users\hhori\AppData\Local\IsolatedStorage\1kfvgi1p.mav\ffm1lt5i.one\Url.wceofzimsoftw3w0ysd1p3jp0e2bhw2a\AppFiles\  |
|`UserStoreForAssembly`       |C:\Users\hhori\AppData\Local\IsolatedStorage\1kfvgi1p.mav\ffm1lt5i.one\Url.wceofzimsoftw3w0ysd1p3jp0e2bhw2a\AssemFiles\   |
|`UserStoreForDomain`         |C:\Users\hhori\AppData\Local\IsolatedStorage\1kfvgi1p.mav\ffm1lt5i.one\Url.wceofzimsoftw3w0ysd1p3jp0e2bhw2a\Url.wceofzimsoftw3w0ysd1p3jp0e2bhw2a\Files\    |



### UWP

 - ファイルは、伝統的な Windows アプリ (エクスプローラ含む.) から開ける
 - 内容も、暗号化されていない!

|種別                         |保存先                                          |
|-----------------------------|------------------------------------------------|
|`UserStoreForApplication`    |C:\Users\hhori\AppData\Local\Packages\5ebc1c6e-2c0b-4b19-8411-9511678c4c57_kbcqj21zwsk6y\LocalState\IsolatedStorage\Url.mn41yn5ggwnkvhb1xduxfgq1qdsxtvre\AppFiles\   |
|`UserStoreForAssembly`       |C:\Users\hhori\AppData\Local\Packages\5ebc1c6e-2c0b-4b19-8411-9511678c4c57_kbcqj21zwsk6y\LocalState\IsolatedStorage\Url.mn41yn5ggwnkvhb1xduxfgq1qdsxtvre\AssemFiles\ |
|`UserStoreForDomain`         |C:\Users\hhori\AppData\Local\Packages\5ebc1c6e-2c0b-4b19-8411-9511678c4c57_kbcqj21zwsk6y\LocalState\IsolatedStorage\Url.mn41yn5ggwnkvhb1xduxfgq1qdsxtvre\Url.mn41yn5ggwnkvhb1xduxfgq1qdsxtvre\Files\  |




## Data Protection API (DPAPI)

`ProtectedData.Protect()` で暗号化し, `ProtectedData.Unprotect()` で復号する。

エントロピーが共通鍵になる。逆コンパイルされれば、鍵が抜き取られる。

カジュアルな攻撃を防ぐだけのもの。


