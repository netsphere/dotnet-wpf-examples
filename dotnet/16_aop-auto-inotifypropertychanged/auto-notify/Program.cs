
using System.ComponentModel;


namespace auto_notify
{

// public or internal でなければならない。
internal class Customer
{
    // 注意! `virtual` が必要
    [AlsoNotifyFor("FullName")]
    public virtual string GivenName { get; set; }

    [AlsoNotifyFor("FullName")]
    public virtual string FamilyName { get; set; }

    public string FullName {
        get { return $"{GivenName} {FamilyName}"; }
    }

    public virtual string Address { get; set; }

    [DoNotNotify]
    public virtual string Hoge { get; set; }
}


class Observer
{
    string _name;

    public Observer(string name) 
    {
        _name = name;
    }

    public void OnThingPropertyChanged(object? sender, PropertyChangedEventArgs ea)
    { 
        var c = (Customer) sender;
        Console.WriteLine(_name + ": propName = " + ea.PropertyName);
    }
}


internal class Program
{
    static void Main(string[] args)
    {
        var c = AutoNotifyInterceptor<Customer>.CreateInstance();
        c.GivenName = "Test";

        var o1 = new Observer("a");
        var o2 = new Observer("b");
        ((INotifyPropertyChanged) c).PropertyChanged += o1.OnThingPropertyChanged;
        ((INotifyPropertyChanged) c).PropertyChanged += o2.OnThingPropertyChanged;

        c.FamilyName = "Hage"; // `FullName` も通知される
        c.Address = "Kansai";
        c.Hoge = "hogehoge"; // 通知されない

        Console.WriteLine(c.FullName);
    }
}
}
