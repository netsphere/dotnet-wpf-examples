using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;

/*
これを参考にした: https://gist.github.com/gsoulavy/b3b7fbce1439d470535806e72419e0a9

 - 対象 (observable) クラスが `INotifyPropertyChanged` なのを強制すると本末転倒
   `CreateClassProxy()` で差し込む
*/


// internal class を使う場合, このアセンブリが必要.
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace auto_notify
{

// @note 厳密に `...Attribute` という名前でなければならない。
[AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
public sealed class AlsoNotifyForAttribute : Attribute
{
    public string[] PropNames { get; protected set; }

    public AlsoNotifyForAttribute(params string[] propNames)
    {
        this.PropNames = propNames;
    }
}

[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
public sealed class DoNotNotifyAttribute : Attribute
{

}


// Note. `StandardInterceptor` から派生する例もあるが、`IInterceptor` から派生
//       したほうがよさそう
// @param TModel public class or internal class
public class AutoNotifyInterceptor<TModel> : IInterceptor
                                        where TModel : class
                                        // where TModel : INotifyPropertyChanged
{
    // プロパティは内部でメソッドになっている get_XXX
    const string SetPrefix = "set_";
	//const string GetPrefix = "get_";

    public TModel Target;

    event PropertyChangedEventHandler? PropertyChanged;

    void RaisePropertyChanged(string name)
    {
        PropertyChanged ?.Invoke(Target, new PropertyChangedEventArgs(name));
    }

    // `set_XXX` メソッドから `XXX` プロパティの情報を得る
    PropertyInfo? propertyInfoForSetterMethod(MethodInfo methodInfo)
	{
        var propertyName = methodInfo.Name.Substring(SetPrefix.Length);
		return methodInfo.DeclaringType.GetProperty(propertyName);
	}

    // @override IInterceptor
    public void Intercept(IInvocation invocation)
    {
        var method = invocation.Method;

        // event `+=` も裏ではメソッド。
        if (method.Name == "add_PropertyChanged") { 
            PropertyChanged += (PropertyChangedEventHandler) invocation.Arguments[0];
            return;
        }
        else if (method.Name == "remove_PropertyChanged") {
            PropertyChanged -= (PropertyChangedEventHandler) invocation.Arguments[0];
            return;
        }

        invocation.Proceed();

        PropertyInfo? propInfo;
        if ( method.Name.StartsWith(SetPrefix) &&
             (propInfo = propertyInfoForSetterMethod(method)) != null) {
            // アトリビュートはメソッドに付いているわけではない
            if (propInfo.GetCustomAttributes<DoNotNotifyAttribute>().Count() == 0 ) {
                RaisePropertyChanged(method.Name.Substring(SetPrefix.Length) );

                var attrs = propInfo.GetCustomAttributes<AlsoNotifyForAttribute>();
                foreach ( AlsoNotifyForAttribute attr in attrs ) {
                    foreach (string propName in attr.PropNames)
                        RaisePropertyChanged(propName);
                }
            }
        }
    }


    static readonly ProxyGenerator _proxyGenerator = new ProxyGenerator();

    public static TModel CreateInstance()
    {
        var interceptor = new AutoNotifyInterceptor<TModel>();

        // public object CreateClassProxy(Type classToProxy,
        //                                Type[] additionalInterfacesToProxy,
        //		                          params IInterceptor[] interceptors);
        var ret = (TModel) _proxyGenerator.CreateClassProxy(typeof(TModel),
                                new Type[] { typeof(INotifyPropertyChanged)},
                                interceptor );
        interceptor.Target = ret;

        return ret;
    }
}

}
