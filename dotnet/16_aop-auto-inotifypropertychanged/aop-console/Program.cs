﻿using Castle.DynamicProxy;
using System.Runtime.CompilerServices;

// internal class を使う場合, このアセンブリが必要.
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
namespace aop_console
{

internal class MyInterceptor : IInterceptor
{
    // override
    public void Intercept(IInvocation invocation)
    {
        Console.WriteLine("beforeMethod: {0}", invocation.Method.Name);

        // 元のメソッドを実行する
        invocation.Proceed();

        Console.WriteLine("afterMethod: {0}", invocation.ReturnValue ?.ToString());
    }
}


// public or internal class のみ.
internal class Person
{ 
    // `virtual` を付けないと、発動されない
    public virtual string Name { get; set; } 

    // `virtual` を付けないと、発動されない
    public virtual void doSomething()
    {
        Console.WriteLine("samu");
    }
}


internal class Program
{
    static void Main(string[] args)
    {
        var proxyGenerator = new ProxyGenerator();
        var person = (Person) proxyGenerator.CreateClassProxy(typeof(Person), 
                                                              new MyInterceptor());
        person.Name = "hoge";
        Console.WriteLine(person.Name);

        person.doSomething();
    }
}
}
