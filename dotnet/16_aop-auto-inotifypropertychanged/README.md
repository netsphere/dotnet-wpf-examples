                                      
# アスペクト指向プログラミング (AOP)

TargetFramework: net6.0

 - Unity.Interception パッケージでは, .NET 6 でどうしても上手く動かすことができなかった。Due to a complete lack of interest from the community to support further development, this project has been archived and will no longer be maintained. There will be no more releases of this library.

 - Castle.Core パッケージでは、問題なく動いた。



## `aop-console/`

単純なサンプル.





## `auto-notify/`

C#インスタンスのプロパティに値を代入したときに自動的に notify するようにする。
いくつか記事が見つかるが、対象のクラスに `INotifyPropertyChanged` を実装させるものがチラホラ。それは本末転倒。


参考:
 - https://www.codewrecks.com/post/old/2008/08/implement-inotifypropertychanged-with-castledynamicproxy/
   複数のハンドラに対応していない? 先にハンドラを呼び出してしまっている
    
 - https://systemartlaboratory.com/notifypropertychangedunity.html
   Unity.Interception を使ったサンプル。これが正解


未了:
 - <s>実用には `[AlsoNotifyFor("FullName")]` アトリビュートや `[DoNotNotify]` アトリビュートが必要。</s> 実装した。
   See https://github.com/Fody/PropertyChanged/wiki/Attributes

