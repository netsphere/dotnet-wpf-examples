
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;


namespace listbox_multiple_select
{

class ItemType {
    public string Name { get; set; }
}

class MainWindowViewModel : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler? PropertyChanged;
    private void notify([CallerMemberName] String propertyName = "")  {  
        PropertyChanged ?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
    
    public ObservableCollection<ItemType> Items { get; protected set; }

    // two way の場合, protected set は不可.
    //  => Mode=TwoWay にはしない.
    public ObservableCollection<ItemType> SelectedItems { get; protected set; }

    private string _selectedOutput;
    public string SelectedOutput { 
        get { return _selectedOutput; }
        set { _selectedOutput = value; notify(); }
    }

    // コンストラクタ
    public MainWindowViewModel() {
        this.Items = new ObservableCollection<ItemType>() {
                new ItemType() {Name = "<all>"},
                new ItemType() {Name = "bbb"},
                new ItemType() {Name = "ccc"} };
        this.SelectedItems = new ObservableCollection<ItemType>();
        this.SelectedItems.CollectionChanged += SelectedItems_CollectionChanged;
    }

    void SelectedItems_CollectionChanged(object? sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    {
        SelectedOutput = "";
        foreach (var selected in SelectedItems)
            SelectedOutput += selected.Name + "\n";
    }
}


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
    }

    void print_vm_items()
    {
        var vm = (MainWindowViewModel) DataContext;
        output.Text = "";
        foreach (var selected in vm.Items)
            output.Text += selected.Name + "\n";
    }

    // vm.Items に追加
    void Button_Click(object sender, RoutedEventArgs e)
    {
        var vm = (MainWindowViewModel) DataContext;
        vm.Items.Add(new ItemType() { Name = $"{DateTime.Now}"});
    }

    // vm.Selection に追加
    void Button_Click_1(object sender, RoutedEventArgs e)
    {
        var vm = (MainWindowViewModel) DataContext;
        vm.SelectedItems.Add(vm.Items[0]);
    }

/*
    // ListBox.Items に追加
    void Button_Click_1(object sender, RoutedEventArgs e)
    {
        // ItemsSource を使用する場合, Items.Add() は無効。
        //   => InvalidOperationException 例外
        listBox.Items.Add(new ItemType() { Name = $"{DateTime.Now}"});
        print_vm_items();
    }
*/
    // ListBox.Selection に追加
    void Button_Click_3(object sender, RoutedEventArgs e)
    {
        var vm = (MainWindowViewModel) DataContext;
        listBox.SelectedItems.Add(vm.Items[1]);
    }

    // vm.Items から削除
    void Button_Click2(object sender, RoutedEventArgs e)
    {
        var vm = (MainWindowViewModel) DataContext;
        vm.Items.RemoveAt(0);
    }

    // vm.Selection から削除
    void Button_Click_2(object sender, RoutedEventArgs e)
    {
        var vm = (MainWindowViewModel) DataContext;
        if (vm.SelectedItems.Count > 0)
            vm.SelectedItems.RemoveAt(0);
    }

/*
    // ListBox.Items から削除
    void Button_Click_2(object sender, RoutedEventArgs e)
    {
        // ItemsSource が使用中の場合、操作は無効
        //   => InvalidOperationException 例外
        listBox.Items.RemoveAt(0);
        print_vm_items();
    }
*/
    // 先にこのイベントハンドラが呼び出され、後から view model が更新される.
    //  => ここで view model を参照するのは早すぎる. `CollectionChanged` を使え.
    void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
/*
        var vm = (MainWindowViewModel) DataContext;
        // ここで e.AddedItems を編集してもよい
        // e.AddedItems の実体は object[] 型の配列. IList インタフェイスを持つが,
        // Clear() で capacity が詰まらない! 全部 null になる
        bool found = false;
        foreach (var item in e.AddedItems) {
            if (item == vm.Items.ElementAt(0)) {  found = true; break; }
        }
        if (!found) 
            e.RemovedItems.Add(vm.Items.ElementAt(0));
        else { 
            e.RemovedItems.Clear();
            for (int i = 1; i < vm.Items.Count; i++) { 
                // 固定長エラー. インタフェイスと実体が合っていない
                e.RemovedItems.Add(vm.Items.ElementAt(i));
            }
        }
        */
    }
}
}
