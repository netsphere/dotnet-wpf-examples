
# listbox-multiple-select

WPF `ListBox` の `SelectedItems` (複数形) プロパティは bind できない。MVVM モデルで使う場合にすごい不便なので、添付ビヘイビアを使う。

多くの解説ページやサンプルがあるが、ほとんどのは上手く動かない。探したなかで唯一まともなのは、こちら:
 - <a href="https://www.codeproject.com/Tips/1207965/SelectedItems-Behavior-for-ListBox-and-MultiSelect">SelectedItems Behavior for ListBox and MultiSelector</a>

これをベースに作った。ほぼほぼそのまま。



Microsoft.Xaml.Behaviors.Wpf パッケージを使う解説も多いが、別にコードが短くなるわけでもなし、書き損じを防ぐわけでもなし、特に使う必要はないのでは?


