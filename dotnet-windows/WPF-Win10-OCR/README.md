
# WPF-Win10-OCR

Forked from https://www.codeproject.com/Articles/5276805/OCR-in-WPF-using-the-WinRT-OCR-API

TargetFramework: `net6.0-windows10.0.19041.0`


## OCR

`Windows.Media.Ocr` namespace のドキュメントに次の記載があるが、誤り。.NET アプリから問題なく使える。

<blockquote>
APIs in the <code>Windows.Media.Ocr</code> namespace are only supported for desktop apps with <i>package identity</i>. This means that the app is installed and run from an MSIX package.
</blockquote>



## カメラでの撮影

 - `Windows.Media.Capture.CameraCaptureUI()` は、ビルドが通るにも関わらず、単に動かない
 - .NET 6.0 は XAML Islands できない。

OpenCvSharp4 を使う






Sample project showing how to use the WinRT OCR API in a .NET Core WPF application.

![Screenshot](Screenshot.png)
