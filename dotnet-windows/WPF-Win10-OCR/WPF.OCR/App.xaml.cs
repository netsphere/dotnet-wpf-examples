
using System.Windows;
using System.Windows.Input;
using WPF.OCR.Views;

namespace WPF.OCR
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
public partial class MyApp : Application
{
    void Application_Startup(object sender, StartupEventArgs e)
    {
        var wnd = new MainWindow();
        wnd.Show();
    }
}


// コマンドを並べる
static class MyCommands
{
    public static readonly RoutedUICommand SelectImage =
            new RoutedUICommand("画像ファイルを選択", nameof(SelectImage),
                                typeof(MyCommands));

    public static readonly RoutedUICommand ExtractText =
            new RoutedUICommand("テキストを抽出", nameof(ExtractText),
                                typeof(MyCommands));

    public static readonly RoutedUICommand PrepareCaptureImage =
            new RoutedUICommand("カメラで撮影準備", nameof(PrepareCaptureImage),
                                typeof(MyCommands));

    public static readonly RoutedUICommand DoCaptureImage =
            new RoutedUICommand("撮影", nameof(DoCaptureImage), typeof(MyCommands));

    public static readonly RoutedUICommand CopyTextToClipboard =
            new RoutedUICommand("クリップボードにコピー",
                                nameof(CopyTextToClipboard), typeof(MyCommands));
}

}
