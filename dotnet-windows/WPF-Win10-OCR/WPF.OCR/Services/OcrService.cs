
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Globalization;
using Windows.Graphics.Imaging;
using Windows.Media.Ocr;
using Windows.System.UserProfile;


namespace WPF.OCR.Services
{

public class OcrService
{

    /**
     * Extracts text from an image using Windows 10 OCR.
     *
     * @param image         The image to extract text from.</param>
     * @param languageCode  The language code of the language in the image.
     *                      The language code should be an installed language supported by Windows 10 OCR.</param>
     *                      null の場合, ユーザプロファイルの言語を使う.
     * @return The text extracted from an image.</returns>
     */
    public async Task<string> ExtractText(string image, string languageCode)
    {
        if (string.IsNullOrWhiteSpace(image))
            throw new ArgumentNullException("Image can't be null or empty.");

        // if (string.IsNullOrWhiteSpace(languageCode))
        //        throw new ArgumentNullException("Language can't be null or empty.");

        if (!File.Exists(image))
            throw new ArgumentOutOfRangeException($"'{image}' doesn't exist.");

        CheckIfFileIsImage(image);

        if (languageCode != null) { 
            if (!GlobalizationPreferences.Languages.Contains(languageCode))
                throw new ArgumentOutOfRangeException($"{languageCode} is not installed.");
        }

        StringBuilder text = new StringBuilder();
        await using (var fileStream = File.OpenRead(image)) {
            // `BitmapDecoder` は .NET にもあるが, そうではなく,
            // WinRT の同名クラス `Windows.Graphics.Imaging.BitmapDecoder`
            var bmpDecoder = await BitmapDecoder.CreateAsync(fileStream.AsRandomAccessStream());
            var softwareBmp = await bmpDecoder.GetSoftwareBitmapAsync();

            var ocrEngine = languageCode != null ?
                                OcrEngine.TryCreateFromLanguage(new Language(languageCode)) :
                                OcrEngine.TryCreateFromUserProfileLanguages();
            // 画像が横向きだと全滅。事前に水平出しが必要
            var ocrResult = await ocrEngine.RecognizeAsync(softwareBmp);

            foreach (OcrLine line in ocrResult.Lines) {
                text.AppendLine(line.Text);
                // テキストは1行でもすごい刻む。
                // TODO: ボックスを結合したほうがいい。
                foreach (var it in line.Words) {
                    text.AppendLine($"({it.BoundingRect.X},{it.BoundingRect.Y},{it.BoundingRect.Width},{it.BoundingRect.Height})");    
                }
            }
        }

        return text.ToString();
    }

        private void CheckIfFileIsImage(string file)
        {
            var isImage = Regex.IsMatch(Path.GetExtension(file).ToLower(),
                "(jpg|jpeg|jfif|png|bmp)$", RegexOptions.Compiled);

            if (!isImage)
                throw new ArgumentOutOfRangeException($"'{file}' is not an image.");
        }
    }
}
