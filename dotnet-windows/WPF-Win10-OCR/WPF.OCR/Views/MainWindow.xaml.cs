
using System;
using System.Windows.Input;

namespace WPF.OCR.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
public partial class MainWindow
{
    public MainWindow()
    {
        InitializeComponent();

        MainWindowViewModel vm = (MainWindowViewModel) DataContext;

        // 画像を読み込む
        CommandBindings.Add(new CommandBinding(MyCommands.PrepareCaptureImage,
                                               PrepareCaptureImage));
        CommandBindings.Add(new CommandBinding(MyCommands.SelectImage,
                                               vm.SelectImageCommand));

        CommandBindings.Add(new CommandBinding(MyCommands.ExtractText,
                                vm.ExtractTextCommand, vm.CanExtractText));
        CommandBindings.Add(new CommandBinding(MyCommands.CopyTextToClipboard,
                                vm.CopyTextToClipboard, vm.CanCopyTextToClipboard));
    }


    // Command Handlers ///////////////////////////////////////////////////

    // [キャプチャ...]
    void PrepareCaptureImage(object sender, System.Windows.RoutedEventArgs e)
    {
        var wnd = new CaptureWindow();
        if (wnd.ShowDialog() == true) {

        }

/* 以下は動かない
        var dialog = new Windows.Media.Capture.CameraCaptureUI();

        // 写真のフォーマットを指定
        dialog.PhotoSettings.Format =
                        Windows.Media.Capture.CameraCaptureUIPhotoFormat.Png;

        // ✗写真撮影モードでダイアログを起動
        //   -> これは WPF では動かない. Why?
        var file = await dialog.CaptureFileAsync(
                            Windows.Media.Capture.CameraCaptureUIMode.Photo);

        // 撮影できると、保存されたファイルのStorageFileが返ってくる。
        // 保存先はApplicationData.Current.TemporaryFolder固定
        if (file != null) {
            var msgbox = new Windows.UI.Popups.MessageDialog(file.Path, "撮影結果");
            await msgbox.ShowAsync();
        }
*/
    }
}
}
