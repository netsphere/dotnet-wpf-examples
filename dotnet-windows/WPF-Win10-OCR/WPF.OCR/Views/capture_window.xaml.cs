
using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;


namespace WPF.OCR.Views
{

// sourceType (view model 側) = Bitmap, targetType (XAML 側) = BitmapImage
// From https://stackoverflow.com/questions/6484357/converting-bitmapimage-to-bitmap-and-vice-versa
[System.Windows.Data.ValueConversion(typeof(Bitmap), typeof(BitmapImage))]
public class BitmapToBitmapImageConverter : IValueConverter
{
    // @implements
    // Bitmap -> BitmapImage
    public object Convert(object value, Type targetType, object parameter,
                          CultureInfo culture)
    {
        if (value == null) return null;

        using (var memory = new MemoryStream()) {
            ((System.Drawing.Bitmap) value).Save(memory, ImageFormat.Png);
            memory.Position = 0;

            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = memory;
            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            bitmapImage.EndInit();
            bitmapImage.Freeze();

            return bitmapImage;
        }
/*
        IntPtr hBitmap = bitmap.GetHbitmap();
        BitmapImage retval;

        try {
            戻り値の型が BitmapSource
            retval = (BitmapImage)Imaging.CreateBitmapSourceFromHBitmap(
                     hBitmap,
                     IntPtr.Zero,
                     Int32Rect.Empty,
                     BitmapSizeOptions.FromEmptyOptions());
        }
        finally {
            //DeleteObject(hBitmap);
        }

        return retval;
        */
    }

    // @implements
    // BitmapImage -> Bitmap
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value == null) return null;

        using (MemoryStream outStream = new MemoryStream()) {
            BitmapEncoder enc = new BmpBitmapEncoder();
            enc.Frames.Add(BitmapFrame.Create((BitmapImage) value));
            enc.Save(outStream);
            return new Bitmap(outStream);
        }
    }
}


public class CaptureWindowViewModel : MVVM.BindableBase
{
    private Bitmap _cameraImage;
    public Bitmap CameraImage {
        get => _cameraImage;
        set { SetPropertyAndRaise(ref _cameraImage, value); }
    }

    private Bitmap _capturedImage;
    public Bitmap CapturedImage {
        get => _capturedImage;
        set { SetPropertyAndRaise(ref _capturedImage, value); }
    }
}


    /// <summary>
    /// capture_window.xaml の相互作用ロジック
    /// </summary>
public partial class CaptureWindow : System.Windows.Window
{
    private VideoCapture _capture;
    private bool _enable_update = true;
    private Mat _currentFrame;

    public CaptureWindow()
    {
        InitializeComponent();

        this.CommandBindings.Add(new CommandBinding(MyCommands.DoCaptureImage,
                                               CaptureButton_Click, CanCaptureImage));

        // new VideoCapture() がすごい遅い。Workaround.
        Environment.SetEnvironmentVariable("OPENCV_VIDEOIO_MSMF_ENABLE_HW_TRANSFORMS", "0");
        _capture = new VideoCapture(0); // カメラデバイスを選択（0はデフォルトのカメラ）
        //_capture.FrameHeight = 1280;
        //_capture.FrameWidth = 720;

        if (!_capture.IsOpened()) {
            MessageBox.Show("カメラデバイスが利用できません。");
            Close();
        }

        // フレーム毎に描画
        CompositionTarget.Rendering += CompositionTarget_Rendering;
    }


    // Event Handlers /////////////////////////////////////////////////

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
    }

    async void CompositionTarget_Rendering(object sender, EventArgs e)
    {
        if (_capture != null && _capture.IsOpened() && _enable_update) {
            _enable_update = false;
            _currentFrame = new Mat();
            _capture.Read(_currentFrame);

            if (!_currentFrame.Empty()) {
                // `BitmapConverter` だけ OpenCvSharp4.Extensions パッケージ
                System.Drawing.Bitmap bitmapSource = BitmapConverter.ToBitmap(_currentFrame);
                // ウィンドウに表示
                CaptureWindowViewModel vm = (CaptureWindowViewModel) DataContext;
                vm.CameraImage = bitmapSource;
            }

            // これがないと command が有効にならない.
            // 30 FPS = 33 ミリ秒
            await Task.Delay(66); // 15 FPS

            _enable_update = true;
        }
    }

    // [決定]
    void Button_Click(object sender, RoutedEventArgs e)
    {
        DialogResult = true;
    }


    // Command Handlers /////////////////////////////////////////////////

    // [撮影]
    void CaptureButton_Click(object sender, RoutedEventArgs e)
    {
        if (_capture == null || !_capture.IsOpened())
            return;

        _enable_update = false; // キャプチャ中は更新を停止

        // 画像を保存
        string imagePath = "C:\\Windows\\Temp\\captured_image.jpg";
        _currentFrame.SaveImage(imagePath);

        // 画像をAPIサーバーにアップロード
        //await UploadImageToServer(imagePath);

        Bitmap bitmapSource = BitmapConverter.ToBitmap(_currentFrame);
        CaptureWindowViewModel vm = (CaptureWindowViewModel) DataContext;
        vm.CapturedImage = bitmapSource;

        _enable_update = true; // キャプチャが終了したら再開
    }

    void CanCaptureImage(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = _capture != null && _capture.IsOpened();
    }

}

}
