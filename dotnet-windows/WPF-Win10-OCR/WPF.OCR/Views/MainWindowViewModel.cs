
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Windows.System.UserProfile;
using WPF.OCR.Commands;
using WPF.OCR.Services;


namespace WPF.OCR.Views
{

public class MainWindowViewModel : MVVM.BindableBase
{
    // private readonly IDialogService dialogService;

    private readonly OcrService ocrService;

    // コンストラクタ. 引数なし
    public MainWindowViewModel()
    {
        //dialogService = dialogSvc;
        ocrService = new OcrService();
    }

    // Language codes of installed languages.
    public List<string> InstalledLanguages => GlobalizationPreferences.Languages.ToList();

    private string _imageLanguageCode;
    public string ImageLanguageCode { 
        get => _imageLanguageCode;
        set { SetPropertyAndRaise(ref _imageLanguageCode, value); }
    }

    // 画像ファイル名
    private string _selectedImage;
    public string SelectedImage {
        get => _selectedImage;
        set { SetPropertyAndRaise(ref _selectedImage, value); }
    }

    private string _extractedText;
    public string ExtractedText {
        get => _extractedText;
        set { SetPropertyAndRaise(ref _extractedText, value); }
    }


    // Command handlers /////////////////////////////////////////////////

    // Select Image Command
    internal void SelectImageCommand(object sender, ExecutedRoutedEventArgs e)
    {
        var dialog = new OpenFileDialog() { 
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures),
                Title = "Select image",
                Filter = "Image Files (*.jpg;*.jpeg;*.png;*.bmp)|*.jpg;*.jpeg;*.png;*.bmp",
                CheckFileExists = true,
                CheckPathExists = true,
                RestoreDirectory = true };

        if (dialog.ShowDialog() == true) {
            string image = dialog.FileName;

            SelectedImage = image;
            ExtractedText = string.Empty;
        }
    }


    // Extract Text Command
    internal async void ExtractTextCommand(object sender, ExecutedRoutedEventArgs e)
    {
        ExtractedText = await ocrService.ExtractText(SelectedImage, ImageLanguageCode);
    }

    internal void CanExtractText(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = !string.IsNullOrWhiteSpace(ImageLanguageCode) &&
                       !string.IsNullOrWhiteSpace(SelectedImage);
    }


    // Copy Text to Clipboard Command
    internal void CopyTextToClipboard(object sender, ExecutedRoutedEventArgs e)
    {
        Clipboard.SetData(DataFormats.Text, _extractedText);
    }

    internal void CanCopyTextToClipboard(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = !string.IsNullOrWhiteSpace(_extractedText);
    }

}

}
