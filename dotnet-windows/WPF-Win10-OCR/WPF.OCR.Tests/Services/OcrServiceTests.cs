

using System;
using System.IO;
using System.Threading.Tasks;
using Xunit;

namespace WPF.OCR.Services.Tests
{

public class OcrServiceTests
{
    OcrService ocrService;

    //[SetUp]
    public OcrServiceTests()
    {
        ocrService = new OcrService();
    }


    [Fact]
    public async Task ExtractTextTest_UsingCodeOfNonInstalledLanguage_ThrowsArgumentOutOfRangeException()
    {
        string image = Path.Combine(Environment.CurrentDirectory, "Images", "Quote.png");
        string languageCode = "fr-FR";

        try {
            await ocrService.ExtractText(image, languageCode);
        }
        catch (ArgumentOutOfRangeException e) {
            Assert.Contains($"{languageCode} is not installed.",
                                  e.Message);
            return;
        }

        Assert.Fail("ArgumentOutOfRangeException was not thrown.");
    }

    [Fact]
    public async Task ExtractTextTest_UsingCodeOfInstalledLanguage_ImageTextExtracted()
    {
        string image = Path.Combine(Environment.CurrentDirectory, "Images", "Quote.png");
        string languageCode = "en-GB";

        string extractedText = await ocrService.ExtractText(image, languageCode);

        Assert.True(!string.IsNullOrWhiteSpace(extractedText));
        Assert.True(extractedText.Contains("programmer", StringComparison.OrdinalIgnoreCase));
    }
}
}
