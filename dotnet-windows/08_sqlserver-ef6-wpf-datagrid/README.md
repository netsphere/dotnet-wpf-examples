
# Entity Framework 6 (EF6) - WPF DataGrid

未了:
  - 楽観ロック. `RecordBase` クラスに `LockVersion` を作ったが、使っていない
  - フィルタが未実装
  - <s>bug/ salesorder list から [編集] ボタンで編集画面を開いて、status を変更して update しても、一覧画面のほうのステイタスが更新されない。</s> すみ
  - 

ローカルのデータベースを利用するサンプル。SQL Server を利用。

TargetFrameworkVersion: v4.7.2

このサンプルの狙い;
 - ウィンドウを複数開き、データが更新されたときに全部の表示を更新する。
 - datagrid での並び替えを実装
 

データベースアプリケーションが簡単に作れる。生産性高い。
派手にするにはもっと工夫が必要。




## How to run

`App.config` ファイルで接続先データベース名を指定。


### SQL Server Express LocalDB

Visual Studio Installer で SQL Server Express 2019 LocalDB がインストールされていること。

インスタンスの状況を確認する。PowerShell から,

<pre>
&gt; <kbd>SqlLocalDB.exe info</kbd>
MSSQLLocalDB
ProjectModels
</pre>

<pre>
&gt; <kbd>SqlLocalDB.exe info MSSQLLocalDB</kbd>
Name:               MSSQLLocalDB
Version:            15.0.4153.1
Shared name:
Owner:              DESKTOP-4148VUU\hhori
Auto-create:        Yes
State:              Stopped
Last start time:    2023/03/21 13:19:16
Instance pipe name:
</pre>

表示 > その他のウィンドウ > [パッケージマネージャーコンソール]

<pre>
PM&gt; <kbd>Update-Database</kbd>
Specify the '-Verbose' flag to view the SQL statements being applied to the target database.
No pending explicit migrations.
Running Seed method.
</pre>




### SQL Server

管理者権限で, SQL Server Management Studio でデータベースを作成
<kbd>Update-Database</kbd> コマンドでデータベースを更新。





## Code First

1. いきなりモデルクラスをC#で手書きする。この例では `Models` ディレクトリにまとめた。
2. `DbContext` 派生クラスを作り、そこに各モデルクラスを記載する。



## EntityFramework 6 (EF6): Seed 

`Migrations/Configuration.cs` ファイルに記述する。
 - https://increment-i.hateblo.jp/entry/entityframework/seed



EF Core の場合はやり方が変わる。EF Core 2.x 以前と 3.0 以降も異なる。
 - https://www.learnentityframeworkcore.com/migrations/seeding
 - https://code-maze.com/migrations-and-seed-data-efcore/



