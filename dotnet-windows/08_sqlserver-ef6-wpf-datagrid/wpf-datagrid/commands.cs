using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input; // CommandBinding

namespace wpf_datagrid
{

// コマンドを並べる
static class MyCommands
{
    // CommandBindingCollection クラスは非ジェネリックな IList から派生.
    public static readonly List<CommandBinding> CommandBindings =
                                             new List<CommandBinding>();

    // コマンド -- Static にするのがキモ!

    // MainWindow -> [新しい注文...] button
    public static readonly RoutedUICommand NewSalesOrder =
            new RoutedUICommand("新しい注文", nameof(NewSalesOrder),
                                typeof(MyCommands));

    public static readonly RoutedUICommand NewCustomer =
            new RoutedUICommand("新しい顧客", nameof(NewCustomer),
                                typeof(MyCommands));

    // SalesOrder List Window -> [フィルタ] button
    public static readonly RoutedUICommand FilterOrders =
            new RoutedUICommand("受注の一覧", nameof(FilterOrders),
                                typeof(MyCommands));

    // MainWindow -> [詳細] button
    public static readonly RoutedUICommand SalesOrderDetail =
            new RoutedUICommand("受注の詳細を確認", nameof(SalesOrderDetail),
                                typeof(MyCommands));

    // Customer List Window -> [詳細] button
    public static readonly RoutedUICommand CustomerDetail =
            new RoutedUICommand("顧客の詳細を確認", nameof(CustomerDetail),
                                typeof(MyCommands));

    public static readonly RoutedUICommand Window_SalesOrderList =
            new RoutedUICommand("SalesOrder一覧ウィンドウ",
                                nameof(Window_SalesOrderList),
                                typeof(MyCommands));

    public static readonly RoutedUICommand Window_CustomerList =
            new RoutedUICommand("SalesOrder一覧ウィンドウ",
                                nameof(Window_CustomerList),
                                typeof(MyCommands));

} // class MyCommands

}
