
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Windows;


namespace TreeListLazyLoading
{

class ItemTemplate : MVVM.BindableBase
{
    // Expander を表示させるため, 子を一つつくる
    static ItemTemplate _dummy = new ItemTemplate();

    public string Name {
        get {
            return (_model != null ? _model.Name : "(dummy)");
        }
    }

    public string FullPath {
        get {
            // FullPath フィールドは public ではない
            return _model != null ? _model.FullName : "";
        }
    }

    public List<ItemTemplate> Children { get; protected set; }

    //public Brush Brush { get; set; }

    bool HasDummyChild()
    {
        return Children.Count == 1 && Children[0] == _dummy;
    }

    private bool _isExpanded;
    public bool IsExpanded {
        get { return _isExpanded; }
        set {
            if (value != _isExpanded) {
                _isExpanded = value;
                RaisePropertyChanged();
            }

            if (_isExpanded ) {
                // Expand all the way up to the root.
                if ( _parent != null)
                    _parent.IsExpanded = true;

                // Lazy load the child items, if necessary.
                if (HasDummyChild() ) {
                    //this.Children.Clear();
                    LoadChildren();
                }
                RaisePropertyChanged(nameof(Children));
            }
        }
    }

    private bool _isSelected;
    public bool IsSelected {
        get { return _isSelected; }
        set {
            if ( _parent != null && !_parent.IsExpanded )
                _parent.IsExpanded = true;  
                    
            SetPropertyAndRaise(ref _isSelected, value);
        }
    }

    void LoadChildren()
    {
        List<ItemTemplate> list = new List<ItemTemplate>();
        try { 
            foreach (var info in _model.GetDirectories()) // ここで access denied 例外
                list.Add(new ItemTemplate(this, info.FullName));
        }
        catch (Exception ex) {
            //MessageBox.Show(ex.Message);
        }

        this.Children = list;
    }

    readonly DirectoryInfo? _model;

    readonly ItemTemplate? _parent;

    protected ItemTemplate()
    {
    }

    // コンストラクタ
    // @param parent Root のときは null
    public ItemTemplate(ItemTemplate? parent, string path)
    {
        _parent = parent;

        _model = new DirectoryInfo(path); // 存在チェックはしない. マジか
        if (!_model.Exists )
            throw new ArgumentException(path + " は存在しない");

        this.Children = new List<ItemTemplate>() {_dummy};
    }

    public ItemTemplate? find(string path)
    {
        if (HasDummyChild() )
            LoadChildren();

        foreach (var x in Children) {
            if (String.Compare(path, x.Name, true) == 0)
                return x;
        }
        return null;
    }
}


internal class MainWindowViewModel : MVVM.BindableBase
{
    private ObservableCollection<ItemTemplate> _treeRoots;
    public ObservableCollection<ItemTemplate> TreeRoots {
        get { return _treeRoots; }
        set { SetPropertyAndRaise(ref _treeRoots, value); }
    }

    // 配列は固定サイズ
    static string[] split_path(string path)
    {
        var dirs = path.Split(new char[] {System.IO.Path.DirectorySeparatorChar},
                              StringSplitOptions.RemoveEmptyEntries);
        // dirs = ["C:", "Program Files (x86)", ..., "ja"]

        return dirs;
    }

    public void lookup(string fullpath)
    {
        var dirs = split_path(fullpath);
        ItemTemplate? cur = TreeRoots[0];

        int idx = 0;
        // ドライブレター
        if (dirs[0][1] == ':') {
            // TODO: impl.
            idx++;
        }

        for ( ; idx < dirs.Count(); ++idx) {
            var x = cur.find(dirs[idx]);
            if (x == null) {
                MessageBox.Show("not found");
                break;
            }
            cur = x;
        }
        cur.IsSelected = true;
    }
}


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();

        MainWindowViewModel vm = (MainWindowViewModel) DataContext;
        vm.TreeRoots = new ObservableCollection<ItemTemplate>() {
                                new ItemTemplate(null, @"c:/") };
    }

    // [Go] button
    void Button_Click(object sender, RoutedEventArgs e)
    {
        MainWindowViewModel vm = (MainWindowViewModel) DataContext;
        vm.lookup(txtGoto.Text);
    }
}

}
