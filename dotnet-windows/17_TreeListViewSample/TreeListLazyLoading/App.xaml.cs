
using System.Windows;

namespace TreeListLazyLoading
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
public partial class MyApp : Application
{
    void Application_Startup(object sender, StartupEventArgs e)
    {
        var wnd = new MainWindow();
        wnd.Show();
    }
}

}
