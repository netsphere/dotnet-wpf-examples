
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;


namespace TreeGridSample
{

class ItemTemplate
{
    public string Name { get; set; }

    public List<ItemTemplate> Children { get; set; }

    public Brush Brush { get; set; }
}


internal class MainWindowViewModel : MVVM.BindableBase
{
    // 差替え可能にする
    private ObservableCollection<ItemTemplate> _treeRoots;
    public ObservableCollection<ItemTemplate> TreeRoots { 
        get { return _treeRoots; }
        set { SetPropertyAndRaise(ref _treeRoots, value); }
    }
}


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
    }

    private ObservableCollection<ItemTemplate> make_elements(int inum, int jnum, int knum)
    {
        var roots = new ObservableCollection<ItemTemplate>();
        for (int i = 0; i < inum; ++i) { 
            var item = new ItemTemplate() { 
                            Name = "toplevel " + (i + 1), 
                            Children = new List<ItemTemplate>() };
            roots.Add(item);

            for (int j = 0; j < jnum; ++j) {
                var item_2nd = new ItemTemplate() { Name = "second " + (j + 1), 
                                            Children = new List<ItemTemplate>(),
                                            Brush = new SolidColorBrush(Colors.Red) };
                item.Children.Add(item_2nd);

                for (int k = 0; k < knum; ++k) {
                    var item_3rd = new ItemTemplate() { Name = "3rd " + (k + 1), 
                                            Brush = new SolidColorBrush(Colors.Blue) };
                    item_2nd.Children.Add(item_3rd);
                }
            }
        }

        return roots;
    }

    // [Add items] button
    private void Button_Click(object sender, RoutedEventArgs e)
    {
        var vm = (MainWindowViewModel) DataContext;
        vm.TreeRoots = make_elements(3, 5, 5);
    }

    // [Add a large number]
    private void Button_Click_1(object sender, RoutedEventArgs e)
    {
        var vm = (MainWindowViewModel) DataContext;
        // これは一瞬で戻る。が、ユーザがノードを展開すると固まる
        vm.TreeRoots = make_elements(5, 5000, 0);
    }
}

}
