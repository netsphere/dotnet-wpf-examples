
# WPF TreeGrid (TreeList) sample


## Tree+ListView

表のなかの一番左側の列に `TreeView` を表示したい、という要望は多い。にもかかわらず、なかなか上手くいかない。
非常に罠が多い。


<a href="https://dlaa.me/blog/post/9898803">If it walks like a duck and talks like a duck, it must be a ... TreeGrid! [A simple, XAML-only TreeGrid UI for WPF] - The blog of dlaa.me</a>
  試したが、2つ目の列が右へズレてしまう。

そのほかにも、多くのページがヒットするが、どうにも上手くいかない。有償のライブラリならいけそう。Qt5/Qt6 なら `QTreeView` class, 

狙った形に表示できたのは、次の二つだけ:

 - <a href="https://www.codeproject.com/Articles/24973/TreeListView-2">TreeListView - CodeProject</a>
   `TreeView` を派生させた `TreeListView` クラス.
   列を設定するだけで、あとは `TreeView` と同じ使い方。
 
 - <a href="https://www.codeproject.com/Articles/30721/WPF-TreeListView-Control">WPF TreeListView Control - CodeProject</a>
   `ListView` を派生させた `TreeList` クラス.
   スピードが速いのが売り。`Model` プロパティに木の view model データを設定して、コールバックさせるようになっている。木のデータそのものを設定するわけではないのに注意。
   最初から木のデータを設定しきっても十分速いし、大きいデータの場合は `GetChildren()` メソッドをオーバライドすれば自然に lazy loading にもできる。

どちらのクラスも `Themes/Generic.xaml` ファイルでスタイルを設定している。厳密にこのファイル名の Resource Dictionary ファイルは、実行時に自動的に読み込まれる。別プロジェクトに持っていくとき、名前を変えると読み込まれず、だいぶ悩んだ。罠か。

後者は, .NET 6 で使おうと思うと、プロジェクトをマルチターゲットにしてやらないといけない。(.NET Standard は WPF は使えない)

```xml
<Project Sdk="Microsoft.NET.Sdk">
  <PropertyGroup>
    <!-- This project will output netstandard2.0 and net462 assemblies -->
    <TargetFrameworks>netstandard2.0;net462</TargetFrameworks>
  </PropertyGroup>
</Project>
```






## サンプルプログラム

### `TreeListView` sample: 

上記の `TreeListView` を使ったサンプル。
多くの要素を追加すると、元の `TreeView` と同様, 非常に遅い。


### Lazy Loading サンプル:

簡単に、PC のディレクトリを表示するのを作ってみた。
`TreeListView` との両立はできなかった。スタイルの優先順の問題に思うが、未解決。

下のサンプルでは `IsExpanded` setter を実装しているが、`IsSelected` setter も実装が必要。



## Lazy Loading (Data Virtualisation)

WPF `TreeView` の lazy loading も、なかなか上手くいかない。探したが、上手くいくサンプルは、次の2つだけか。

 - <a href="https://www.codeproject.com/Articles/1206685/Advanced-WPF-TreeViews-Part-of-n">Advanced WPF TreeViews Part 2 of n</a>
   クラス `FolderBrowserTreeView` を `UserControl` から派生させている。XAML で `TreeView` を wrap. 
   `<HierarchicalDataTemplate>` を view model の型に応じて 3つ。基底クラス `TreeViewItemViewModel` クラスにて, `LoadChildren()` メソッドで子要素を追加。

   それを呼び出しているのは, この添付ビヘイビア:
```xaml
  <TreeView.ItemContainerStyle>
    <Style TargetType="{x:Type TreeViewItem}" BasedOn="{StaticResource {x:Type TreeViewItem}}">
      <Setter Property="behav:TreeViewItemExpanded.Command" Value="{Binding Path=Data.ExpandCommand, Source={StaticResource DataContextProxy}}" />
```
   呼び出されるコマンドは, 結局 `DemoViewModel.cs` ファイルで定義されている。ちょっとやり過ぎでは?
   
 - <a href="https://www.codeproject.com/Articles/26288/Simplifying-the-WPF-TreeView-by-Using-the-ViewMode">Simplifying the WPF TreeView by Using the ViewModel Pattern</a>
   基底クラス `TreeViewItemViewModel` クラスにて, `IsExpanded` プロパティの setter で子要素を読み込む。こちらのほうがシンプル。
```csharp
        public bool IsExpanded
        {
            get { return _isExpanded; }
            set {
                if (value != _isExpanded)
                {
                    _isExpanded = value;
                    this.OnPropertyChanged("IsExpanded");
                }

                // Expand all the way up to the root.
                if (_isExpanded && _parent != null)
                    _parent.IsExpanded = true;

                // Lazy load the child items, if necessary.
                if (this.HasDummyChild)
                {
                    this.Children.Remove(DummyChild);
                    this.LoadChildren();
                }
            }
        }
```

 - <a href="https://hiroronn.hatenablog.jp/entry/20170501/1493640544">WPFのTreeViewで、ツリー展開時に検索を行って子要素を決める - SE（たぶん）の雑感記</a>
   試していないが、ざっくり見た感じ、同じ方法
   
  
コンセプトは同じ。`TreeView.ItemsSource` プロパティに、決まった形の view model を設定する。

 - 仮に expander (左側の▷) を表示させるために、ダミーの子を設定しておく
 - `IsExpanded` プロパティと `IsSelected` プロパティを実装。
 
次のようなスタイルを XAML 側で設定. `BasedOn` 必須. `Mode=TwoWay` も忘れずに。

```xml
  <TreeView.ItemContainerStyle>
    <Style TargetType="{x:Type TreeViewItem}" BasedOn="{StaticResource {x:Type TreeViewItem}}">
      <Setter Property="IsExpanded" Value="{Binding IsExpanded, Mode=TwoWay}" />
      <Setter Property="IsSelected" Value="{Binding IsSelected, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}" />
    </Style>
  </TreeView.ItemContainerStyle>
```



