
using System.Collections.Generic;
using System.Windows.Input;

namespace dotnet_http2_sample
{
// アプリケーションで利用する "コマンド" を並べる. アプリケーショレ内で使い回
// す.
// ここには実行する関数は出てこない。
public static class MyCommands
{
    // CommandBindingCollection クラスは非ジェネリックな IList から派生.
    // このリストを main window の CommandBindings プロパティにコピーして、有効
    // 化する.
    public static readonly List<CommandBinding> CommandBindings =
                                             new List<CommandBinding>();

    // Static にするのがキモ!
    public static readonly RoutedUICommand FetchCommand =
        new RoutedUICommand("取得!", nameof(FetchCommand), typeof(MyCommands));
} // MyCommands

}
