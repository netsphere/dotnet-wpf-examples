
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows;


namespace dotnet_http2_sample
{

// WPF ValidationRule の状態を view model に通知 [汎用ルーチン]
// 添付ビヘイビアは, static class にして, プロパティを `DependencyProperty.RegisterAttached()` で登録する。
// See https://stackoverflow.com/questions/10596452/passing-state-of-wpf-validationrule-to-view-model-in-mvvm
//     Passing state of WPF ValidationRule to View Model in MVVM
//
// リフレクションを使っているので、メソッドの名前が重要. 気づかん
// See https://docs.microsoft.com/en-us/dotnet/desktop/wpf/advanced/attached-properties-overview
public static class ValidationBehavior
{
    //#region Attached Properties

    public static readonly DependencyProperty HasErrorsProperty =
            DependencyProperty.RegisterAttached(
                "HasErrors",
                typeof(bool),
                typeof(ValidationBehavior),
                new FrameworkPropertyMetadata(false,
                        FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                        null, // PropertyChangedCallback を null がポイント
                        CoerceHasError));

    private static readonly DependencyProperty HasErrorsDescriptorProperty =
            DependencyProperty.RegisterAttached(
                "HasErrorsDescriptor",
                typeof(DependencyPropertyDescriptor),
                typeof(ValidationBehavior));

    private static DependencyPropertyDescriptor GetHasErrorsDescriptor(DependencyObject d)
    {
        return (DependencyPropertyDescriptor)d.GetValue(HasErrorsDescriptorProperty);
    }

    private static void SetHasErrorsDescriptor(DependencyObject d, DependencyPropertyDescriptor value)
    {
        d.SetValue(HasErrorsDescriptorProperty, value);
    }

    #region Attached Property Getters and setters

    // リフレクションを使っているので、この名前 (GetXXX, SetXXX) でなければならない. 分からん!
    public static bool GetHasErrors(DependencyObject d)
    {
        return (bool)d.GetValue(HasErrorsProperty);
    }

    public static void SetHasErrors(DependencyObject d, bool value)
    {
        d.SetValue(HasErrorsProperty, value);
    }

    #endregion

    #region CallBacks

    // Callback
    // Validation.HasError と紐付ける
    private static object CoerceHasError(DependencyObject d, object baseValue)
    {
        bool result = (bool)baseValue;
        if (BindingOperations.IsDataBound(d, HasErrorsProperty)) {
            if (GetHasErrorsDescriptor(d) == null) {
                var desc = DependencyPropertyDescriptor.FromProperty(Validation.HasErrorProperty, d.GetType());
                desc.AddValueChanged(d, OnHasErrorChanged);
                SetHasErrorsDescriptor(d, desc);

                // `Validation.HasError` attached property の値を得る.
                result = System.Windows.Controls.Validation.GetHasError(d);
            }
        }
        else {
            if (GetHasErrorsDescriptor(d) != null) {
                var desc = GetHasErrorsDescriptor(d);
                desc.RemoveValueChanged(d, OnHasErrorChanged);
                SetHasErrorsDescriptor(d, null);
            }
        }
        return result;
    }

    private static void OnHasErrorChanged(object sender, EventArgs e)
    {
        var d = sender as DependencyObject;
        if (d != null) {
            d.SetValue(HasErrorsProperty, d.GetValue(Validation.HasErrorProperty));
        }
    }

    #endregion
}

}
