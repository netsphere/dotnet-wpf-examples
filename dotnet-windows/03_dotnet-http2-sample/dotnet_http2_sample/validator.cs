
using System;
using System.Windows.Controls;


namespace dotnet_http2_sample
{
// 文字列があること [汎用ルーチン]
public class ValidatesPresence : ValidationRule
{
    public override ValidationResult Validate(object value,
                            System.Globalization.CultureInfo cultureInfo)
    {
        string b = value as string;
        if ( String.IsNullOrWhiteSpace(b) )
            return new ValidationResult(false, "値が必須");

        return ValidationResult.ValidResult;
    }
}


// URI の妥当性を確認する [汎用ルーチン]
public class ValidatesURI : ValidationRule
{
    public override ValidationResult Validate(object value,
                            System.Globalization.CultureInfo cultureInfo)
    {
        string s = value as string;
        Uri uri;
        if (!s.Contains("://"))
            s = "http://" + s;
        if (!Uri.TryCreate(s, UriKind.Absolute, out uri))
            return new ValidationResult(false, "不正なURI");
        if ( uri.DnsSafeHost == "" )
            return new ValidationResult(false, "URI: ホスト名が必要");

        // DNS調べるのはやりすぎだった.
        // if (Dns.GetHostAddresses(uri.DnsSafeHost).Length > 0)
        return ValidationResult.ValidResult;
    }
} // class ValidatesURI



}
