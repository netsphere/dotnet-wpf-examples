
# dotnet-http2-sample


NuGet パッケージ
  System.Net.Http.WinHttpHandler
  .NET Framework 4.7 で HTTP/2 通信するのに必要。




## 検証器の状態を view model に通知

TextBox などのコントロールには, `Validation.HasError` 添付プロパティがあり,
エラーがある場合に true になる。

しかし、この添付プロパティは bind できない。ので、view model に連携できない。

こういう場合は, 新しく bindable な添付ビヘイビア Behaviour を作ってやる。



Microsoft.Xaml.Behaviors.Wpf パッケージがあるが、これはメリット乏しいので、使う必要ない。

  
