
using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace WPFNotepad.Views
{
    /// <summary>
    /// Interaction logic for HelpDialog.xaml
    /// </summary>
public partial class HelpDialog : Window
{
    public HelpDialog()
    {
        InitializeComponent();
    }

    static void HandleRequestNavigate(string URL)
    {
            Process.Start(new ProcessStartInfo(URL));
    }

    void sourceCodeHL_RequestNavigate(object sender, RoutedEventArgs e)
    {
            string navigateUri = sourceCodeLink.NavigateUri.ToString();
            HandleRequestNavigate(navigateUri);
            e.Handled = true;
        }

    private void OnMouseEnter(object sender, EventArgs e)
    {
            sourceCodeLink.TextDecorations = TextDecorations.Underline;
    }

    private void OnMouseLeave(object sender, EventArgs e)
    {
            sourceCodeLink.TextDecorations = null;
    }
}
}
