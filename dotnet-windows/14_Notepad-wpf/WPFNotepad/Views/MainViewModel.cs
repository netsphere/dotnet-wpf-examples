
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using WPFNotepad.Models;

namespace WPFNotepad.Views
{
    /// <summary>
    /// Main view model for the Notepad application main window.
    /// </summary>
public class MainViewModel : MVVM.BindableBase, IWeakEventListener
{
    // Document that is saved, loaded and hold editor text
    // 参照用
    private DocumentModel _document;

    // Window title
    public string Title {
        get {
            return (_document.IsDirty ? "*" : "") +
                   (_document.FilePath != null ? _document.FileName : "無題") +
                   " - WPF Text Editor";
        }
    }

    // 本来は見えている領域のみのテキスト。手抜き.
    private string _text;
    public string Text {
        get { return _text; }
        set {
            if (SetPropertyAndRaise(ref _text, value))
                RaisePropertyChanged(nameof(Title)) ; // dirty の変更
        }
    }

    private int _cursorPos;
    public int CursorPos {
        get { return _cursorPos; }
        set { SetPropertyAndRaise(ref _cursorPos, value); }
    }

    public FormatModel Format { get; protected set; }


    // コンストラクタ
    public MainViewModel()
    {
        if (Application.Current is MyApp app) { 
            // ドキュメントを view model が所有するのは、適切でない。
            // 単一なら Application 派生クラスに持たせる。
            _document = app.Document;
        }

        Format = new FormatModel();
        InAction = false;
    }

    // `TextChanged` イベントでループするのを防ぐ
    public bool InAction;

    // IWeakEventListener
    bool IWeakEventListener.ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
    {
        var args = (PropertyChangedEventArgs) e;
        switch (args.PropertyName) {
        case "ResetText":
            InAction = true;
            this.Text = _document.Text;
            InAction = false;
            break;
        case "ChangeText":
            InAction = true;
            this.Text = _document.Text; // 手抜き
            this.CursorPos = _document.LastAction.caretIndex;
            InAction = false;
            break;
        case "UndoChangeText":
            InAction = true;
            this.Text = _document.Text; // 手抜き
            this.CursorPos = _document.LastAction.textChange.Offset +
                             _document.LastAction.removedText.Length;
            InAction = false;
            break;
        case "FilePath":
            RaisePropertyChanged(nameof(Title));
            break;
        case "IsDirty":
            RaisePropertyChanged(nameof(Title));
            break;
        default:
            throw new NotImplementedException();
        }

        return true;
    }


    /////////////////////////////////////////////////////////////////
    // Command Handlers

    // 書式 -> 右端で折返す
    public void ToggleWrap(object sender, ExecutedRoutedEventArgs ea)
    {
        if (Format.Wrap == System.Windows.TextWrapping.Wrap)
            Format.Wrap = System.Windows.TextWrapping.NoWrap;
        else
            Format.Wrap = System.Windows.TextWrapping.Wrap;
    }

    public void OpenStyleDialog(object sender, ExecutedRoutedEventArgs ea)
    {
        var fontDialog = new FontDialog();
        fontDialog.DataContext = Format;
        // Modeless dialog
        fontDialog.Show();
    }

}

}
