
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;


namespace WPFNotepad.Views
{

// See https://stackoverflow.com/questions/28233878/how-to-bind-to-caretindex-aka-curser-position-of-an-textbox
// "添付ビヘイビア" の手法.
// <s>DependencyObject 派生でなければならない.</s> -> static class にすればよい.
public static class TextBoxCursorPositionBehavior //: DependencyObject
{
    private const int _CaretIndexDefault = -485609317;

    // Setter. これで `CursorPosition` プロパティが生える
    public static void SetCursorPosition(DependencyObject dependencyObject, int i)
    {
        dependencyObject.SetValue(CursorPositionProperty, i);
    }

    // Getter
    public static int GetCursorPosition(DependencyObject dependencyObject)
    {
        return (int) dependencyObject.GetValue(CursorPositionProperty);
    }

    // `DependencyProperty` は bind できるプロパティ.
    public static readonly DependencyProperty CursorPositionProperty =
                DependencyProperty.RegisterAttached("CursorPosition"
                            , typeof(int)
                            , typeof(TextBoxCursorPositionBehavior)
                            , new FrameworkPropertyMetadata(
                                    _CaretIndexDefault,
                                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                                    OnTrackCaretIndex));

    // Callback
    private static void OnTrackCaretIndex(DependencyObject dependency,
                                          DependencyPropertyChangedEventArgs e)
    {
        // `IS NOT` は C# 9.0 以上
        if (!(dependency is TextBox textBox &&
              e.OldValue is int oldValue && e.NewValue is int newValue)) {
              return ;
        }

        if ( oldValue == _CaretIndexDefault && newValue != _CaretIndexDefault)
            textBox.SelectionChanged += OnSelectionChanged;
        else if ( oldValue != _CaretIndexDefault && newValue == _CaretIndexDefault)
            textBox.SelectionChanged -= OnSelectionChanged;

        if (newValue != textBox.CaretIndex)
            textBox.CaretIndex = newValue;  // これがキモ
    }

    // Callback
    private static void OnSelectionChanged(object sender, RoutedEventArgs e)
    {
        if (sender is TextBox textbox)
            SetCursorPosition(textbox, textbox.CaretIndex); // dies line does nothing
    }
}


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
public partial class MainWindow : Window
{
    // コンストラクタ
    public MainWindow()
    {
        InitializeComponent();

        MainViewModel vm = (MainViewModel) DataContext;

        // [編集] menu
        CommandBindings.Add(
                new CommandBinding(ApplicationCommands.Cut, Cut_Click));
        CommandBindings.Add(
                new CommandBinding(ApplicationCommands.Copy, Copy_Click));
        CommandBindings.Add(
                new CommandBinding(ApplicationCommands.Paste, Paste_Click));
        CommandBindings.Add(
                new CommandBinding(ApplicationCommands.Delete, Delete_Click));

        // TODO: この間に [Clear All]

        CommandBindings.Add(
                new CommandBinding(ApplicationCommands.Find, Find_Click));
        CommandBindings.Add(
                new CommandBinding(ApplicationCommands.SelectAll, SelectAll_Click));

        // [書式] menu
        CommandBindings.Add(
                new CommandBinding(MyCommands.WrapCommand, vm.ToggleWrap));
        CommandBindings.Add(
                new CommandBinding(MyCommands.FormatCommand, vm.OpenStyleDialog));
    }


    /// //////////////////////////////////////////////////////////////
    // Command Handlers

    private void Cut_Click(object sender, ExecutedRoutedEventArgs ea)
    {
            if (text.SelectedText != "")
                text.Cut();
    }

    private void Copy_Click(object sender, ExecutedRoutedEventArgs ea)
    {
            if (text.SelectionLength > 0)
                text.Copy();
    }

    private void Paste_Click(object sender, ExecutedRoutedEventArgs ea)
    {
            if(Clipboard.GetDataObject().GetDataPresent(DataFormats.Text) == true)
            {
                if(text.SelectionLength > 0)
                {
                    text.SelectionStart = text.SelectionStart + text.SelectionLength;
                }    
            }
            text.Paste();
    }

    private void Clear_Click(object sender, RoutedEventArgs e)
    {
            text.Clear();
    }

    private void Find_Click(object sender, ExecutedRoutedEventArgs ea)
    {
        var openFindDialog = new FindDialog();
        openFindDialog.ShowDialog();
    }

    private void Delete_Click(object sender, ExecutedRoutedEventArgs ea)
    {
            text.Text = text.Text.Remove(text.SelectionStart, text.SelectionLength);
    }

    private void SelectAll_Click(object sender, ExecutedRoutedEventArgs ea)
    {
            text.SelectAll();
    }


    void sourceCodeHL_RequestNavigate(object sender, RoutedEventArgs e)
    {
            string navigateUri = sourceCodeLink.NavigateUri.ToString();
            MyApp.HandleRequestNavigate(navigateUri);
            e.Handled = true;
        }

    private void OnMouseEnter(object sender, EventArgs e)
    {
            sourceCodeLink.TextDecorations = TextDecorations.Underline;
        }

    private void OnMouseLeave(object sender, EventArgs e)
    {
            sourceCodeLink.TextDecorations = null;
        }

    private void SendFeedback_Click(object sender, RoutedEventArgs e)
    {
            string feedBackUrl = "https://mail.google.com";
            MyApp.HandleRequestNavigate(feedBackUrl);
            e.Handled = true;
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {

    }

    // イベントハンドラ。コマンドではない。
    private void text_TextChanged(object sender, TextChangedEventArgs e)
    {
        // 手抜き.
        var doc = ((MyApp) Application.Current).Document;
        var vm = (MainViewModel) DataContext;
        if ( !vm.InAction )
            doc.OnTextChanged(sender, e);
    }
}

}
