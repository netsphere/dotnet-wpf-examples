
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using WPFNotepad.Models;
using WPFNotepad.Views;


/*
See <a href="https://stackoverflow.com/questions/21723677/weakeventmanagerteventsource-teventargs-and-propertychangedeventmanager-cause">WeakEventManager<TEventSource, TEventArgs> and PropertyChangedEventManager causes memory leak</a>
  -> `WeakEventManager<,>` is broken という意見もあるが, 別に壊れていなさそう.

PropertyChangedEventManager.AddListener()
   -> CurrentManager.PrivateAddListener()
      -> PropertyChangedEventManager#AddListener()
         -> ここで StartListening() される. リスナを追加するだけでよい.

PropertyChangedEventManager.RemoveHandler()
   -> CurrentManager.PrivateRemoveHandler()
      -> PropertyChangedEventManager#RemoveListener() 第3引数 = null
         -> #RemoveListener(source, propertyName, listener, handler)
            ListenerList<PropertyChangedEventArgs> hlist =
                            ((HybridDictionary) this[source])[propertyName]
            hlist#RemoveHandler(handler)
            -> Target プロパティ (Listener オブジェクト) の一致で見ているか.
*/


namespace WPFNotepad
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
public partial class MyApp : Application
{
    public DocumentModel Document;

    private FileOperation _fileOperation;
    private EditOperation _editOperation;
    private HelpOperation _helpOperation;

    public MyApp()
    {
    }

    public static void HandleRequestNavigate(string URL)
    {
        Process.Start(new ProcessStartInfo(URL));
    }


    private void Application_Startup(object sender, StartupEventArgs e)
    {
        this.Document = new DocumentModel();
        var wnd = new MainWindow();

        _fileOperation = new FileOperation(wnd, Document);
        _editOperation = new EditOperation(wnd, Document);
        _helpOperation = new HelpOperation(wnd);

        // 弱参照にすること!
        var vm = (MainViewModel) wnd.DataContext;
        PropertyChangedEventManager.AddListener(this.Document, vm, "");

        // [File] menu
        wnd.CommandBindings.Add(
                new CommandBinding(ApplicationCommands.Close, OnFileExit));

        wnd.Show();
    }

    // File -> 終了
    private void OnFileExit(object sender, ExecutedRoutedEventArgs e)
    {
        // TODO: ここで、保存しますか? などを出す
        Application.Current.Shutdown();
    }
}


static class MyCommands
{
    // [File] menu

    // [Edit] menu

    // TextBox にフォーカスがあるとき, ApplicationCommands.Undo はその TextBox
    // が処理してしまう。
    //public static readonly ICommand EditUndoCmd =
    //    new RoutedUICommand("Undo", nameof(EditUndoCmd), typeof(MyCommands));

    // [Format] menu /////////////////////////////////////////////////

    public static readonly ICommand WrapCommand =
        new RoutedUICommand("右端で折返す", nameof(WrapCommand), typeof(MyCommands));

    // Format -> Font...
    public static readonly ICommand FormatCommand =
        new RoutedUICommand("フォント変更", nameof(FormatCommand), typeof(MyCommands));

    public static readonly ICommand HelpCommand =
        new RoutedUICommand("About", nameof(HelpCommand), typeof(MyCommands));
}


}
