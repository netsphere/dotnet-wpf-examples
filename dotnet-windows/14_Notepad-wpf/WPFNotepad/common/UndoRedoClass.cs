using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WPFNotepad.Functionality
{

public class UndoRedoFunction<T>
{
    // Stack<T> では途中が取れない
    private List<Tuple<int, T> > _undoStack;

    // 単調増加させる
    private int _counter;

    // 末尾の index を返す.
    // item A のみ -> 保存 -> 積んで戻して, 積んで戻してで同じにする.
    public int Index {
        get {
            return _position == 0 ? -1 : _undoStack[_position - 1].Item1;
        }
    }

    private int _position;

    // コンストラクタ
    public UndoRedoFunction()
    {
        _counter = 1;
        //Index = _counter;

        _undoStack = new List<Tuple<int, T> >();
        _position = 0;
        //RedoStack = new Stack<string>();
    }

    public void Clear()
    {
        if (_undoStack.Count > 0) {
            //Index = _undoStack[0].Item1;
            _undoStack.Clear();
            _position = 0;
        }
            //RedoStack.Clear();
    }

    // Redo 要素を削除してから、追加する.
    public void AddAction(T item)
    {
        _undoStack.RemoveRange(_position, _undoStack.Count - _position);
        _undoStack.Add(new Tuple<int, T>(_counter++, item));
        _position = _undoStack.Count;
            //UndoStack.Push(item);
    }

    // ポインタを一つ戻し, T を返す.
    public T Undo()
    {
        if (_position <= 0)
            throw new InvalidOperationException(nameof(Undo));

        // 削除しないのがキモ.
        return _undoStack[--_position].Item2;
    }

    // いくつ戻せるか
    public int UndoCount {
        get { return _position; }
    }

    // T を返す. ポインタを進める.
    public T Redo()
    {
        if (_position >= _undoStack.Count)
            throw new InvalidOperationException(nameof(Redo));

        //        return UndoStack.First();
            //string item = RedoStack.Pop();
            //UndoStack.Push(item);
        return _undoStack[_position++].Item2;
    }

    public int RedoCount {
        get { return _undoStack.Count - _position; }
    }

    // Undo 可能なアイテム
    public List<T> UndoableActions()
    {
        return _undoStack.GetRange(0, _position).ConvertAll((x) => x.Item2);
    }

    public List<T> RedoableActions()
    {
        return _undoStack.GetRange(_position, _undoStack.Count - _position)
                         .ConvertAll((x) => x.Item2);
    }
}

}
