
using WPFNotepad.Functionality;
using System;
using System.Windows.Input;
using System.Windows;
using WPFNotepad.Models;


namespace WPFNotepad
{

public class EditOperation
{
    private DocumentModel _document;

    // コンストラクタ
    public EditOperation(Window window, DocumentModel document)
    {
        _document = document;

        window.CommandBindings.Add(
                new CommandBinding(ApplicationCommands.Undo, Undo_Click, CanEditUndo));
        window.CommandBindings.Add(
                new CommandBinding(ApplicationCommands.Redo, Redo_Click, CanEditRedo));
    }

    private bool txtAreaTextChangeRequired = true;
    public bool TxtAreaTextChangeRequired {
        get {
                return txtAreaTextChangeRequired;
            }

        set {
                txtAreaTextChangeRequired = value;
            }
    }

        public string DateTime_Now()
        {
            return DateTime.Now.ToString();
        }


    /// ///////////////////////////////////////////////////////////////////
    // Command Handlers

    private void Undo_Click(object sender, ExecutedRoutedEventArgs ea)
    {
        if (_document.CanUndo)
            _document.Undo();
    }

    private void CanEditUndo(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = _document.CanUndo;
    }

    private void Redo_Click(object sender, RoutedEventArgs e)
    {
        if (_document.CanRedo)
            _document.Redo();
    }

    private void CanEditRedo(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = _document.CanRedo;
    }


    public FindNextResult FindNext(FindNextSearch search)
    {
            FindNextResult result = new FindNextResult();
            int position=-1;
            StringComparison s = search.MatchCase ? StringComparison.CurrentCulture :
                StringComparison.CurrentCultureIgnoreCase;
            if (search.Direction == "UP")
            {
                position = search.Content.Substring(0, search.Position)
                    .LastIndexOf(search.SearchString, s);
                search.Success = position >= 0 ? true : false;
                result.SearchStatus = search.Success;
            }else
            {
                int start = search.Success ? search.Position + search.SearchString.Length :
                    search.Position;
                position = start + search.Content
                    .Substring(start, search.Content.Length - start)
                    .IndexOf(search.SearchString, s);
                search.Success = position - start >= 0 ? true : false;
                result.SearchStatus = search.Success;
            }
            result.SelectionStart = result.SearchStatus ? position : -1;
            return result;
        }
    }
}
