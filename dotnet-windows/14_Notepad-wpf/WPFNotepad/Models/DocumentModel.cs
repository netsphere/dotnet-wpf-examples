using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using WPFNotepad.Functionality;


namespace WPFNotepad.Models
{

public class TextChangeAction
{
    public TextChange textChange;

    public string insertedText;

    public string removedText;

    public int caretIndex;
}

    /// <summary>
    /// Model for the text editor document.
    /// </summary>
[Serializable]
public class DocumentModel : INotifyPropertyChanged
{
    // @override INotifyPropertyChanged
    public event PropertyChangedEventHandler PropertyChanged;

    private string _text;
    public string Text {
        get { return _text; }
        // Insert, Delete で編集する.
        protected set {
            if (value != _text) {
                _text = value;
                RaisePropertyChanged("ResetText");
                //RaisePropertyChanged(nameof(IsDirty));
            }
        }
    }

    // 型引数はすごい手抜き.
    private readonly UndoRedoFunction<TextChangeAction> _undoRedo;

    public bool CanUndo {
        get { return _undoRedo.UndoCount > 0; }
    }

    public bool CanRedo {
        get { return _undoRedo.RedoCount > 0; }
    }

    public TextChangeAction LastAction;

    public bool Undo()
    {
        LastAction = _undoRedo.Undo();
        _text = _text.Remove(LastAction.textChange.Offset, LastAction.textChange.AddedLength)
                     .Insert(LastAction.textChange.Offset, LastAction.removedText);
        RaisePropertyChanged("UndoChangeText");

        return true;
    }

    public bool Redo()
    {
        LastAction = _undoRedo.Redo();
        _text = _text.Remove(LastAction.textChange.Offset, LastAction.textChange.RemovedLength)
                     .Insert(LastAction.textChange.Offset, LastAction.insertedText);
        RaisePropertyChanged("ChangeText");
        //RaisePropertyChanged(nameof(IsDirty));

        return true;
    }

    private int _savedUndoIndex;

    // ファイル名を含むフルパス. 新規ファイルのときは null
    private string _filePath;
    public string FilePath {
        get { return _filePath; }
        // SaveAs のときに、ファイル名だけの設定がある。
        set { _filePath = value; RaisePropertyChanged(); }
    }

    // コンストラクタ
    public DocumentModel()
    {
        _undoRedo = new UndoRedoFunction<TextChangeAction>();
        this.Reset(null);
    }

    // @param filePath 新規ファイルのときは null
    public void Reset(string filePath)
    {
        this.FilePath = filePath;
        this.Text = filePath != null ? File.ReadAllText(filePath) : "";
        _undoRedo.Clear();
        ResetDirty();
    }

    //private string _fileName;
    public string FileName {
        get { return Path.GetFileName(_filePath); }
        //set { SetPropertyAndRaise(ref _fileName, value); }
    }

    //private bool _isDirty;
    public bool IsDirty {
        get {
            return _undoRedo.Index != _savedUndoIndex;
        }
    }

    public void ResetDirty()
    {
        _savedUndoIndex = _undoRedo.Index;
        RaisePropertyChanged(nameof(IsDirty));
    }

    // Note. これはコマンドではない.
    public void OnTextChanged(object sender, TextChangedEventArgs e)
    {
        var tb = (TextBox) e.Source;

        //switch (e.UndoAction) {
        // 文字入力のとき, 削除のときも. UndoAction.Merge もある。同一文字種の入力が続いた場合か?
        //case UndoAction.Create: 

        // Paste のときも Changes.Count = 1. 選択して貼り付け (入れ替え) でも.
        if (e.Changes.Count != 1)
            throw new NotSupportedException();

        // `e.Changes` にはテキスト情報が含まれない! なんですと!
        // テキスト挿入のときは AddedLength > 0, 削除のときは RemovedLength > 0
        //   -> Paste のときは 1より大きくなりうる.
        // 選択して貼り付け (入れ替え) の場合, 両方が >0 になる
        TextChange textChange = e.Changes.First();
        var changeAction = new TextChangeAction() {
                    textChange = textChange,
                    insertedText = tb.Text.Substring(textChange.Offset, textChange.AddedLength),
                    removedText = this.Text.Substring(textChange.Offset, textChange.RemovedLength),
                    caretIndex = tb.CaretIndex,
                };
        _undoRedo.AddAction(changeAction);

        _text = tb.Text;
        RaisePropertyChanged(nameof(IsDirty));
    }

    // プロパティ値が変更されたことをリスナに通知する
    protected virtual void RaisePropertyChanged([CallerMemberName] string prop = "")
    {
        PropertyChanged ?.Invoke(this, new PropertyChangedEventArgs(prop));
    }
}


} // of namespace WPFNotepad.Models
