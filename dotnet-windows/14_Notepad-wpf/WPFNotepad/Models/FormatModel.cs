using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace WPFNotepad.Models
{
    /// <summary>
    /// Model for document format properties.
    /// </summary>
public class FormatModel : MVVM.BindableBase
{
    private FontStyle _style;
    public FontStyle Style {
        get { return _style; }
        set {
            SetPropertyAndRaise(ref _style, value);
        }
    }

    private FontWeight _weight;
    public FontWeight Weight {
        get { return _weight; }
        set {
            SetPropertyAndRaise(ref _weight, value);
        }
    }

    private FontFamily _family;
    public FontFamily Family {
        get { return _family; }
        set { SetPropertyAndRaise(ref _family, value); }
    }

    private TextWrapping _wrap;
    public TextWrapping Wrap {
        get { return _wrap; }
        set {
            SetPropertyAndRaise(ref _wrap, value);
            IsWrapped = value == TextWrapping.Wrap ;
        }
    }

    private bool _isWrapped;
    public bool IsWrapped {
        get { return _isWrapped; }
        protected set { SetPropertyAndRaise(ref _isWrapped, value); }
    }

    private double _size;
    public double Size {
        get { return _size; }
        set { SetPropertyAndRaise(ref _size, value); }
    }

    public FormatModel()
    {
        this.Wrap = TextWrapping.Wrap;
    }
}

}
