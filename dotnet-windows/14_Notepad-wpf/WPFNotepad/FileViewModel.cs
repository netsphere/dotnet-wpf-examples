
using Microsoft.Win32;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WPFNotepad.Models;


namespace WPFNotepad
{
    /// <summary>
    /// View model for the file toolbar.
    /// </summary>
public class FileOperation
{
    // 参照のみ
    private DocumentModel _document;

    // コンストラクタ
    public FileOperation(Window window, DocumentModel document)
    {
        _document = document;

        window.CommandBindings.Add(
                new CommandBinding(ApplicationCommands.New, NewFile));
        window.CommandBindings.Add(
                new CommandBinding(ApplicationCommands.Open, OpenFile));
        window.CommandBindings.Add(
                new CommandBinding(ApplicationCommands.Save, SaveFile));
        window.CommandBindings.Add(
                new CommandBinding(ApplicationCommands.SaveAs, SaveFileAs));
    }


    /// /////////////////////////////////////////////////////////////////
    // Command Handlers 

    private void NewFile(object sender, ExecutedRoutedEventArgs ea)
    {
        _document.Reset(null);
    }

    private void OpenFile(object sender, ExecutedRoutedEventArgs ea)
    {
        var openFileDialog = new OpenFileDialog() { 
            Filter = "Text File (*.txt)|*.txt|Log File (*.log)|*.log" };

        if (openFileDialog.ShowDialog() == true) {
            // 読み込み.
            _document.Reset(openFileDialog.FileName);
        }
    }

    private void SaveFile(object sender, ExecutedRoutedEventArgs ea)
    {
        if (_document.FilePath == null || _document.FilePath == "")
            SaveFileAs(sender, ea);
        else { 
            File.WriteAllText(_document.FilePath, _document.Text);
            _document.ResetDirty();
        }
    }

    private void SaveFileAs(object sender, ExecutedRoutedEventArgs ea)
    {
        var saveFileDialog = new SaveFileDialog() { 
            Filter = "Text File (*.txt)|*.txt|Log File (*.log)|*.log" };

        if (saveFileDialog.ShowDialog() == true) {
            File.WriteAllText(saveFileDialog.FileName, _document.Text);

            _document.FilePath = saveFileDialog.FileName;
            _document.ResetDirty();
        }
    }

}
}
