
using System.Windows;
using System.Windows.Input;
using WPFNotepad.Views;


namespace WPFNotepad
{
    /// <summary>
    /// View model for a help dialog.
    /// </summary>
public class HelpOperation
{
    // コンストラクタ
    public HelpOperation(Window window)
    {
        // [Help] menu
        window.CommandBindings.Add(
                new CommandBinding(ApplicationCommands.Help, ViewHelp_Click));
        window.CommandBindings.Add(
                new CommandBinding(MyCommands.HelpCommand, DisplayAbout));
    }

    // Help -> Help
    private void ViewHelp_Click(object sender, RoutedEventArgs e)
    {
        string helpUrl = "https://www.bing.com/search?q=nh%e1%ba%adn+tr%e1%bb%a3+gi%c3%bap+v%e1%bb%81+notepad+trong+windows+10&filters=guid:%224466414-vi-dia%22%20lang:%22vi%22&form=T00032&ocid=HelpPane-BingIA";
        MyApp.HandleRequestNavigate(helpUrl);
        e.Handled = true;
    }

    private void DisplayAbout(object sender, ExecutedRoutedEventArgs ea)
    {
        var helpDialog = new HelpDialog();
        helpDialog.ShowDialog();
    }
}
}
