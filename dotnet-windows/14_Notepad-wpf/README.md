
# Notepad-wpf

This is a C# WPF sample notepad application written by Nguyễn Trí Trường (17521187), Lê Anh Vũ (17521269)

Based on https://github.com/NguyenTruong224/Notepad-wpf/
 - Undo - Redo ができる。<s>が, `TextBox#Undo()` をそのまま使っている。</s> 再実装した。
 - <s>ドキュメントの状態 (未保存) は持っていないようだ。</s> 実装した。
 - Find dialog は未実装
 - <s>キャレット位置の復元が必要。</s> 実装した。




## Undo-Redo の実装

元のオリジナルには, `UndoRedoClass` クラスがあったが、これは実際には使っておらず, 単に `TextBox#Undo()` を呼び出していた。

自作の Undo-Redo クラスを使うようにした。`TextBox.IsUndoEnabled` プロパティを偽にしないと、組み込みのハンドラが使われることに注意。

元の実装は `UndoStack` と `RedoStack` の二つを使っていたが、冗長。ただ一つのリストでよい。
ファイルを保存したときに、そこからさらに undo すると、逆に dirty になることに注意。特に難しくない。

`TextBox` の `TextChanged` イベントのハンドラには, `TextChangedEventArgs` が渡ってくる。`UndoAction` プロパティは `UndoAction.Create` など。これは, 挿入でも削除でも同じで, 役に立たない。
Undo-Redo クラスに挿入・削除イベントを追加していくときに, 挿入したテキスト・元のテキストも加えればよい。



## 添付ビヘイビア

`TextBox` クラスの `Text` プロパティは、代入するとキャレットが先頭になる。Undo-Redo で view model 側から再設定が必要。しかし, `CaretIndex` プロパティは XAML で bind できない。

添付ビヘイビアで新しいプロパティを追加し、`CaretIndex` を更新させればよい。

`DependencyProperty.RegisterAttached()` でつくる。クラスは static class にする。

<a href="https://qiita.com/flasksrw/items/ee57552bf0513601e4fd">添付プロパティと依存関係プロパティの違い</a>



## 弱い参照

model よりも view model のほうが寿命が短い。

モデルクラスの event を view model が listen する場合, 弱参照にしなければならない。
単に listener を "add" すると, イベントソース側 (モデルクラス) がリスナオブジェクトへの参照を保有するため、view model オブジェクトが解放されず, メモリリークする。

The .NET Framework 4.5 Way
<a href="https://www.codeproject.com/Articles/738109/The-NET-Weak-Event-Pattern-in-Csharp">The .NET Weak Event Pattern in C#</a>

.NET Framework 3.0 のやり方:
 `WeakEventManager.ProtectedAddListener()` でリスナを登録する。
 ソースオブジェクトの側に event オブジェクトは不要。発火させるのは, `WeakEventManager.DeliverEvent()` になる。
 

 `PropertyChangedEventManager` class では, `StartListening()` 時にソースオブジェクトの `PropertyChanged` event に イベントマネジャ自身を "add" し, listener は弱参照にする。`_target` を `WeakReference` で持っている。
 これで, ソースオブジェクト側からは, イベントの発火でリスナが呼び出せる。
 See https://referencesource.microsoft.com/#WindowsBase/Base/System/ComponentModel/PropertyChangedEventManager.cs,57861757c54ad63b
 
