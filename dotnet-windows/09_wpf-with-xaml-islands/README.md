
# WinRT API をデスクトップアプリから呼び出す


## このサンプルは何?

XAML Islands (XAML諸島) のサンプル。
 - カメラのプレビューを表示する。`Windows.UI.Xaml.Controls.CaptureElement`
 - `MessageDialog` ポップアップダイアログを表示する。

2023年3月現在, .NET 6 ではまだ XAML 諸島は使えない。.NET Framework 4.7 を使うしかない。

Ref: <a href="https://learn.microsoft.com/en-us/answers/questions/1010047/does-net6-0-support-xaml-island">Does .net6.0 support xaml island?</a> ほか

わざわざ無理して UWP のコントロールを使わなくても、例えば地図であれば https://github.com/judero01col/GMap.NET/ など、代替コントロールを探したほうが健全な気もする。




## .NET Framework 4.7 WPF で UWP コントロールを表示する。

次のいずれかのパッケージをインストールすればよい。 ※.NET Framework, .NET Core 3.x でのみサポート。

.NET 6 プロジェクトでもインストールできてしまうが、ビルドが通らない。
いずれも, リポジトリ [Microsoft.Toolkit.Win32](https://github.com/CommunityToolkit/Microsoft.Toolkit.Win32/) がアーカイブされている。開発終了。.NET 6 以降で使える見込みはない。

 - `Microsoft.Toolkit.Wpf.UI.Controls` NuGet パッケージ. 
   次のXAMLコントロールをサポート:
   + InkCanvas
   + InkToolbar
   + MapControl
   + MediaPlayerElement
   + SwapChainPanel

 - `Microsoft.Toolkit.Wpf.UI.XamlHost` NuGet パッケージ
   上記以外のための汎用XAMLホスト
   注意! このパッケージを入れただけでは, 実行時に `System.Windows.Markup.XamlParseException` 例外が出る。意味がわからない;
         マニフェストファイルがあればよい。エラーメッセージからそんなの分からない。



## Windows UI Library (WinUI) 3

WinUI 3 - Windows App SDK 1.2 は、WPF とはもちろん, UWP とも違う GUI フレームワーク。WPF と併用できない。

-> これがまた上手くいかない。WinUI 3 内に UWP コントロールを埋め込む XAML 諸島も未実装に見える。




### Windows App SDK の混乱

2020年頃 (?) Project Reunion が、自ら分断したデスクトップアプリとUWP との統合を目指した。
リリース時に名称変更して, WinUI 3 となったり、さらに Windows App SDK とコロコロ変わった。
既存のデスクトップアプリ環境で動作し、UWP への投資をデスクトップアプリに移植しやすくした。

UWP から Windows App SDK への移行ガイドも提供されている。<a href="https://learn.microsoft.com/en-us/windows/apps/windows-app-sdk/migrate-to-windows-app-sdk/migrate-to-windows-app-sdk-ovw">Migrate from UWP to the Windows App SDK</a> けっこう本腰をいれた移行ガイドに見える。

UWP にあった機能を Windows App SDK に移植したものが多数ある。例えば UWP 用の `Windows.UI.Input` 名前空間を Windows App SDK では `Microsoft.UI.Input` 名前空間など。これが, 上記の UWP の機能をデスクトップアプリ側から「呼び出す」のと丸被りになってしまう。

用語もさらに混乱している。Runtime (WinRT) API は、もともと UWP 向けのAPIを指していたのに、Windows App SDK が提供するデスクトップアプリ向けの API も WinRT APIs と呼んでいる。アホとちゃうか。

とにかく、UWP を畳むのは確実なので、まんまと UWP で作ってしまっていたものがあった場合は、さっさと移行するのがよさそう。





## UWP WinRT API を呼び出す

一部の WinRT 名前空間やクラスは .NET 6 から利用できない。利用できないにも2種類あって,

 1. .NET から名前空間/クラスが見えないのでコンパイルが通らない。これは移行先のを使えばよい。
See <a href="https://learn.microsoft.com/ja-jp/windows/apps/desktop/modernize/desktop-to-uwp-enhance">デスクトップ アプリで Windows ランタイム API を呼び出す</a>

|     WinRT クラス                    |     移行先                           |
|-------------------------------------|--------------------------------------|
|`Windows.UI.Colors` class            |`Microsoft.UI.Colors` class           |
|`Windows.UI.ColorHelper` class       |                                      |
|`Windows.UI.Text` (all classes in this namespace *except* for<br /><ul><li>`Windows.UI.Text.FontStretch`, <li>`Windows.UI.Text.FontStyle`, <li>`Windows.UI.Text.FontWeight`, <li>`Windows.UI.Text.UnderlineType`, and <li>all classes under the `Windows.UI.Text.Core` namespace) | |
|`Windows.UI.Xaml` (all classes in this namespace) |`Microsoft.UI.Xaml` namespace  |

 2. もう一つは、UWP WinRT API が見えてコンパイルできるが、実行環境が UWP でないため、実行時に失敗するもの。こちらのほうが厄介。むしろコンパイルエラーにしてほしい。
リスト; <a href="https://learn.microsoft.com/en-us/windows/apps/desktop/modernize/desktop-to-uwp-supported-api">Windows Runtime APIs not supported in desktop apps</a>


しかし、UWP がまだ終了していないので、これらのクラスや名前空間は、ドキュメント上、非推奨と書いておらず、混乱が極まっている。




