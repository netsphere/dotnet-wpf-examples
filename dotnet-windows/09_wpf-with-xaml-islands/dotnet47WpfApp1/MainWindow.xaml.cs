
using Microsoft.Toolkit.Wpf.UI.XamlHost;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;
using Windows.UI.Popups;
//using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
//using static System.Net.Mime.MediaTypeNames;


namespace dotnet47WpfApp1
{

[ComImport]
[Guid("3E68D4BD-7135-4D10-8018-9FB6D9F33FA1")]
[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
public interface IInitializeWithWindow 
{
    void Initialize( IntPtr hwnd );
}


    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
public partial class MainWindow : Window
{
    private CaptureElement _captureControl;

    private MediaCapture _mediaCapture1;

    private IntPtr _hwnd;


    public MainWindow()
    {
/* マニフェストがないと次のエラーが発生する;
  System.Windows.Markup.XamlParseException
    HResult=0x80131501
    Message='指定されたバインディング制約に一致する型 'Microsoft.Toolkit.Wpf.UI.XamlHost.WindowsXamlHost' 
    のコンストラクターの呼び出しで例外がスローされました。' 行番号 '20'、行位置 '6'。
  WindowsXamlManager and DesktopWindowXamlSource are supported for apps 
  targeting Windows version 10.0.18226.0 and later.  Please check either the 
  application manifest or package manifest and ensure the MaxTestedVersion 
  property is updated.

メッセージが非常に惑わせるものになっている。
ドキュメント https://learn.microsoft.com/ja-jp/windows/apps/desktop/modernize/using-the-xaml-hosting-api
でも「アプリの MSIX パッケージを作成し、それをパッケージから実行するか、または 
Microsoft.Toolkit.Win32.UI.SDK NuGet パッケージをプロジェクトにインストールし
ます。」とあるが、これは正しくない。
正解は、単にマニフェストを追加すればよい。
*/
        InitializeComponent();
    }

    private void WindowsXamlHost_ChildChanged(object sender, EventArgs e)
    {
        var host = (WindowsXamlHost)sender;
        if (_captureControl == null) {
            _captureControl = (Windows.UI.Xaml.Controls.CaptureElement)host.Child;
            _captureControl.Stretch = Windows.UI.Xaml.Media.Stretch.Uniform;
        }
    }

    // Start preview
    private async void Button_Click(object sender, RoutedEventArgs e)
    {
        _mediaCapture1 = new MediaCapture();
        await _mediaCapture1.InitializeAsync();

        // Start capture preview
        _captureControl.Source = _mediaCapture1;
        await _mediaCapture1.StartPreviewAsync();
    }

    // Capture
    private async void Button_Click_1(object sender, RoutedEventArgs e)
    {
        var format = ImageEncodingProperties.CreatePng();
        var ms = new MemoryStream();
        var stream = ms.AsRandomAccessStream();
        // 第2引数は IRandomAccessStream 
        await _mediaCapture1.CapturePhotoToStreamAsync(format, stream);
        await stream.FlushAsync();
        ms.Position = 0;
        var source = new BitmapImage();
        source.BeginInit();
        source.CacheOption = BitmapCacheOption.OnLoad;
        source.StreamSource = ms;
        source.EndInit();

        imageControl.Source = source;
    }


    // Popup
    private async void Button_Click_2(object sender, RoutedEventArgs e)
    {
        // WinRT Windows.UI.Popups 名前空間 -> 非推奨.
        //   -> 移行先 ContentDialog クラスは `Windows.UI.Xaml.Controls` 名前空間.
        //      .NET 6 では使えない。
        var dlg = new MessageDialog("はろーわーるど");

        // 次の文がないと例外発生
        // System.Runtime.InteropServices.COMException: 'Invalid window handle. (0x80070578)
        // Consider WindowNative, InitializeWithWindow
        // See https://learn.microsoft.com/ja-jp/windows/apps/develop/ui-input/display-ui-objects
        //WinRT.Interop.InitializeWithWindow.Initialize(dlg, _hwnd);
        ((IInitializeWithWindow) (object) dlg).Initialize(_hwnd);

        await dlg.ShowAsync();
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
        // これでは System.InvalidCastException: 'Specified cast is not valid.'
        //_hwnd = WinRT.Interop.WindowNative.GetWindowHandle(this);
        _hwnd = new WindowInteropHelper(Application.Current.MainWindow).Handle;
    }


    // Get LocalFolder
    private void Button_Click_3(object sender, RoutedEventArgs e)
    {
        string path;
        // コンパイルできるが、実行時に例外発生: System.InvalidOperationException
        //     Message=プロセスにパッケージ ID がありません。Source=Windows.Storage
        try { 
            path = Windows.Storage.ApplicationData.Current.LocalFolder.Path;
        } catch (Exception ex) {
            Console.WriteLine(ex.Message);
            // C:\Users\hhori\AppData\Roaming
            path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        }
        Console.WriteLine("LocalFolder = {0}", path);
    }
}

}
