﻿
using System.ComponentModel.DataAnnotations;


namespace DrawingApp1.Models
{

public enum ShapeType {
    DOT = 1,
    LINES = 2,
}

public class MyShape
{
    [Required]
    public ShapeType Type { get; set; }

    public System.Drawing.Color Color { get; set; }

    public int Thickness { get; set; }
}

public class DrawDot : MyShape {
    public System.Drawing.Point Point { get; set; }
}

public class DrawLines : MyShape {
    // 点が2つ以上
    public System.Drawing.Point[] Points { get; set; }
}


public class Scene
{
    public event EventHandler<EventArgs> SceneChanged ;

    public List<MyShape> Shapes { get; set; }

    public Scene()
    {
        Shapes = new List<MyShape>();
    }

    public void AddShape(MyShape shape)
    {
        Shapes.Add(shape);
        SceneChanged ?.Invoke(this, EventArgs.Empty);
    }
}

}
