
using DrawingApp1.Models;


namespace DrawingApp1.Views
{

class MainViewModel : MVVM.BindableBase
{
    Scene _scene;
    public Scene Scene {
        get { return _scene; }
        set {
            if (value == null)
                throw new ArgumentNullException("internal error: `Scene` cannot be null");
            _scene = value;
            RaisePropertyChanged(nameof(Scene));
        }
    }

    System.Drawing.Color _selectedColor;
    public System.Drawing.Color SelectedColor {
        get { return _selectedColor; }
        set { SetPropertyAndRaise(ref _selectedColor, value); }
    }

    System.Drawing.Color _secondaryColor;
    public System.Drawing.Color SecondaryColor {
        get { return _secondaryColor; }
        set { SetPropertyAndRaise(ref _secondaryColor, value); }
    }

    bool _panMode;
    public bool PanMode {
        get { return _panMode; }
        set { SetPropertyAndRaise(ref _panMode, value); }
    }
}


}
