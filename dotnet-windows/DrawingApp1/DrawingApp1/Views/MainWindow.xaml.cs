
using DrawingApp1.Models;
using System.Windows;
using System.Windows.Input;


namespace DrawingApp1.Views
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
public partial class MainWindow : Window
{
    System.Drawing.Rectangle _canvas_rect;
    System.Drawing.Rectangle _viewport_rect;

    List<System.Drawing.Point> _points;


    public MainWindow()
    {
        InitializeComponent();

        // HSV (=HSB) 色空間は,色相, 彩度, 明度で表す。Adobe のソフトで使われる
        // HSL (=HLS) は, 明度に代えて輝度を使う。CSS ほかで使われる. カラーのモノクロ化は、輝度を使ったほうが人間に自然。
        // 例えば https://personal.canon.jp/product/camera/software/picturestyle-ed/picturestyle/thinking-colormanagement
        colorPicker.PickerType = ColorPicker.Models.PickerType.HSL; 
    }


    void update_scrollbar()
    {
        _canvas_rect.X = Math.Min(_canvas_rect.X, _viewport_rect.X);
        _canvas_rect.Y = Math.Min(_canvas_rect.Y, _viewport_rect.Y);
        _canvas_rect.Width = Math.Max(_canvas_rect.Width,
                    (_viewport_rect.X - _canvas_rect.X) + _viewport_rect.Width);
        _canvas_rect.Height = Math.Max(_canvas_rect.Height,
                    (_viewport_rect.Y - _canvas_rect.Y) + _viewport_rect.Height);

        if (_canvas_rect.Width <= 0 || _canvas_rect.Height <= 0)
            return;

        sb_h.Minimum = _canvas_rect.X + _viewport_rect.Width / 2;
        sb_h.Maximum = _canvas_rect.X + _canvas_rect.Width - _viewport_rect.Width / 2;
        // これが正解. なにこれ
        // See https://stackoverflow.com/questions/3116287/setting-the-scrollbar-thumb-size
        double thumbSize = sb_h.Track.ActualWidth *
                            _viewport_rect.Width / _canvas_rect.Width;
        sb_h.ViewportSize = thumbSize * (sb_h.Maximum - sb_h.Minimum) /
                            (sb_h.Track.ActualWidth - thumbSize + 1);

        sb_v.Minimum = _canvas_rect.Y + _viewport_rect.Height / 2;
        sb_v.Maximum = _canvas_rect.Y + _canvas_rect.Height - _viewport_rect.Height / 2;
        thumbSize = sb_v.Track.ActualHeight *
                    _viewport_rect.Height / _canvas_rect.Height;
        sb_v.ViewportSize = thumbSize * (sb_v.Maximum - sb_v.Minimum) /
                            (sb_v.Track.ActualHeight - thumbSize + 1);

        sb_h.Value = _viewport_rect.X + _viewport_rect.Width / 2;
        sb_v.Value = _viewport_rect.Y + _viewport_rect.Height / 2;
    }

    // 右下にマウスを動かすと, viewport は左上に動く
    void move_offset(int dx, int dy)
    {
        _viewport_rect.X -= dx;
        _viewport_rect.Y -= dy;

        canvas_text.Text = _canvas_rect.ToString();
        viewport_text.Text = _viewport_rect.ToString();

        update_scrollbar();

        drawControl.Position = new System.Drawing.Point(_viewport_rect.X,
                                                        _viewport_rect.Y);
        drawControl.InvalidateVisual();
    }


    /// ///////////////////////////////////////////////////////////////////
    // Event Handlers

    bool _pressed ;
    System.Drawing.Point _lastPoint;

    void Canvas_MouseDown(object sender, MouseButtonEventArgs ea)
    {
        Mouse.Capture(drawControl);
        _pressed = true;

        System.Drawing.Point pt = Ext.pointFromSWPoint(ea.GetPosition(drawControl));

        MainViewModel vm = (MainViewModel) DataContext;
        if (!vm.PanMode) {
            _points = new List<System.Drawing.Point>() {
                            new System.Drawing.Point(pt.X + _viewport_rect.X,
                                                     pt.Y + _viewport_rect.Y) };
        }

        _lastPoint = pt;
    }

    void Canvas_MouseMove(object sender, MouseEventArgs ea)
    {
        if (!_pressed)
            return;

        System.Drawing.Point pt = Ext.pointFromSWPoint(ea.GetPosition(drawControl));

        MainViewModel vm = (MainViewModel) DataContext;
        if (!vm.PanMode) {
            _points.Add(new System.Drawing.Point(pt.X + _viewport_rect.X,
                                                 pt.Y + _viewport_rect.Y));
        }
        else {
            var dx = pt.X - _lastPoint.X;
            var dy = pt.Y - _lastPoint.Y;
            move_offset(dx, dy);
        }

        _lastPoint = pt;
    }

    void Canvas_MouseUp(object sender, MouseButtonEventArgs e)
    {
        if (!_pressed)
            return;

        Mouse.Capture(null);
        _pressed = false;

        MainViewModel vm = (MainViewModel) DataContext;
        if (!vm.PanMode) {
            if (_points.Count == 1) {
                vm.Scene.AddShape(new DrawDot {
                        Type = ShapeType.DOT, Thickness = 15,
                        Color = vm.SelectedColor,
                        Point = _points[0]} );
            }
            else if (_points.Count > 1) {
                vm.Scene.AddShape(new DrawLines {
                        Type = ShapeType.LINES, Thickness = 3,
                        Color = vm.SelectedColor,
                        Points = _points.ToArray()} );
            }
            _points.Clear();
        }
    }


    void drawControl_SizeChanged(object sender, SizeChangedEventArgs e)
    {
        _viewport_rect = new System.Drawing.Rectangle(
                                _viewport_rect.X, _viewport_rect.Y,
                                (int) e.NewSize.Width, (int) e.NewSize.Height);
        update_scrollbar();
    }


    void drawControl_Loaded(object sender, RoutedEventArgs e)
    {
        // (Width, Height) を設定すると, 領域がその大きさになる。
        // (ActualWidth, ActualHeight) もそれに釣られて、viewport ではなくなる。
        // ウィンドウの大きさを変更しても, 固定
        //
        // (Width, Height) を設定しない場合, (ActualWidth, ActualHeight) は
        // viewport になり、ウィンドウの大きさ変更に追従する。

        //drawControl.Height = 1000; //drawControl.ActualHeight * 2;
        //drawControl.Width  = 1000; //drawControl.ActualWidth * 2;
        _viewport_rect = new System.Drawing.Rectangle(0, 0,
                                (int) drawControl.ActualWidth, (int) drawControl.ActualHeight);
        _canvas_rect = new System.Drawing.Rectangle(0, 0,
                                _viewport_rect.Width, _viewport_rect.Height);

        update_scrollbar();
    }

}
}
