
using DrawingApp1.Models;
using System.Windows;


namespace DrawingApp1
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
public partial class MyApp : Application
{
    static Scene _document;

    private void Application_Startup(object sender, StartupEventArgs e)
    {
        _document = new Scene();

        var wnd = new DrawingApp1.Views.MainWindow();
        Views.MainViewModel viewModel = (Views.MainViewModel) wnd.DataContext;
        viewModel.Scene = _document;

        wnd.Show();
    }
}

}
