
using DrawingApp1.Models;
using System.Windows;
using System.Windows.Controls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Media.Imaging;

/*
描画は、2系統ある
1. System.Drawing.Graphics クラス.  <- 基本はこっちを使う
  .NET Framework 1.1 からある。.NET Standard 2.0 でもある
     DrawImage()
     DrawLine()
     DrawLines(System.Drawing.Pen pen, params System.Drawing.Point[] points)
     DrawPath()
     DrawPie()
     ...
    Pen = System.Drawing.Pen class
    Point = System.Drawing.Point struct

2. System.Windows.Media.DrawingContext class
   .NET Framework 3.0 で導入。
     DrawImage()
     DrawLine(Pen, Point, Point)
     DrawRectangle()
     DrawText()
     ...
    Pen = System.Windows.Media.Pen
    Point = System.Windows.Point struct

*/

namespace DrawingApp1
{
    /// このカスタム コントロールを XAML ファイルで使用するには、手順 1a または 1b の後、手順 2 に従います。
    ///
    /// 手順 1a) 現在のプロジェクトに存在する XAML ファイルでこのカスタム コントロールを使用する場合
    /// この XmlNamespace 属性を使用場所であるマークアップ ファイルのルート要素に
    /// 追加します:
    ///
    ///     xmlns:MyNamespace="clr-namespace:DrawingApp1"
    ///
    ///
    /// 手順 1b) 異なるプロジェクトに存在する XAML ファイルでこのカスタム コントロールを使用する場合
    /// この XmlNamespace 属性を使用場所であるマークアップ ファイルのルート要素に
    /// 追加します:
    ///
    ///     xmlns:MyNamespace="clr-namespace:DrawingApp1;assembly=DrawingApp1"
    ///
    /// また、XAML ファイルのあるプロジェクトからこのプロジェクトへのプロジェクト参照を追加し、
    /// リビルドして、コンパイル エラーを防ぐ必要があります:
    ///
    ///     ソリューション エクスプローラーで対象のプロジェクトを右クリックし、
    ///     [参照の追加] の [プロジェクト] を選択してから、このプロジェクトを参照し、選択します。
    ///
    ///
    /// 手順 2)
    /// コントロールを XAML ファイルで使用します。
    ///
    ///     <MyNamespace:draw_control/>
    ///
// カスタムコントロールは `System.Windows.Controls.Control` クラスから派生させる
public class DrawControl : Control
{
    static DrawControl()
    {
        DefaultStyleKeyProperty.OverrideMetadata(typeof(DrawControl), 
                        new FrameworkPropertyMetadata(typeof(DrawControl)));
    }

    // Scene for the drawing control.
    public static readonly DependencyProperty SceneProperty =
            DependencyProperty.Register("Scene", typeof(Scene), 
                    typeof(DrawControl),
                    new PropertyMetadata(new PropertyChangedCallback(ScenePropertyChanged)));

    // `Size` も System.Windows と System.Drawing の2系統ある。後者は int.
    public static readonly DependencyProperty ActualSizeProperty =
            DependencyProperty.Register("ActualSize", typeof(System.Drawing.Size),
                                        typeof(DrawControl));

    // 依存プロパティが変更された. XAML 上で `Scene` プロパティを bind したとき
    static void ScenePropertyChanged(DependencyObject d, 
                                     DependencyPropertyChangedEventArgs e)
    {
        if (e.NewValue != null) {
            ((Scene) e.NewValue).SceneChanged += ((DrawControl) d).OnSceneChanged;
        }
        ((DrawControl) d).InvalidateVisual();
    }

    // binding に必要
    public Scene Scene {
        get {
            return (Scene) GetValue(SceneProperty);
        }
        set { 
            SetValue(SceneProperty, (Scene) value); 
        }
    }

    // Callback: Invalidate the scene.
    void OnSceneChanged(object? sender, EventArgs e)
    {
        this.InvalidateVisual();
    }

    protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
    {
        base.OnPropertyChanged(e);

        if (e.Property == ActualHeightProperty || 
                                        e.Property == ActualWidthProperty) {
            SetValue(ActualSizeProperty, 
                     new System.Drawing.Size((int) ActualWidth, (int) ActualHeight));
        }
    }
    

    // viewport position
    public System.Drawing.Point Position = new System.Drawing.Point(0, 0);

    void draw_shape(MyShape shape, Graphics gfx)
    {
        switch (shape) { 
        case DrawDot dot:
            using (var brush = new SolidBrush(shape.Color)) { 
                var rect = new RectangleF(dot.Point.X - dot.Thickness / 2 - Position.X, // x
                                          dot.Point.Y - dot.Thickness / 2 - Position.Y, // y
                                          dot.Thickness, dot.Thickness); // w, h
                gfx.FillEllipse(brush, rect);
            }
            break;
        case DrawLines lines:
            using (var brush = new SolidBrush(shape.Color))
            using (var pen = new Pen(brush, lines.Thickness) { 
                                StartCap = LineCap.Round,
                                EndCap   = LineCap.Round,
                                LineJoin = LineJoin.Round }) {
                // ドキュメントと異なり, 2点より少ないとエラー
                // System.ArgumentException: 'Parameter is not valid.'
                //gfx.DrawLines(pen, lines.Points);
                for (int i = 1; i < lines.Points.Length; ++i) { 
                    gfx.DrawLine(pen, 
                            new System.Drawing.Point(lines.Points[i - 1].X - Position.X, 
                                     lines.Points[i - 1].Y - Position.Y),
                            new System.Drawing.Point(lines.Points[i].X - Position.X,
                                        lines.Points[i].Y - Position.Y));
                }
            }
            break;
        }
    }

    // @override UIElement
    // 自前で描画するときは, このメソッドを override すればよい.
    protected override void OnRender(System.Windows.Media.DrawingContext drawingContext)
    {
        base.OnRender(drawingContext);

        // clear
        //drawingContext.DrawRectangle(new SolidColorBrush(Colors.White), 
        //                             null, // pen
        //                             new Rect(0, 0, ActualWidth, ActualHeight));
        if (ActualWidth == 0 || ActualHeight == 0)
            return; 

        using (var bmp = new Bitmap((int) (ActualWidth + 0.9), (int) (ActualHeight + 0.9))) { 
            using (var gfx = Graphics.FromImage(bmp)) { 
                gfx.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                gfx.Clear(System.Drawing.Color.LightGray);

                // (Width, Height) を設定すると, 領域がその大きさになる。
                // この場合, (ActualWidth, ActualHeight) は、viewport でなくなる
                // これはダメだ。
                using (var pen = new Pen(Color.Aqua)) {
                    gfx.DrawRectangle(pen, 5, 5, (int) ActualWidth - 10, 
                                      (int) ActualHeight - 10);
                }

                Scene scene = (Scene) GetValue(SceneProperty);
                if (scene == null)
                    goto L1;

                foreach (MyShape shape in scene.Shapes) 
                    draw_shape(shape, gfx);
            }
        L1:
            // ここで 1ドット小さくなることがある?  ⇒ここでは変化しない
            BitmapSource s = Ext.ConvertFromBitmap(bmp);
            //if (s.Width != ActualWidth || s.Height != ActualHeight) {
            //    throw new Exception("internal error"); 端数で例外がある
            //}
            drawingContext.DrawImage(s, new Rect(0, 0, ActualWidth, ActualHeight));
        }
    }

}

}
