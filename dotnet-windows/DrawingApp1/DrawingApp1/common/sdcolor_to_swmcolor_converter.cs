﻿
using System.Globalization;
using System.Windows.Data;


namespace DrawingApp1
{

[ValueConversion(typeof(System.Drawing.Color),        // sourceType
                 typeof(System.Windows.Media.Color))] // targetType
internal class SDColorToSWMColorConverter : IValueConverter
{
    // source (viewmodel) to the target (WPF element)
    // @override IValueConverter
    public object Convert(object value, Type targetType, object parameter, 
                          CultureInfo culture)
    {
        System.Drawing.Color color = (System.Drawing.Color) value;
        System.Windows.Media.Color converted = 
                System.Windows.Media.Color.FromArgb(color.A, color.R, color.G, color.B);
        return converted;
    }

    // @override IValueConverter
    public object ConvertBack(object value, Type targetType, object parameter, 
                              CultureInfo culture)
    {
        System.Windows.Media.Color color = (System.Windows.Media.Color) value;
        System.Drawing.Color converted = 
                System.Drawing.Color.FromArgb(color.A, color.R, color.G, color.B);
        return converted;
    }
}

}
