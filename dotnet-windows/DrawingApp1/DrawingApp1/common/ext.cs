
using System.Windows;
using System.Windows.Media.Imaging;


namespace DrawingApp1
{

// `NativeMethods` という名前にしないといけない
static class NativeMethods
{
    [System.Runtime.InteropServices.DllImport("gdi32.dll")]
    public static extern bool DeleteObject(IntPtr hObject);
}


static class Ext
{
    public static System.Drawing.Point pointFromSWPoint(
                                                System.Windows.Point point)
    {
        return new System.Drawing.Point((int) point.X, (int) point.Y);
    }


    // このページにベンチマークあり:
    // https://zenn.dev/nuits_jp/articles/2023-12-03-convert-bitmap-to-bitmapsource
    // "BitmapからBitmapSourceへ変換するベストプラクティス"

    // 1. `BitmapData` を使う。
    // From https://stackoverflow.com/questions/30727343/fast-converting-bitmap-to-bitmapsource-wpf
    // 上記ページでもこれがお勧め。
    // ▲HiDPI ではこのままでは右下にズレる。150% 補正が必要
    public static BitmapSource ConvertFromBitmap__(System.Drawing.Bitmap bitmap)
    {
        var bitmapData = bitmap.LockBits(
                    new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                    System.Drawing.Imaging.ImageLockMode.ReadOnly,
                    bitmap.PixelFormat);
        try {
            var bitmapSource = BitmapSource.Create(
                        bitmapData.Width, bitmapData.Height,
                        bitmap.HorizontalResolution, // ここが不味い
                        bitmap.VerticalResolution,
                        System.Windows.Media.PixelFormats.Bgr24, null,
                        bitmapData.Scan0, bitmapData.Stride * bitmapData.Height,
                        bitmapData.Stride);
            return bitmapSource;
        }
        finally {
            bitmap.UnlockBits(bitmapData);
        }
    }

/*
    // まとめ記事
    // https://qiita.com/YSRKEN/items/a24bf2173f0129a5825c
    // このやり方は HiDPI でもそのまま OK.
    // 遅い。これならまだ `CreateBitmapSourceFromHBitmap()` がよい。
    public static BitmapSource ConvertFromBitmap(System.Drawing.Bitmap bitmap)
    {
        // MemoryStreamを利用した変換処理
        using (var ms = new System.IO.MemoryStream()) {
            // MemoryStreamに書き出す
            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
	        // MemoryStreamをシーク
	        ms.Seek(0, System.IO.SeekOrigin.Begin);
	        // MemoryStreamからBitmapFrameを作成
	        // (BitmapFrameはBitmapSourceを継承しているのでそのまま渡せばOK)
            return System.Windows.Media.Imaging.BitmapFrame.Create(
			        ms,
			        System.Windows.Media.Imaging.BitmapCreateOptions.None,
			        System.Windows.Media.Imaging.BitmapCacheOption.OnLoad );
        }
    }
*/
    // 次点.
    public static BitmapSource ConvertFromBitmap(System.Drawing.Bitmap bitmap)
    {
        IntPtr hBitmap = bitmap.GetHbitmap();
        BitmapSource source;
        try {
            source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                            hBitmap, IntPtr.Zero, Int32Rect.Empty,
                            BitmapSizeOptions.FromEmptyOptions());
        }
        finally {
            NativeMethods.DeleteObject(hBitmap); // これがないとメモリリーク
        }
        return source;
    }
}

}
