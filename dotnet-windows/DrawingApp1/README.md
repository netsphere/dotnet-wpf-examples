
未了: マウスボタンを離すまで、線が描画されない。


# DrawingApp1

WPF でのグラフィクスのサンプル。



## `Shape` オブジェクト, `System.Drawing` 名前空間と `System.Windows.Media` 名前空間

WPF のグラフィクスは、いくつか選択肢がある。高レベルか低レベルかが分かれ目。低レベルの場合は, `System.Drawing` を使う。


WPF を使わない .NET コンソールアプリケィションで, Windows 以外でも動かしたい場合は, SkiaSharp を使え。

`Microsoft.Maui.Graphics` (`Microsoft.Maui.Graphics.dll` アセンブリ) という手もあるかもしれない
<a href="https://blog.masudaq.com/items/00ba96d8-021f-4f08-b8a0-75ea9edfce73">System.Drawing.Common の代替として Microsoft.Maui.Graphics を Linux で利用する</a>






### 高レベルの `Shape` クラス (このサンプルでは扱わない)

`System.Windows.Shapes` 名前空間.
矩形や楕円などの図形。ベクタグラフィクスとして描画される。

`<Canvas>` 要素の子として追加できる。z-order は `<Canvas>` にお任せ.


 
### `DrawingContext` クラスで描画

`System.Windows.Media` 名前空間, `PresentationCore.dll` アセンブリ。なので、WPF 専用. .NET Framework v3.0 で導入。

`DrawRectangle()` メソッドなどで描画する。低レベル。

`System.Drawing` 名前空間の各クラスを再利用せず、全部新規に起こしているため、名前がことごとく衝突する。ひどい
 



### `System.Drawing.Graphics` クラスで描画

`System.Drawing.Common.dll` アセンブリ。.NET Framework v1.1 からある。

.NET 5 とそれ以前のヴァージョンでは, Windows, Linux, macOS で動作していた。
.NET 6 の破壊的変更で, .NET 6 以降では Windows OS専用になった。.NET 7 で Windows 以外のサポートが完全に廃止。







