
# データベースアプリケィション

Based on https://www.codeproject.com/Articles/24940/AdventureWorks-WPF-Part-1

Part 1 だが part 2 はなさそう。



## How to run

### データベースの復元

データベースは AdventureWorks ではなく, AdventureWorks LT. スキーマが異なる。

 1. ここからダウンロード: <a href="https://github.com/microsoft/sql-server-samples/releases/tag/adventureworks">AdventureWorks サンプル データベース</a>. `AdventureWorksLT2019.bak` ファイル
 
 2. 復元の手順はこちら; https://learn.microsoft.com/ja-jp/sql/samples/adventureworks-install-configure?view=sql-server-ver16&tabs=ssms
    接続先 (サーバ名): `(localdb)\MSSQLLocalDB`

install-script を使う方法は、LT がつかない方になってしまうため、アプリケィションがスキーマ不一致で例外になる。

SQL Server Management Studio (SSMS) を使え.


### 設定

データベース接続先は、アプリケィションの設定ファイルにある。





## DBML ファイル

昔は, データベースから DBML ファイルを作り, そこからさらに C# クラスを生成していたようだ。
"LINQ to SQL" と呼ばれる仕組みか。LINQ to SQL は .NET Framework 3.5 (2007年) で導入された。`System.Data.Linq.DataContext` クラスから data context クラスを自動生成させる。

LINQ to SQL は Entity Framework の競合で、2008 年頃には Entity Framework のほうが推奨だったようだ。
https://www.infoq.com/jp/news/2008/11/DLINQ-Future/

◆Database First
EntityFramework 6 はデータベースからモデルをリバースエンジニアリング。Visual Studio 2012 のころ。
https://learn.microsoft.com/ja-jp/ef/ef6/modeling/designer/workflows/database-first

◆EF Core
EF Core のリバースエンジニアリングは、あくまで model クラスのコードを生成するもの。一回生成したら、以降は Code First 開発を想定。
一応, <kbd>--force</kbd> オプションで再同期は可能。

開発者用PowerShell 上で,

<pre>
> <s><kbd>dotnet tool update --global dotnet-ef</kbd></s> これは不要では?
</pre>


パッケージマネジャーコンソール上で,

<pre>
PM> <kbd>Scaffold-DbContext "Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=AdventureWorksLT2019;Integrated Security=True" Microsoft.EntityframeworkCore.SqlServer -o Models</kbd>
Build started...
Build succeeded.
The column '<code>SalesLT.SalesOrderHeader.OnlineOrderFlag</code>' would normally be mapped to a non-nullable bool property, but it has a default constraint. Such a column is mapped to a nullable bool property to allow a difference between setting the property to false and invoking the default constraint. See https://go.microsoft.com/fwlink/?linkid=851278 for details.
</pre>



