
using AdventureWorks.WPF.Models;
using System.Windows;

namespace AdventureWorks.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
public partial class MyApp : Application
{
    public static AdventureWorksLT2019Context DbContext;

    private void Application_Startup(object sender, StartupEventArgs e)
    {
        if (DbContext == null) {
            DbContext = new AdventureWorksLT2019Context();
            //DbContext.Database.EnsureCreated();  // EF Core のみ.
        }
        
        var wnd = new Window1();
        wnd.Show();
    }
}
}
