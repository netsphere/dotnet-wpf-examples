using AdventureWorks.WPF.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;


namespace AdventureWorks.WPF
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
public partial class Window1 : Window
{
    //    ModelDataContext DC = new ModelDataContext();

    public Window1()
    {
        InitializeComponent();

        WindowsListBox.ItemsSource = WindowViewStateManager.Instance.WindowViewStates;
    }

    private void SalesOrderButton_Click(object sender, RoutedEventArgs e)
    {
        SalesOrderWindow wnd = new SalesOrderWindow();
        var vm = (SalesOrderViewViewModel) wnd.view.DataContext;
        vm.SalesOrderList = new ObservableCollection<SalesOrderHeader>(
                                MyApp.DbContext.SalesOrderHeader
                                    .Include(so => so.Customer)
                                        .ThenInclude(c => c.CustomerAddress)
                                    .Include(so => so.SalesOrderDetail)
                                    .ThenInclude(detail => detail.Product));
        wnd.Show();
    }

    private void CustomerButton_Click(object sender, RoutedEventArgs e)
    {
        CustomerWindow wnd = new CustomerWindow();
        var vm = (CustomerWindowViewModel) wnd.DataContext;
        vm.CustomerList = new ObservableCollection<Customer>(
                            MyApp.DbContext.Customer
                                    //.Where(c => c.CustomerAddress.Count > 0)
                                    .Include(c => c.CustomerAddress)
                                    .ThenInclude(a => a.Address) );
                                    //.ToList() );
        wnd.Show();
    }

    private void ProductButton_Click(object sender, RoutedEventArgs e)
    {
        ProductWindow wnd = new ProductWindow();
        wnd.DataContext = new ObservableCollection<Product>(MyApp.DbContext.Product);
            //wnd.DataContext = DC.Products;
        wnd.Show();            
    }
}
}
