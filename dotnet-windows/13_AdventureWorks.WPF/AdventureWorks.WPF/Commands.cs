using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace AdventureWorks.WPF
{

public static class Commands
{
    public static readonly RoutedUICommand NewSalesOrder;
    public static readonly RoutedUICommand DeleteSalesOrder;
    public static readonly RoutedUICommand UpdateSalesOrder;

        public static readonly RoutedUICommand NewCustomer;
        public static readonly RoutedUICommand DeleteCustomer;
        public static readonly RoutedUICommand UpdateCustomer;

        public static readonly RoutedUICommand NewProduct;
        public static readonly RoutedUICommand DeleteProduct;
        public static readonly RoutedUICommand UpdateProduct;

        static Commands()
        {
            NewSalesOrder = new RoutedUICommand("Create a new Sales Order", "NewSalesOrder", typeof(Commands));
            DeleteSalesOrder = new RoutedUICommand("Delete current Sales Order", "DeleteSalesOrder", typeof(Commands));
            UpdateSalesOrder = new RoutedUICommand("Update current Sales Order", "UpdateSalesOrder", typeof(Commands));

            NewCustomer = new RoutedUICommand("Create a new customer", "NewCustomer", typeof(Commands));
            DeleteCustomer = new RoutedUICommand("Delete current customer", "DeleteCustomer", typeof(Commands));
            UpdateCustomer = new RoutedUICommand("Update current customer", "UpdateCustomer", typeof(Commands));

            NewProduct = new RoutedUICommand("Create a new product", "NewProduct", typeof(Commands));
            DeleteProduct = new RoutedUICommand("Delete current product", "DeleteProduct", typeof(Commands));
            UpdateProduct = new RoutedUICommand("Update current product", "UpdateProduct", typeof(Commands));
        }
    }
}
