﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AdventureWorks.WPF
{
    /// <summary>
    /// Interaction logic for ProductWindow.xaml
    /// </summary>
    public partial class ProductWindow : Window
    {
        ProductController controller = new ProductController();

        public ProductWindow()
        {
            InitializeComponent();
        }

        #region Command Sinks

        void NewProduct_CanExecute(
            object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        void NewProduct_Executed(
            object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("New Product");
        }

        void UpdateProduct_CanExecute(
            object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        void UpdateProduct_Executed(
            object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("Update Product");
        }

        void DeleteProduct_CanExecute(
            object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        void DeleteProduct_Executed(
            object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("Delete Product");
        }

        #endregion

    }
}
