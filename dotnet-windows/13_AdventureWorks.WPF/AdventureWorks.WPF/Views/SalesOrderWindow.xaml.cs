﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AdventureWorks.WPF
{
    /// <summary>
    /// Interaction logic for SalesOrderWindow.xaml
    /// </summary>
    public partial class SalesOrderWindow : Window
    {
        private SalesOrderController controller = new SalesOrderController();

        public SalesOrderWindow()
        {
            InitializeComponent();
        }

        #region Command Sinks

        void NewSalesOrder_CanExecute(
            object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        void NewSalesOrder_Executed(
            object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("New Sales Order");
        }

        void DeleteSalesOrder_CanExecute(
            object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.CanDeleteCurrent();
        }

        void DeleteSalesOrder_Executed(
            object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("Delete Sales Order");
        }

        void UpdateSalesOrder_CanExecute(
            object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = controller.CanUpdateCurrent();
        }

        void UpdateSalesOrder_Executed(
            object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("Update Sales Order");
        }
        
        #endregion // Command Sinks

    }
}
