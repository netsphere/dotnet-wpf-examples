using AdventureWorks.WPF.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace AdventureWorks.WPF
{

internal class CustomerWindowViewModel : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler PropertyChanged;

    // プロパティ値が変更されたことをリスナに通知する
    protected virtual void RaisePropertyChanged([CallerMemberName] string prop = "")
    {
        PropertyChanged ?.Invoke(this, new PropertyChangedEventArgs(prop));
    }

    private ObservableCollection<Customer> _customerList;
    public ObservableCollection<Customer> CustomerList {
        get => _customerList;
        set { _customerList = value; RaisePropertyChanged(); }
    }

    private Customer _selectedCustomer;
    public Customer SelectedCustomer {
        get => _selectedCustomer;
        set { _selectedCustomer = value; RaisePropertyChanged(); }
    }

    public CustomerWindowViewModel()
    {
    }

}


    /// <summary>
    /// Interaction logic for CustomerWindow.xaml
    /// </summary>
public partial class CustomerWindow : Window
{
    CustomerController controller = new CustomerController();

    public CustomerWindow()
    {
        InitializeComponent();

        CommandBindings.Add(new CommandBinding(Commands.NewCustomer,
                                               NewCustomer_Executed,
                                               NewCustomer_CanExecute));
        CommandBindings.Add(new CommandBinding(Commands.DeleteCustomer,
                                               DeleteCustomer_Executed,
                                               DeleteCustomer_CanExecute));
        CommandBindings.Add(new CommandBinding(Commands.UpdateCustomer,
                                               UpdateCustomer_Executed,
                                               UpdateCustomer_CanExecute));
    }

    /// ////////////////////////////////////////////////////////////////
    // Command Handlers

        void NewCustomer_CanExecute(
            object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        void NewCustomer_Executed(
            object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("New Customer");
        }

        void UpdateCustomer_CanExecute(
            object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        void UpdateCustomer_Executed(
            object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("Update Customer");
        }

        void DeleteCustomer_CanExecute(
            object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
        }

        void DeleteCustomer_Executed(
            object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("Delete Customer");
        }
    }
}
