using AdventureWorks.WPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AdventureWorks.WPF
{

public class ConcatNamesConverter : IMultiValueConverter
{
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                string lastName = (string)values[0];
                string firstName = (string)values[1];
                string title = (string)values[2];

                string displayValue = lastName.ToUpper();
                if (firstName != string.Empty)
                {
                    displayValue += ", ";
                    displayValue += firstName;
                }
                if (title != string.Empty)
                {
                    displayValue += " (";
                    displayValue += title;
                    displayValue += ")";
                }

                return displayValue;
            }
            catch (Exception)
            {

            }
            return string.Empty;
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            // Not implemented
            return null;
        }
}


internal class SalesOrderViewViewModel : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler PropertyChanged;

    // プロパティ値が変更されたことをリスナに通知する
    protected virtual void RaisePropertyChanged([CallerMemberName] string prop = "")
    {
        PropertyChanged ?.Invoke(this, new PropertyChangedEventArgs(prop));
    }

    private ObservableCollection<SalesOrderHeader> _salesOrderList;
    public ObservableCollection<SalesOrderHeader> SalesOrderList {
        get => _salesOrderList;
        set { _salesOrderList = value; RaisePropertyChanged(); }
    }

    private SalesOrderHeader _selectedSalesOrder;
    public SalesOrderHeader SelectedSalesOrder {
        get => _selectedSalesOrder;
        set { _selectedSalesOrder = value; RaisePropertyChanged(); }
    }
}


public partial class SalesOrderView
{
		public SalesOrderView()
		{
			this.InitializeComponent();
		}
	}
}
