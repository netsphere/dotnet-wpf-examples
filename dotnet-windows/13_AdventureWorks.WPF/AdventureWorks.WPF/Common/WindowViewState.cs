using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace AdventureWorks.WPF
{
    public class WindowViewStateInstance
    {
        public Guid Identifier { get; set; }
        public string Title { get; set; }
        public VisualBrush Icon { get; set; }
    }

    public class WindowViewStateManager
    {
        #region Instance

        static private WindowViewStateManager instance;
        static public WindowViewStateManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new WindowViewStateManager();

                return instance;
            }
        }

        #endregion

        ObservableCollection<WindowViewStateInstance> windowViewStates;
        public ObservableCollection<WindowViewStateInstance> WindowViewStates
        {
            get { return windowViewStates ?? (windowViewStates = new ObservableCollection<WindowViewStateInstance>()); }
        }

        public bool RemoveWindowViewState(Guid identifier)
        {
            var view = (from v in windowViewStates
                        where v.Identifier == identifier
                        select v).First();

            if (view != null)
            {
                windowViewStates.Remove(view);
                return true;
            }
            return false;
        }
    }


// 添付ビヘイビアは, static class にして, プロパティを
// `DependencyProperty.RegisterAttached()` で登録する。
public static class WindowViewState
{
    public static readonly DependencyProperty IdentifierProperty =
            DependencyProperty.RegisterAttached("Identifier", typeof(Guid),
                        typeof(WindowViewState),
                        new FrameworkPropertyMetadata(Guid.Empty));

    // GetXXX(), SetXXX() がリフレクションでプロパティを生やす
    public static Guid GetIdentifier(DependencyObject d)
    {
        return (Guid)d.GetValue(IdentifierProperty);
    }

    public static void SetIdentifier(DependencyObject d, Guid value)
    {
        d.SetValue(IdentifierProperty, value);
    }


    public static readonly DependencyProperty IsManagedProperty =
            DependencyProperty.RegisterAttached("IsManaged", typeof(bool), typeof(WindowViewState),
                new FrameworkPropertyMetadata((bool)false,
                    new PropertyChangedCallback(OnIsManagedChanged)));

    public static bool GetIsManaged(DependencyObject d)
    {
        return (bool)d.GetValue(IsManagedProperty);
    }

    public static void SetIsManaged(DependencyObject d, bool value)
    {
        d.SetValue(IsManagedProperty, value);
    }

        private static void OnIsManagedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Window target = (Window)d;
            if (target != null)
            {
                target.Loaded += new RoutedEventHandler(target_Loaded);
                target.Closed += new EventHandler(target_Closed);
            }
        }

        static void target_Closed(object sender, EventArgs e)
        {
            Window target = (Window)sender;
            if (target != null)
            {
                WindowViewStateManager.Instance.RemoveWindowViewState((Guid)target.GetValue(WindowViewState.IdentifierProperty));
            }
        }

        static void target_Loaded(object sender, RoutedEventArgs e)
        {
            Window target = (Window)sender;
            if (target != null)
            {
                WindowViewStateInstance instance = new WindowViewStateInstance();
                instance.Title = target.Title;
                instance.Icon = new VisualBrush(target);
                instance.Identifier = Guid.NewGuid();

                target.SetValue(WindowViewState.IdentifierProperty, instance.Identifier);


                WindowViewStateManager.Instance.WindowViewStates.Add(instance);
            }
        }

}

}
