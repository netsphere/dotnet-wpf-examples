﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace weak_listener
{

[Serializable]
public class DocumentModel : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler? PropertyChanged;

    private string? _filePath;
    // The full path of the file selected.
    public string? FilePath {
        get { return _filePath; }
        set {
            _filePath = value;
            if (_filePath != null)
                _text = File.ReadAllText(_filePath); // 裏から入れる.
            else
                _text = "";
            _isDirty = false;

            RaisePropertyChanged();  // イベント1回
        }
    }

    private string _text;
    public string Text {
        get { return _text; }
        set {
            if (_text != value) {
                _text = value; IsDirty = true;
                RaisePropertyChanged();
            }
        }
    }

    private bool _isDirty;
    public bool IsDirty {
        get { return _isDirty; }
        protected set {
            if (value != _isDirty) {
                _isDirty = value;
                RaisePropertyChanged();
            }
        }
    }

    // プロパティ値が変更されたことをリスナに通知する
    protected virtual void RaisePropertyChanged([CallerMemberName] string prop = "")
    {
        PropertyChanged ?.Invoke(this, new PropertyChangedEventArgs(prop));
    }
}

}
