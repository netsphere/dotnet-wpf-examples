﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.IO;
using System.Windows.Controls;


namespace weak_listener
{

internal class MainWindowViewModel : INotifyPropertyChanged, IWeakEventListener
{
    public event PropertyChangedEventHandler? PropertyChanged;

    private void updateTitle()
    {
        var doc = ((MyApp) Application.Current).DocumentModel;
        var fileName = string.IsNullOrEmpty(doc.FilePath) ? "無題" :
                                        Path.GetFileName(doc.FilePath);
        this.Title = (doc.IsDirty ? "*" : "") + fileName + " - HogeHoge App";
    }

    public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
    {
        var args = (PropertyChangedEventArgs) e;
        var doc = ((MyApp) Application.Current).DocumentModel;

        switch (args.PropertyName) {
        case "FilePath":
            updateTitle(); 
            this.Text = doc.Text;
            break;

        case nameof(Text):
            this.Text = doc.Text; break;

        case "IsDirty":
            updateTitle(); break;
        }

        return true;
    }

    private string _title;
    public string Title { 
        get { return _title; }
        protected set { _title = value; RaisePropertyChanged(); }
    }

    // 見えている領域のみ.
    private string _text;
    public string Text { 
        get { return _text; }
        set { _text = value; RaisePropertyChanged();}
    }


    public MainWindowViewModel() 
    { 
        var doc = ((MyApp) Application.Current).DocumentModel;

        updateTitle();
        this.Text = doc.Text;
    }

    // プロパティ値が変更されたことをリスナに通知する
    protected virtual void RaisePropertyChanged([CallerMemberName] string prop = "")
    {
        PropertyChanged ?.Invoke(this, new PropertyChangedEventArgs(prop));
    }
}


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
    }

    private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
    {
        var doc = ((MyApp) Application.Current).DocumentModel;
        doc.Text = textBox.Text;
    }
}
}