using Microsoft.Win32;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Windows;
using System.Windows.Input;

namespace weak_listener
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
public partial class MyApp : Application
{
    public DocumentModel DocumentModel;

    public MyApp()
    {
        DocumentModel = new DocumentModel();
    }


    private void Application_Startup(object sender, StartupEventArgs e)
    {
        OnWindowNewWindow(this, null);
    }


    /// /////////////////////////////////////////////////////////////////////
    // Command Handlers

    private void OnFileOpen(object sender, ExecutedRoutedEventArgs e)
    {
        var dlg = new OpenFileDialog();
        dlg.Filter = "テキストファイル (*.txt;*.html)|*.txt;*.html|すべてのファイル (*.*)|*.*";
        if (dlg.ShowDialog() == true ) {
            DocumentModel.FilePath = dlg.FileName;
        }
    }

    private void OnWindowNewWindow(object sender, ExecutedRoutedEventArgs e)
    {
        var wnd = new MainWindow();

        var vm = (MainWindowViewModel) wnd.DataContext;
        PropertyChangedEventManager.AddListener(DocumentModel, vm, "");  // ☆Point!

        wnd.CommandBindings.Add(
                new CommandBinding(ApplicationCommands.Open, OnFileOpen));
        wnd.CommandBindings.Add(
                new CommandBinding(MyCommands.NewWindow, OnWindowNewWindow));

        wnd.Show();
    }

}


static class MyCommands
{
    public static readonly ICommand NewWindow =
        new RoutedUICommand("New Window", nameof(NewWindow), typeof(MyCommands));
}

}
