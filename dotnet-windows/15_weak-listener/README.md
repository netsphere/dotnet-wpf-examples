
# Weak Listener

WPF では, ウィンドウなどの view と view model は, 密結合になっている。一つの view に対して一つの view model が生成される. 寿命も同じ.

ファイルそのほかにデータを保存するアプリケィションは、MVC モデルでつくる。Document (Model) の変更を view model で察知し, view に反映する、という流れになる。ここで, document よりも view model のほうが寿命が短い点に注意が必要。
 
`INotifyPropertyChanged` インタフェイスの `PropertyChanged` イベントは, ターゲット (イベントハンドラ側) を強参照で保持するため、document - view model を単に `INotifyPropertyChanged` で繋ぐと, view を閉じたときでも view model がメモリ解放されない。

簡単にいくには、間に `PropertyChangedEventManager` を挟むだけでいい。非常に簡単!



## Mediator デザインパタン

`System.ComponentModel.PropertyChangedEventManager` クラスは .NET Windows Desktop アセンブリに含まれる。コンソールアプリでは使えない。基底クラスの `WeakEventManager` も同じ。

公式ドキュメントが相変わらずよく分からないが, `WeakEventManager` クラスは Mediator デザインパタンになっている。




参考:
 - <a href="http://msyi303.blog130.fc2.com/blog-entry-60.html">Scribbled Records - WPF の弱いイベントパターン (Weak Event Patterns)</a>
 - <a href="http://pro.art55.jp/?eid=1107021">【WPF】変更通知をIWeakEventListenerでリッスン！ | 創造的プログラミングと粘土細工</a>
 - <a href="https://www.abhisheksur.com/2012/02/optimizing-inpc-objects-against-memory.html">DOT NET TRICKS: Optimizing INPC Objects against memory leaks using WeakEvents</a>






## .NET Framework 4.5 以降

`WeakEventManager<TEventSource,TEventArgs>` class が導入された。

<a href="https://www.codeproject.com/Articles/738109/The-NET-Weak-Event-Pattern-in-Csharp">The .NET Weak Event Pattern in C#</a>

