
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using SimpleEditor.Extentions;
using SimpleEditor.Models;


namespace SimpleEditor.Controls
{
    /// <summary>
    /// Control to draw Scene object
    /// </summary>
// 独自コントロールは, System.Windows.Controls.Control クラスから派生させる
public class DrawControl : Control
{
    // Dependency properties //////////////////////////////////////////////

        /// <summary>
        /// Dependency property. Scene for drawing control
        /// </summary>
    public static readonly DependencyProperty SceneProperty =
            DependencyProperty.Register("Scene", typeof (Scene), typeof (DrawControl),
                new PropertyMetadata(new PropertyChangedCallback(SceneChanged)));


        /// Dependency property. Actual size of drawing
        /// </summary>
    public static readonly DependencyProperty ActualSizeProperty =
            DependencyProperty.Register("ActualSize", typeof(System.Drawing.Size),
                                            typeof (DrawControl));


        /// Scene for drawing control
    // 依存プロパティのためのヘルパプロパティ. binding に必要.
    public Scene Scene {
        get {
            return (Scene) GetValue(SceneProperty);
        }
        set {
                SetValue(SceneProperty, value);
        }
    }

        /// <summary>
        /// Actual size of drawing
        /// </summary>
    public System.Drawing.Size ActualSize {
        get {
            return (System.Drawing.Size) GetValue(ActualSizeProperty);
        }
        set {
            SetValue(ActualSizeProperty, value);
        }
    }


    // 依存プロパティが変更された. XAML で bind した場合は load 時に呼び出される
    static void SceneChanged(DependencyObject sender,
                             DependencyPropertyChangedEventArgs e)
    {
            if (e.NewValue!=null)
                ((Scene)e.NewValue).SceneChanged += ((DrawControl)sender).InvalidateScene;
            ((DrawControl)sender).InvalidateVisual();
        }

        /// <summary>
        /// Invalidate scene
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
    void InvalidateScene(object sender, EventArgs e)
    {
            this.InvalidateVisual();
        }


    // Protected methods ////////////////////////////////////////////////////

    // @override FrameworkElement
    protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
    {
        base.OnPropertyChanged(e);
        if ( e.Property == ActualHeightProperty || e.Property == ActualWidthProperty)
            ActualSize = new System.Drawing.Size((int) ActualWidth, (int) ActualHeight);
    }


        /// Draw scene bitmap in context
        /// </summary>
        /// <param name="drawingContext">Drawing context</param>
    // @override UIElement
    // 自前で描画するときは, このメソッドを override すればよい.
    protected override void OnRender(DrawingContext drawingContext)
    {
        base.OnRender(drawingContext);
        drawingContext.DrawRectangle(new SolidColorBrush(Colors.White), null, new Rect(0, 0, this.ActualWidth, this.ActualHeight));
        if (Scene == null)
            return;

        using (var resultBitmap = Scene.DrawToBitmap((int) ActualWidth, (int) ActualHeight))
        {
            drawingContext.DrawImage(resultBitmap.ToWpfBitmap(),new Rect(0,0,ActualWidth,ActualHeight));
        }
    }

    }
}
