
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.Serialization; // アセンブリも追加する
using Brush = System.Drawing.Brush;
using Color = System.Drawing.Color;
using Pen = System.Drawing.Pen;
using PixelFormat = System.Drawing.Imaging.PixelFormat;
using Point = System.Drawing.Point;


namespace SimpleEditor.Models
{

    /// Scene manage layer logic.
    /// Inherited from IDisposable, because has Pen and Brush (GDI unmanaged objects) and layers inside has unmanaged resources
// レイヤを1つ以上持つ。
public class Scene : IDisposable
{
    List<Point> _points;


        /// Gets layer list
        /// </summary>
    // これがモデル本体. 後ろが手前
    public List<Layer> Layers { get; private set; }

        /// Returns true if scene has no layers
        /// </summary>
    public bool HasNoLayers {
        get { return Layers.Count == 0; }
    }


        /// Fires if something changed in scene
        /// </summary>
    public event EventHandler SceneChanged;

        /// <summary>
        /// Fires when order of layers is changed (moving layer up\down, deleting)
        /// </summary>
    public event EventHandler LayersOrderChanged;


    // Constructor
    public Scene()
    {
        Layers = new List<Layer>();

        // TODO: view model に移動する
        _points = new List<Point>();
    }


        /// Save current scene to file
        /// </summary>
        /// <param name="filename">Filename</param>
    public void Save(string filename)
    {
        var lst = Layers.Select(layer => new LayerBundle() {
                                    Bitmap = layer.Bitmap,
                                    Position = layer.CanvasRect.Location,
                                    Size = layer.CanvasRect.Size,
                                    IsVisible = layer.IsVisible
                  }).ToList();
        var dcs = new DataContractSerializer(typeof(List<LayerBundle>));
        dcs.WriteObject(File.Create(filename), lst);
    }


        /// Export scene to PNG
        /// </summary>
        /// <param name="filename">Filename</param>
        /// <param name="size">Canvas size</param>
    public void Export(string filename, Size size)
    {
        var bmp = DrawToBitmap(size.Width, size.Height);
        bmp.Save(filename,ImageFormat.Png);
    }

    // Load scene from a file, 新しいインスタンスを生成.
        /// </summary>
        /// <param name="filename">Filename</param>
        /// <returns>Loaded scene</returns>
    public static Scene Load(string filename)
    {
        var dcs = new DataContractSerializer(typeof(List<LayerBundle>));
        var layerBundles = (List<LayerBundle>)dcs.ReadObject(File.OpenRead(filename));
        var scene = new Scene();
        scene.LayersFromBundle(layerBundles);

        return scene;
    }


        /// Draw scene to bitmap
        /// </summary>
        /// <param name="sceneWidth">Width of bitmap</param>
        /// <param name="sceneHeight">Height of bitmap</param>
        /// <returns>Bitmap with drawed scene</returns>
    public Bitmap DrawToBitmap(int sceneWidth, int sceneHeight)
    {
        var bitmap = new Bitmap(sceneWidth, sceneHeight, PixelFormat.Format32bppArgb);
        using (var gr = Graphics.FromImage(bitmap)) {
            gr.Clear(Color.White);
            foreach (var layer in Layers) { // 後ろが手前
                if (layer.IsVisible) {
                    gr.DrawImage(layer.Bitmap, layer.CanvasRect.Location);
                    if (layer.BufferBitmap != null)
                        gr.DrawImage(layer.BufferBitmap, layer.CanvasRect.Location);
                }
            }
        }

        return bitmap;
    }


    /// Adding new layer to scene
    public void InsertNewLayer(int pos)
    {
        var layer = new Layer();
        layer.LayerChanged += layer_LayerChanged;
        Layers.Insert(pos, layer);
        Invalidate();
        InvalidateLayersOrder();
    }


    /// Remove selected layer from scene
    public void RemoveLayerAt(int pos)
    {
        if (HasNoLayers)
            return;

        Layers.RemoveAt(pos);

        Invalidate();
        InvalidateLayersOrder();
    }


    /// Move selected layer up in layer list (move up in drawing order, move down in canvas )
    public void MoveLayer(int oldIndex, int newIndex)
    {
        if (HasNoLayers)
            return;

        var layer = Layers[oldIndex];
        Layers.RemoveAt(oldIndex);
        Layers.Insert(newIndex, layer);

        Invalidate();
        InvalidateLayersOrder();
    }


    // ● TODO: 下 3メソッド, `Scene` クラスに必要?

        /// Start user interaction logic, like MouseDown on control
        /// If not pan mode draw point
        /// </summary>
        /// <param name="p">Start point</param>
    public void PressDown(int layer_idx, Brush brush, float thickness,
                          System.Drawing.Point pt)
    {
        var layer = Layers[layer_idx];

        layer.CheckPostionAndSize(pt,  thickness);
        //var normalized = p.Normalize(Layers[layer_idx].Position);
        layer.DrawPoint(brush, thickness, pt);
        _points = new List<Point>() { pt };
    }

        /// Draw line or move layer (depends on pan mode)
        /// </summary>
        /// <param name="p">Point to move</param>
    public void MouseMove(int layer_idx, Pen pen, Point pt)
    {
        var layer = Layers[layer_idx];

        layer.CheckPostionAndSize(pt, pen.Width);
        _points.Add(pt);
        // TODO: ここ、毎回全部描き直していて, 効率悪い。
        layer.DrawLines(pen, _points.ToArray()); 
    }

        /// Ends user intercation logic, like mouseup
        /// </summary>
        /// <param name="p">Last point</param>
    public void PressUp(int layer_idx, Point pt)
    {
        Layers[layer_idx].Apply();
        _points.Clear();
    }

    // ● TODO: ここまで


        /// Dispose scene
        /// </summary>
    public void Dispose()
    {
        //_pen.Dispose();
        //_brush.Dispose();
        foreach (var layer in Layers) {
            layer.Dispose();
        }
    }


        /// Add layers from bundles
        /// </summary>
        /// <param name="layerBundles">Collection of layer bundles</param>
    void LayersFromBundle(IEnumerable<LayerBundle> layerBundles)
    {
        foreach (var layerBundle in layerBundles) {
                var layer = new Layer(layerBundle);
                layer.LayerChanged += layer_LayerChanged;
                Layers.Add(layer);
        }
    }


    void layer_LayerChanged(object sender, System.EventArgs e)
    {
            Invalidate();
        }

        private void Invalidate()
        {
            if (SceneChanged != null)
                SceneChanged(this, new EventArgs());
        }

    void InvalidateLayersOrder()
    {
            if (LayersOrderChanged!=null)
                LayersOrderChanged(this,new EventArgs());
    }


}
}
