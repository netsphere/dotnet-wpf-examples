
using SimpleEditor.Extentions;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;


namespace SimpleEditor.Models
{
    /// <summary>
    /// Layer class. Contains drawing logic
    /// </summary>
public class Layer : IDisposable
{

        /// Gets or sets visibility of the layer
        /// </summary>
    bool _isVisible = true;
    public bool IsVisible {
        get { return _isVisible; }
        set {
            _isVisible = value;
            Invalidate();
        }
    }

    // Gets or sets layer position
    // レイヤーの領域。選択したレイヤーだけ動かせるようにするため、レイヤーごとに持つ
    System.Drawing.Rectangle _canvas_rect;
    public Rectangle CanvasRect {
        get { return _canvas_rect; }
        set {
            _canvas_rect = value;
            Invalidate();
        }
    }


        /// Gets bitmap with already drawed figures, 
        /// does not contain now drawing figure
        /// </summary>
    System.Drawing.Bitmap _bitmap;
    public Bitmap Bitmap {
        get { return _bitmap; }
        private set {
            if (_bitmap!=null)
                _bitmap.Dispose();
            _bitmap = value;
        }
    }


        /// Gets bitmap of now drawing figure
        /// </summary>
    Bitmap _bufferBitmap;
    public Bitmap BufferBitmap {
        get { return _bufferBitmap; }
        private set {
            if (_bufferBitmap != null)
                _bufferBitmap.Dispose();
            _bufferBitmap = value;
        }
    }


        /// <summary>
        /// Fires when something changed in layer 
        /// </summary>
    public event EventHandler LayerChanged;


        /// Creates layer with 1 on 1 canvas
        /// </summary>
    public Layer()
    {
        //Size = new Size(600, 600); // DEBUG
        CanvasRect = new Rectangle(0, 0, 1, 1);
        Bitmap = new Bitmap(CanvasRect.Width, CanvasRect.Height,
                            PixelFormat.Format32bppArgb);            
    }

        /// Creates layer from layer bundle
        /// </summary>
        /// <param name="layerBundle">layer bundle</param>
    public Layer(LayerBundle layerBundle)
    {
        if (layerBundle == null)
            throw new ArgumentNullException(nameof(layerBundle));

        Bitmap = layerBundle.Bitmap;
        CanvasRect = new Rectangle(layerBundle.Position, layerBundle.Size);
        IsVisible = layerBundle.IsVisible;
    }


#region public methods

        /// Checks that drawed point or line in layer bounds,
        /// if not increase bounds to contains drawing
        /// </summary>
        /// <param name="points">Points of drawing</param>
    public void CheckPostionAndSize(Point point, float thickness)
    {
        bool needToChangeSize = false;
        float halfThickness = thickness / 2;
        int minX = (int) (point.X - halfThickness);
        int minY = (int) (point.Y - halfThickness);
        int maxX = (int) (point.X + halfThickness);
        int maxY = (int) (point.Y + halfThickness);

        Point newPosition = new Point(CanvasRect.X, CanvasRect.Y);
        Point newRB       = new Point(CanvasRect.Right, CanvasRect.Bottom);

        if ( minX < CanvasRect.X) {
            newPosition.X = minX;  needToChangeSize = true;
        }
        if (minY < CanvasRect.Y) {
            newPosition.Y = minY;  needToChangeSize = true;
        }
        if (maxX > CanvasRect.X + CanvasRect.Width) {
            newRB.X = maxX ;       needToChangeSize = true;
        }
        if (maxY > CanvasRect.Y + CanvasRect.Height) {
            newRB.Y = maxY;        needToChangeSize = true;
        }

        if (needToChangeSize) {
            ChangeSizeAndPosition(newPosition,
                new Size(newRB.X - newPosition.X, newRB.Y - newPosition.Y));
        }
    }


    // Changes poisition and size of layer
    // 領域外を描画しようとすると, 領域を広げる。その後 pan すると、その部分も表示できる
        /// </summary>
        /// <param name="newPosition">New position</param>
        /// <param name="newSize">New size</param>
    void ChangeSizeAndPosition(Point newPosition, Size newSize)
    {
        var newBitmap = new Bitmap(newSize.Width, newSize.Height,
                                   PixelFormat.Format32bppArgb);

        using (var gr = Graphics.FromImage(newBitmap)) {
            var dx = CanvasRect.X - newPosition.X;
            var dy = CanvasRect.Y - newPosition.Y;
            gr.DrawImage(Bitmap, dx, dy);
        }

        Bitmap = newBitmap;
        CanvasRect = new Rectangle(newPosition, newSize);

        Invalidate();
    }


        /// Draw lines by points
        /// </summary>
        /// <param name="pen">Pen to draw</param>
        /// <param name="points">Lines points</param>
    public void DrawLines(Pen pen, Point[] points)
    {
        if (pen == null)
            throw new ArgumentNullException(nameof(pen));

        BufferBitmap = new Bitmap(Bitmap.Width, Bitmap.Height,
                                  PixelFormat.Format32bppArgb);

        // TODO: ここ, 毎回全部描き直していて、効率悪い
        using (var gr = Graphics.FromImage(BufferBitmap)) {
            gr.SmoothingMode = SmoothingMode.HighQuality;
            for (int i = 1; i < points.Length; i++) {
                gr.DrawLine(pen, points[i - 1].Normalize(CanvasRect.Location),
                                 points[i].Normalize(CanvasRect.Location) );
            }
        }

        Invalidate();
    }


        /// Draw point 
        /// </summary>
        /// <param name="brush">Brush to draw</param>
        /// <param name="thickness">Diameter of point</param>
        /// <param name="p">Point</param>
    public void DrawPoint(Brush brush, float thickness, Point p)
    {
        if (brush == null)
            throw new ArgumentNullException(nameof(brush));

        BufferBitmap = new Bitmap(Bitmap.Width,Bitmap.Height,PixelFormat.Format32bppArgb);

        using (var gr = Graphics.FromImage(BufferBitmap)) {
            gr.SmoothingMode = SmoothingMode.HighQuality;
            Point LT = new Point((int) (p.X - thickness / 2), (int) (p.Y - thickness / 2))
                                .Normalize(CanvasRect.Location);
            gr.FillEllipse(brush, LT.X, LT.Y, thickness, thickness);
        }

        Invalidate();
    }

        /// <summary>
        /// Applies now drawing figure to Bitmap
        /// </summary>
    public void Apply()
    {
        using (var gr = Graphics.FromImage(Bitmap)) {
            gr.DrawImage(BufferBitmap,0,0);
        }
        BufferBitmap = null;
        Invalidate();
    }

        /// <summary>
        /// Offset layer
        /// </summary>
        /// <param name="dx">X offset (adds to x)</param>
        /// <param name="dy">Y offset (adds to y)</param>
    public void Offset(int dx, int dy)
    {
            _canvas_rect.X += dx;
            _canvas_rect.Y += dy;
            Invalidate();
    }

        /// Invalidating layer
        /// </summary>
    void Invalidate()
    {
        if (LayerChanged!=null)
            LayerChanged(this,new EventArgs());
    }


        /// Disposing layer ( dispose bitmaps )
        /// </summary>
    public void Dispose()
    {
            Bitmap = null;
            BufferBitmap = null;
    }

#endregion
}
}
