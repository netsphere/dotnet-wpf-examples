
using Microsoft.Win32;
using SimpleEditor.Models;
using SimpleEditor.Views;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;


namespace SimpleEditor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
public partial class MyApp : Application
{
    const string FileFilter = "Simple Editor files|*.se";

    const string LoadErrorMessage = "Error occurred on opening:\r\n{0}";
    const string SaveErrorMessage = "Error occurred on saving:\r\n{0}";

    // 単一ドキュメント
    Scene _document;
    public Scene Document { get { return _document; } }

    public static void errorMessage(string message, Exception e)
    {
        MessageBox.Show(string.Format(message, e.Message),
                        "error", MessageBoxButton.OK, MessageBoxImage.Error);
    }


    /// ///////////////////////////////////////////////////////////////////
    // Event Handlers

    void Application_Startup(object sender, StartupEventArgs e)
    {
        var wnd = new Views.MainView();

        List<CommandBinding> bindings = new List<CommandBinding>{
            new CommandBinding(ApplicationCommands.New, OnFileNew),
            new CommandBinding(ApplicationCommands.Open, OnFileOpen),
            new CommandBinding(ApplicationCommands.Save, OnFileSave),
            new CommandBinding(MyCommands.ExportCommand, wnd.OnFileExport)
        };
        foreach (var b in bindings)
            wnd.CommandBindings.Add(b);

        // ドキュメントをつくる
        OnFileNew(null, null);

        wnd.Show();
    }


    /// ///////////////////////////////////////////////////////////////////
    // Command handlers

    void OnFileNew(object sender, ExecutedRoutedEventArgs e)
    {
        // TODO: dirty か確認
        _document = new Scene();
        _document.InsertNewLayer(0);

        MainView wnd = (MainView) MainWindow;
        ((MainViewModel) wnd.DataContext).LoadScene(_document);
    }

    void OnFileOpen(object sender, ExecutedRoutedEventArgs e)
    {
        var ofd = new OpenFileDialog { Filter = FileFilter };
        if (ofd.ShowDialog() != true)
            return;

        try {
            _document = Scene.Load(ofd.FileName);
            MainView wnd = (MainView) MainWindow;
            ((MainViewModel) wnd.DataContext).LoadScene(_document);
        }
        catch (Exception ex) {
            errorMessage(LoadErrorMessage, ex);
        }
    }

    void OnFileSave(object sender, ExecutedRoutedEventArgs e)
    {
        var sfd = new SaveFileDialog { Filter = FileFilter, AddExtension = true };
        if (sfd.ShowDialog() != true)
            return;

        try {
            _document.Save(sfd.FileName); // TODO: dirty をクリア
        }
        catch (Exception ex) {
            errorMessage(SaveErrorMessage, ex);
        }
    }

}


static class MyCommands
{
    public static readonly ICommand ExportCommand =
            new RoutedUICommand("PNGファイルで保存", nameof(ExportCommand),
                                typeof(MyCommands));

    // Command to add new layer to scene
    public static readonly ICommand AddLayerCommand =
            new RoutedUICommand("レイヤを追加", nameof(AddLayerCommand),
                                typeof(MyCommands));

    public static readonly ICommand RemoveLayerCommand =
            new RoutedUICommand("レイヤを削除", nameof(RemoveLayerCommand),
                                typeof(MyCommands));

    public static readonly ICommand UpLayerCommand =
            new RoutedUICommand("レイヤを手前に移動", nameof(UpLayerCommand),
                                typeof(MyCommands));

    public static readonly ICommand DownLayerCommand =
            new RoutedUICommand("レイヤを奥に移動", nameof(DownLayerCommand),
                                typeof(MyCommands));
}

}
