using System;
using System.Diagnostics;
using System.Windows.Input;

namespace MVVM
{
// T コマンドパラメータの型
public class RelayCommand<T> : ICommand
{
    // Fields ///////////////////////////////////////////

    // Action<T> T型の引数を一つ取り, 戻り値は返さない
    readonly Action<T> _execute;

    // T型の引数を一つ取り, bool を返す. `Func<T, bool>` と同じ.
    readonly Predicate<T> _canExecute;


    // Constructors /////////////////////////////////////

    public RelayCommand(Action<T> execute)
    {
        if (execute is null)
            throw new ArgumentNullException(nameof(execute));

        _execute = execute;
        _canExecute = (o) => true ;
    }

    // @param canExecute  陽に指定する場合, null は不可.
    public RelayCommand(Action<T> execute, Predicate<T> canExecute)
    {
        if (execute is null)
            throw new ArgumentNullException(nameof(execute));
        if (canExecute is null)
            throw new ArgumentNullException(nameof(canExecute));

        _execute = execute;
        _canExecute = canExecute;
    }


    public void Execute(T parameter)
    {
        _execute(parameter);
    }

    [DebuggerStepThrough]
    public bool CanExecute(T parameter)
    {
        return _canExecute(parameter);
    }

    // コマンドが実行可能か、の状態変更で発火
    public event EventHandler CanExecuteChanged {
        add { CommandManager.RequerySuggested += value; }
        remove { CommandManager.RequerySuggested -= value; }
    }


    // 覆い隠す
    bool ICommand.CanExecute(object parameter)
    {
        // Special case a null value for a value type argument type.
        // This ensures that no exceptions are thrown during initialization.
        if (parameter is null && default(T) != null)
            return false;

        return CanExecute((T) parameter);
    }

    void ICommand.Execute(object parameter)
    {
        Execute((T) parameter);
    }
} // class RelayCommand<T>

}
