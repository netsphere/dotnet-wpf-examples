
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;


namespace SimpleEditor.Views
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
public partial class MainView : Window
{
    public MainView()
    {
        InitializeComponent();

        var x = (CollectionViewSource) FindResource("layersVS1");
        x.SortDescriptions.Clear();
        x.SortDescriptions.Add(
                new SortDescription("Index", ListSortDirection.Descending));

        var scene_vm = ((MainViewModel) DataContext).SceneViewModel;
        List<CommandBinding> bindings = new List<CommandBinding> {
            new CommandBinding(MyCommands.AddLayerCommand, scene_vm.OnAddLayer),
            new CommandBinding(MyCommands.RemoveLayerCommand,
                                            scene_vm.OnRemoveLayer,
                                            scene_vm.CanRemoveLayer),
            new CommandBinding(MyCommands.UpLayerCommand,
                                            scene_vm.OnUpLayer,
                                            scene_vm.CanMoveSelectedLayerUp),
            new CommandBinding(MyCommands.DownLayerCommand,
                                            scene_vm.OnDownLayer,
                                            scene_vm.CanMoveSelectedLayerDown) };
        foreach (var b in bindings)
            CommandBindings.Add(b);
    }


    /// ////////////////////////////////////////////////////////////////////
    // Event Handlers

    void Window_Closing(object sender, CancelEventArgs e)
    {
        // TODO: dirty チェック
    }

    /// ////////////////////////////////////////////////////////////////////
    // Command Handlers

    const string ExportFilter = "PNG files|*.png";
    const string ExportErrorMessage = "Error occurred on saving to PNG:\r\n{0}";

    public void OnFileExport(object sender, ExecutedRoutedEventArgs e)
    {
        var sfd = new SaveFileDialog { Filter = ExportFilter, AddExtension = true };
        if (sfd.ShowDialog() != true)
            return;

        try {
            ((MyApp) Application.Current).Document.Export(
                                sfd.FileName, drawControl.ActualSize);
        }
        catch (Exception ex) { 
            MyApp.errorMessage(ExportErrorMessage, ex);
        }
    }

} // class MainView

}
