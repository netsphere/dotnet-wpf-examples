
using System.Windows;
using SimpleEditor.Models;


namespace SimpleEditor.Views
{
    /// <summary>
    /// Main view logic class
    /// </summary>
public class MainViewModel: MVVM.BindableBase
{
    public SceneViewModel SceneViewModel { get; private set; }

    public System.Drawing.Size VisibleSize { get; set; }

    // 元の方法では, {Binding ...} で XAML MenuItem.Command と紐付け。
    // この方法だと, 任意のクラスに routing できない。
    //public ICommand NewCommand { get; private set; }
        //public ICommand SaveCommand { get; private set; }
        //public ICommand LoadCommand { get; private set; }
        //public ICommand ExportCommand { get; private set; }


    // コンストラクタ
    public MainViewModel()
    {
        //var scene = new Scene();
        //scene.AddNewLayer();

        SceneViewModel = new SceneViewModel();
    }

    public void LoadScene(Scene scene)
    {
        SceneViewModel.Scene = scene;
    }

}
}
