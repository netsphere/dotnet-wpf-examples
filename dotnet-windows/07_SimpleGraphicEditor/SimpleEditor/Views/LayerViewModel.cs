using System;
using System.Windows.Media.Imaging;
//using MVVM;
using SimpleEditor.Extentions;
using SimpleEditor.Models;


namespace SimpleEditor.Views
{
    /// <summary>
    /// View model for layer
    /// </summary>
public class LayerViewModel : MVVM.BindableBase
{
    readonly Layer _layer;


        /// Gets image from layer
        /// </summary>
    public BitmapSource Image {
        get {
            return _layer.Bitmap.ToWpfBitmap();
        }
    }

    public int Index { get; set; }

        /// <summary>
        /// Gets or sets layer name
        /// </summary>
    string _name;
    public string Name {
        get { return _name; }
        set { SetPropertyAndRaise(ref _name, value); }
    }

        /// <summary>
        /// Gets or sets visibility of the layer
        /// </summary>
    public bool IsVisible {
        get { return _layer.IsVisible; }
        set { _layer.IsVisible = value; }
    }


    public LayerViewModel(Layer layer)
    {
        if (layer == null)
            throw new ArgumentNullException("layer");
        _layer = layer;
        _layer.LayerChanged += _layer_LayerChanged;
    }

        private void _layer_LayerChanged(object sender, EventArgs e)
        {
            RaisePropertyChanged("Image");
        }
    }
}
