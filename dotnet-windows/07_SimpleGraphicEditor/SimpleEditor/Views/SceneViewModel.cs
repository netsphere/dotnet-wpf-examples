
using System;
using System.Collections.ObjectModel;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Windows.Input;
using SimpleEditor.Models;
using SimpleEditor.Controls;
using SimpleEditor.Extentions;


namespace SimpleEditor.Views
{
    /// <summary>
    /// View model for scene class
    /// </summary>
public class SceneViewModel : MVVM.BindableBase
{
    // モデル
    Scene _scene;

    // color, thickness から作る
    System.Drawing.Pen _pen;
    System.Drawing.Brush _brush;

        /// Gets layers viewmodel observable collection
        /// </summary>
    public ObservableCollection<LayerViewModel> Layers { get; private set; }

            /// Gets or sets current selected layer index,
        /// if scene has layers, selected index should be positive,
        /// if has no layers SelectedLayerIndex=-1
    // ListView の表示順を変更しても, SelectedIndex は物理的な表示順になってしまう。
    //  -> こちらではなく, SelectedItem を bind すること
    int _selectedLayerIndex;
    public int SelectedLayerIndex {
        get { return _selectedLayerIndex; }
        set {
            if (value >= 0 && value < Layers.Count) {
                this.SelectedLayerViewModel = Layers[value]; // ここで発報
            }
/*
            if ( this.Layers.Count == 0 ) {
                //_selectedLayerIndex = -1;
                return;
            }

            if (value < 0)
                    throw new ArgumentOutOfRangeException("SelectedLayerIndex must be greater then 0");
            if (value >= this.Layers.Count)
                    throw new ArgumentOutOfRangeException("SelectedLayerIndex must be < layers array count");
*/
            SetPropertyAndRaise(ref _selectedLayerIndex, value);
        }
    }


        /// Gets selected layer view model
    // こちらを SelectedItem に bind する
    public LayerViewModel SelectedLayerViewModel {
        get {
            return SelectedLayerIndex >= 0 && SelectedLayerIndex < Layers.Count ?
                            Layers[SelectedLayerIndex] : null;
        }
        set {
            for (int i = 0; i < Layers.Count; i++) {
                if (Layers[i] == value) {
                    _selectedLayerIndex = i; // loop しないように
                    RaisePropertyChanged(nameof(SelectedLayerIndex));
                    RaisePropertyChanged(nameof(SelectedLayerViewModel));
                    return;
                }
            }
            // _selectedLayerIndex = -1;
        }
    }


    // Gets or sets color for scene brush
    System.Drawing.Color _color;
    public System.Drawing.Color Color {
        get { return _color; }
        set {
            if (SetPropertyAndRaise(ref _color, value)) { 
                InvalidateBrushAndPen();
                RaisePropertyChanged(nameof(ColorString));
            }
        }
    }

    // Gets color in string format
    System.Windows.Media.Brush _bandColor;
    public System.Windows.Media.Brush ColorString {
        get {
            //var cc = new System.Drawing.ColorConverter();
            return _bandColor; // cc.ConvertToString(Color);
        }
    }


    // Gets or sets pen thickness (width)
    float _thickness;
    public float Thickness {
        get { return _thickness; }
        set {
            if (SetPropertyAndRaise(ref _thickness, value))
                InvalidateBrushAndPen();
        }
    }

        /// <summary>
        /// Gets or sets true if pan mode is on
    bool _panMode;
    public bool PanMode {
        get { return _panMode; }
        set { SetPropertyAndRaise(ref _panMode, value); }
    }


        /// Scene
        /// </summary>
    public Scene Scene {
        get { return _scene; }
        set {
            if (value == null)
                throw new ArgumentNullException("Scene can not be null");
            if (_scene != null)
                _scene.Dispose();
            _scene = value;
            _scene.LayersOrderChanged += _scene_LayersOrderChanged;
            RebuildLayerList();
            SelectedLayerIndex = 0;

            RaisePropertyChanged(nameof(Scene));
        }
    }


    // コンストラクタ ////////////////////////////////////////////////

    public SceneViewModel()
    {
        Layers = new ObservableCollection<LayerViewModel>();
        _selectedLayerIndex = -1;

        Color = System.Drawing.Color.Black;
        Thickness = 5;
    }


        private void _scene_LayersOrderChanged(object sender, EventArgs e)
        {
            RebuildLayerList();
        }

    void RebuildLayerList()
    {
        this.Layers.Clear();
        for (var i = 0; i < Scene.Layers.Count; i++) {
            Layers.Add(new LayerViewModel(Scene.Layers[i]) {
                            Index = i + 1,
                            Name = String.Format("Layer {0}", i + 1) });
        }

            //SelectedLayerIndex = Scene.SelectedLayerIndex;
    }


    void InvalidateBrushAndPen()
    {
        if (_pen != null)
            _pen.Dispose();
        if (_brush != null)
            _brush.Dispose();

        _brush = new SolidBrush(_color);
        _pen = new System.Drawing.Pen(_brush, Thickness) {
                        StartCap = LineCap.Round,
                        EndCap = LineCap.Round,
                        LineJoin = LineJoin.Round
                    };
        _bandColor = new System.Windows.Media.SolidColorBrush(
                        System.Windows.Media.Color.FromArgb(_color.A, _color.R, _color.G, _color.B)); 
    }


    /// ////////////////////////////////////////////////////////////////////
    // Event Handlers

    bool _pressed;
    System.Drawing.Point _lastPoint;

    public void DrawControl_PreviewMouseDown(object sender, MouseButtonEventArgs e)
    {
        if (!SelectedLayerViewModel.IsVisible)
            return;

        Mouse.Capture((DrawControl) sender);
        _pressed = true;

        System.Drawing.Point pt = e.GetPosition((DrawControl) sender).ToSDPoint();

        if (!PanMode) { 
            Scene.PressDown(SelectedLayerIndex, _brush, Thickness, pt);
        }

        _lastPoint = pt;
    }

    // TODO: view model に移動するほうがよい
    public void DrawControl_MouseMove(object sender, MouseEventArgs e)
    {
        if (!_pressed || !SelectedLayerViewModel.IsVisible)
            return;

        System.Drawing.Point pt = e.GetPosition((DrawControl) sender).ToSDPoint();

        if (PanMode) {
            var dx = pt.X - _lastPoint.X ;
            var dy = pt.Y - _lastPoint.Y;
            Scene.Layers[SelectedLayerIndex].Offset(dx, dy);
        }
        else { 
            Scene.MouseMove(SelectedLayerIndex, _pen, pt);
        }

        _lastPoint = pt;
    }

    // TODO: view model に移動するほうがよい
    public void DrawControl_MouseUp(object sender, MouseButtonEventArgs e)
    {
        if (!_pressed || !SelectedLayerViewModel.IsVisible)
            return;

        Mouse.Capture(null);
        _pressed = false;

        System.Drawing.Point pt = e.GetPosition((DrawControl) sender).ToSDPoint();

        if (!PanMode) { 
            Scene.PressUp(SelectedLayerIndex, pt);
        }
    }


    /// ////////////////////////////////////////////////////////////////////
    // Command Handlers

    internal void OnAddLayer(object sender, ExecutedRoutedEventArgs e)
    {
        Scene.InsertNewLayer(SelectedLayerIndex + 1);
        SelectedLayerIndex += 1;
    }

    internal void OnRemoveLayer(object sender, ExecutedRoutedEventArgs e)
    {
        Scene.RemoveLayerAt(SelectedLayerIndex);
        // 原則は奥のレイヤを選択。一番奥のレイヤを削除したときだけ異なる。
        if (SelectedLayerIndex > 0)
            SelectedLayerIndex -= 1;
    }

    /// Return true if layer can be deleted
    internal void CanRemoveLayer(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = Layers.Count > 1;
    }

    // 手前に移動
    internal void OnUpLayer(object sender, ExecutedRoutedEventArgs e)
    {
        Scene.MoveLayer(SelectedLayerIndex, SelectedLayerIndex + 1) ;
        SelectedLayerIndex += 1;
    }

    /// Returns true if layer can be moved up
    internal void CanMoveSelectedLayerUp(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = SelectedLayerIndex < Layers.Count - 1;
    }

    // 奥に移動
    internal void OnDownLayer(object sender, ExecutedRoutedEventArgs e)
    {
        Scene.MoveLayer(SelectedLayerIndex, SelectedLayerIndex - 1);
        SelectedLayerIndex -= 1;
    }

    /// Returns true if layer can be moved down
    internal void CanMoveSelectedLayerDown(object sender, CanExecuteRoutedEventArgs e)
    {
        e.CanExecute = SelectedLayerIndex > 0;
    }

}
}
