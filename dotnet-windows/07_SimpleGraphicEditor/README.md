
# Simple Graphic Editor

A sample for WPF MVVM.
TargetFrameworkVersion: v4.7.2

Based on <a href="https://github.com/SVoyt/SimpleEditor/">SVoyt/SimpleEditor: Simple graphic editor. Sample for WPF MVVM</a>


●課題
 - ドキュメントの未保存の状態を持たせる。[New] を押したときに、保存しますか? と尋ねる
 - <s>レイヤを削除したときに、選択が外れる。選択しなおしたほうがよい。</s> すみ





## Topic: `ListView` アイテムの表示順を変更

元々の挙動では、手前のレイヤを画面上は、下に表示するようになっていて、UP / DOWN もレイヤの上下と画面上の上下が逆になっていて、紛らわしかった。

WPF では, コレクションの要素の順序にかかわらず、並べ替えて表示することができる。次のようにするのが簡単.

 - `ListView.ItemsSource` として `CollectionViewSource` を指定する。
 - `SortDescriptions` プロパティを適宜、設定する。

`CollectionViewSource` クラスは collection view と source のなかだちをするだけのクラスなので、実体を掴みにくい。
Web上には多様な方法の解説がある。

`CollectionViewSource.GetDefaultView()` で collection view を得て, それを `ListView.ItemsSource` とする方法. `ICollectionView` インタフェイスにも `SortDescriptions` プロパティがある。
  https://www.fenet.jp/dotnet/column/tool/5346/

XAML で SortDescription を記述する例
    http://nineworks2.blog.fc2.com/blog-entry-73.html



## Topic: `Interaction.Triggers` で, イベントハンドラを view model に持たせる

Microsoft.Xaml.Behaviors.Wpf パッケージを使う



## Topic: 低レベルグラフィクス

System.Drawing 名前空間のクラス、メソッドを使う。

System.Windows.Media 名前空間と、各クラスの名前がことごとく衝突していて、混乱している。



## 各クラスの役割分担

MVVM の場合, 次のように役割分担していく:

 1. アプリケィション -- モデルインスタンスを所有する

 2. ドキュメント (モデル) -- データそのものを保持する。view, view model は参照**しない**.

 3. view -- 表示する. 表示内容は view model に格納する

 4. view model -- view に必要な部分に限ったデータを保持する。モデルを参照する。
 
ドキュメント (モデル) の内容に変更があった場合, イベントで view model or view に通知する 

現代では SDI (シングルドキュメントインタフェイス) が前提。
表示は複数の見方でできるようにする。
そうすると, ユーザの操作によってドキュメントを更新したときに, 各 view に通知していくような作り方になる。

