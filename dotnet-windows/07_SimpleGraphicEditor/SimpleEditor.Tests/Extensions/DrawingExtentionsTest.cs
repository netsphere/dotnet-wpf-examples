﻿using System;
using System.Drawing;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleEditor.Extentions;
using Xunit;

namespace SimpleEditor.Tests.Extentions
{

public class DrawingExtentionsTest
{
    [Fact]
    public void NormalizeTest()
    {
            var p = new Point();
            var normalized = p.Normalize(new Point(1, 1));
        Assert.Equal(normalized,new Point(-1,-1));
    }
}
}
