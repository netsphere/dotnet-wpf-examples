
using System;
using System.Drawing;
using SimpleEditor.Models;
using Xunit;


namespace SimpleEditor.Tests.Model
{

public class LayerTest
{
    [Fact]
    public void ConstructorTest()
    {
            var layer = new Layer();
    }

    [Fact]
    public void ConstructorWithBundleTest()
    {
            try
            {
                var nullBundleLayer = new Layer(null);
                Assert.Fail("Layer initiated with null bundle");
            }
            catch (ArgumentNullException e)
            {
                Assert.Equal("layerBundle", e.ParamName);
            }

            var layerBundle = new LayerBundle()
            {
                Bitmap = new Bitmap(1, 1),
                IsVisible = true,
                Position = new Point(),
                Size = new Size(1, 1)
            };

            var layer = new Layer(layerBundle);

            Assert.Equal(layerBundle.Bitmap,layer.Bitmap);
            Assert.Equal(layerBundle.Size, layer.CanvasRect.Size);
            Assert.Equal(layerBundle.Position, layer.CanvasRect.Location);
            Assert.Equal(layerBundle.Size, layer.CanvasRect.Size);
    }

    [Fact]
    public void OffsetTest()
    {
            var layer = new Layer();
            layer.Offset(5,5);
        Assert.Equal(new Point(5,5), layer.CanvasRect.Location);
    }

    [Fact]
    public void DrawLinesTest()
    {
            var layer = new Layer();
            var pen = new Pen(Color.Blue,2);
            var points = new[] {new Point(0, 0), new Point(4, 4)};

            try
            {
                layer.DrawLines(null,points);
                Assert.Fail("Drawing with null pen");
            }
            catch (ArgumentNullException e)
            {
                Assert.Equal("pen", e.ParamName);
            }

            layer.CheckPostionAndSize(new Point(0,0), 5);
            layer.DrawLines(pen,points);
            Assert.Equal(layer.BufferBitmap.GetPixel(2, 2).ToArgb(),Color.Blue.ToArgb());
            Assert.Equal(layer.BufferBitmap.GetPixel(3, 3).ToArgb(), Color.Blue.ToArgb());
    }
}
}
