﻿
using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleEditor.Models;
using Xunit;

namespace SimpleEditor.Tests.Model
{

public class SceneTest
{

    [Fact]
    public void EmptySceneTest()
    {
        using (var scene = new Scene()) { 
            //Assert.False(scene.CanMoveSelectedLayerDown());
            //    Assert.False(scene.CanMoveSelectedLayerUp());
            //    Assert.False(scene.CanRemoveSelectedLayer());
            Assert.True(scene.HasNoLayers);
        }
    }

    [Fact]
    public void AddLayerTest()
    {
        using (var scene = new Scene()) { 
            scene.InsertNewLayer(0);
            //Assert.True(scene.CanRemoveSelectedLayer());
            //    Assert.False(scene.CanMoveSelectedLayerDown());
            //    Assert.False(scene.CanMoveSelectedLayerUp());
            Assert.False(scene.HasNoLayers);
            //Assert.Equal(scene.SelectedLayerIndex, 0);
        }
    }

    [Fact]
    public void AddSecondLayerTest()
    {
        using (var scene = new Scene()) {
            scene.InsertNewLayer(0);
            scene.InsertNewLayer(0);
            //    Assert.True(scene.CanRemoveSelectedLayer());
            //    Assert.True(scene.CanMoveSelectedLayerDown());
            //    Assert.False(scene.CanMoveSelectedLayerUp());
            Assert.False(scene.HasNoLayers);
            //    Assert.Equal(scene.SelectedLayerIndex, 0);
        }
    }

    [Fact]
    public void RemoveTest()
    {
        using (var scene = new Scene()) {
            scene.InsertNewLayer(0);
            scene.InsertNewLayer(0);
            //    Assert.True(scene.CanRemoveSelectedLayer());
            scene.RemoveLayerAt(0);
            //    Assert.Equal(scene.SelectedLayerIndex,0);
            scene.RemoveLayerAt(0);
            //    Assert.Equal(scene.SelectedLayerIndex, -1);
            Assert.True(scene.HasNoLayers);
        }
    }

}
}
