
using System.Windows;
using System.Windows.Input;


namespace DataTableSample
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
public partial class MyApp : Application
{
    private void Application_Startup(object sender, StartupEventArgs e)
    {
        var w = new MainWindow();
        w.Show();
    }
}


// XAML で Command プロパティを使う場合、このようなクラスを設ける.
// アプリケィション全体で使い回す
public static class MyCommands
{
    public static readonly RoutedUICommand AddRow =
            new RoutedUICommand("行を追加する.", "AddRow", typeof(MyCommands));

    public static readonly RoutedUICommand AddCol =
            new RoutedUICommand("列を追加する.", "AddCol", typeof(MyCommands));
}


}
