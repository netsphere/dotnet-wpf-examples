
using System;
using System.Data;
using System.Windows.Input;


namespace DataTableSample
{
internal class DataTableViewModel : MVVM.BindableBase
{
    // コンストラクタ
    public DataTableViewModel()
    {
        //this.DataTableView = new DataView(dataTable);
        //AddRowCommand = new AddRowCommand(this);
        //AddColCommand = new AddColCommand(this);
        InitializeDataTable();

        DataRow row = dataTable.NewRow();
        row[0] = 1;
        row[1] = "John Doe";
        dataTable.Rows.Add(row);
        //AddRow(0, "Jhon");
    }


    // Point! こちらは private にする.
    private readonly DataTable dataTable = new DataTable();

    // bind するためにこうしているが、これは冗長
    //public ICommand AddRowCommand { get; }
    //public ICommand AddColCommand { get; }

    // WPF 側で行を選ぶと set が呼び出される.
    private DataRowView _selectedRow;
    public DataRowView SelectedRow {
        get => _selectedRow;
        set {
            SetPropertyAndRaise(ref _selectedRow, value);
            if (_selectedRow != null) { 
                DataRow row = _selectedRow.Row;
                InputId = (int) row[0];
                InputName = (string) row[1];
                for (int i = 0; i < 5; ++i) {
                    if (row.Table.Columns.Count - 2 > i &&
                        row[i + 2] is not DBNull) { // null ではない
                        Numbers[i] = (double) row[i + 2];
                    }
                    else
                        Numbers[i] = 0.0;
                    RaisePropertyChanged("Numbers"); // ここは "[]" が不要
                }
            }
        }
    }

    private int _inputId;
    public int InputId {
        get => _inputId;
        set {
            SetPropertyAndRaise(ref _inputId, value);
        }
    }

    private string _inputName;
    public string InputName {
        get => _inputName;
        set {
            SetPropertyAndRaise(ref _inputName, value);
        }
    }

    private string _inputColName;
    public string InputColName {
        get => _inputColName;
        set {
            SetPropertyAndRaise(ref _inputColName, value);
        }
    }

    // 配列も bind できる
    public double[] Numbers { get; set; } = new double[5];

    // こちらを <DataGrid> に bind する.
    // 注意! このような使い捨ての書き方にしなければならない。
    public DataView DataTableView => new DataView(dataTable); //{ get; protected set; }

    private void NotifyTableUpdate()
    {
        // ここは DataView のほう. DataView を bind しているため。
        // 名前を明示的に指定.
        RaisePropertyChanged(nameof(DataTableView));
    }

    private void InitializeDataTable()
    {
        dataTable.Columns.Clear();
        dataTable.Rows.Clear();
        dataTable.Columns.Add("ID", typeof(int));
        dataTable.Columns.Add("Name", typeof(string));
        NotifyTableUpdate();
    }


    ////////////////////////////////////////////////////////////
    // Command Handlers

    public void AddRow( object sender, ExecutedRoutedEventArgs ea )// int id, string name)
    {
        var row = dataTable.NewRow();
        row[0] = this._inputId;
        row[1] = this._inputName;
        for (int i = 0; i < Math.Min(dataTable.Columns.Count - 2, 5); ++i) 
            row[i + 2] = Numbers[i];

        dataTable.Rows.Add(row);
        NotifyTableUpdate();  // ここでも notify が必要. メンドい
    }

    public void CanAddRow( object sender, CanExecuteRoutedEventArgs ea ) { 
        ea.CanExecute = this.InputId >= 1 && this._inputName != "";
    }

    public void AddCol( object sender, ExecutedRoutedEventArgs ea )//string name)
    {
        // System.Data.DuplicateNameException: 'A column named 'asdss' already belongs to this DataTable.'
        var col = new DataColumn(
                        $"{dataTable.Columns.Count + 1}:" + this.InputColName, // 重複不可
                        typeof(double) );
                        //Caption = this.InputColName     Caption は効かない! 
        dataTable.Columns.Add(col);
        // 明示的に notify が必要!
        NotifyTableUpdate();
    }

    public void CanAddCol( object sender, CanExecuteRoutedEventArgs ea ) {
        ea.CanExecute = this._inputColName != "";
    }
}

}
