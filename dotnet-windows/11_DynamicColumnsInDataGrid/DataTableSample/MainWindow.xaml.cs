
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;


namespace DataTableSample
{

// 特定の列だけハイライトする方法
[ ValueConversion(typeof(DataGridCell), typeof(Brush)) ]
class HighlightEvenColumns : IValueConverter
{
    // @override
    public object Convert(object value, Type targetType, object parameter,
                          CultureInfo culture)
    {
        DataGridCell dgc = (DataGridCell) value;
        int idx = dgc.Column.DisplayIndex;
        if ( (idx % 2) == 0 )
            return Brushes.LightGreen;  // even
        else
            return DependencyProperty.UnsetValue;
    }

    // @override
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
        // DataContext は XAML 内で指定する
        //DataContext = new DataTableViewModel();

        var vm = (DataTableViewModel) DataContext;
        // こっちから command bind する
        CommandBindings.Add(new CommandBinding(MyCommands.AddRow, 
                                               vm.AddRow, vm.CanAddRow));
        CommandBindings.Add(new CommandBinding(MyCommands.AddCol, 
                                               vm.AddCol, vm.CanAddCol));
    }


    // Event Handlers ////////////////////////////////////////////////////

    void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {

    }
}

}
