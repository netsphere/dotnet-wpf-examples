
# Dynamic Columns in `DataGrid`

Forked from <a href="https://github.com/ishida722/DataTableSample/">GitHub - ishida722/DataTableSample</a>

動的にカラムを追加する

TargetFramework: net6.0-windows




## `DataTable` クラス

列を動的に追加したい場合 *のみ*, `System.Data.DataTable` クラスを使う

`DataTable` は列指向のため、カラムを追加していける。ただ, .NET Framework 1.1 からあるクラスで、使い勝手はかなりよくない。

`DataView` クラスで包み、こちらを `<DataGrid>` に bind する。



## 特定の列だけハイライトする方法

See <a href="https://qiita.com/7of9/items/e7c0a2b013a8aafc1424">Visual Studio / WPF > DataGrid > 2列だけ色を変える > IValueConverterの使用 > 一部失敗</a>
