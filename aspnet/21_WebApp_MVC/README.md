
●●未了:
 - デバッグ用. ルーティングの一覧表示
 - EFcore: トランザクションと `await`, 例外発生
     + 楽観ロック
 - 認可: ロールでリソースアクセス振り分け
 - カスタム 404ページ
 - Pagination
 



# ASP.NET Core MVC 6

ASP.NET Core で、ベーシックなWebアプリを作る。

TargetFramework: net6.0



## バージョン間の非互換

ASP.NET Core は, ASP.NET 4.x を再設計したもの。
非互換な変更が多い, というか、ほかのフレームワークの感覚からしたら, 別物と言ってもいいほど。

C# 8.0, CLR for .NET Core (CoreCLR) 3.0 (LTS は v3.1, 2019年12月) が選択肢になりうる下限。
現実には ASP.NET Core 5.0 以降ならまぁまぁ使える感じ。この間のヴァージョンは全部 skip するがよい。




## How to run

Microsoft.AspNetCore.Authentication.Google パッケージで, Google認証を使う。
Google認証のシークレットを追加する。

[認証情報 - APIとサービス Google Cloud console](https://console.cloud.google.com/apis/credentials)

表示 > [ターミナル]

<pre>&gt; <kbd>cd .\WebApplication1</kbd>
PS WebApplication1&gt; <kbd>dotnet user-secrets set "Authentication:Google:ClientId" "XXXXXXercontent.com"</kbd>
Successfully saved <code>Authentication:Google:ClientId</code> = <var>XXXXXX</var> to the secret store.
PS WebApplication1&gt; <kbd>dotnet user-secrets set "Authentication:Google:ClientSecret" "client-secret"</kbd>
Successfully saved <code>Authentication:Google:ClientSecret</code> = <var>XXXXXXX</var> to the secret store.
</pre>








## プロジェクトをつくる

テンプレートが多すぎる! しかも入れ替わりも激しい。ドキュメントも混乱している。

  - BFF (Backend-for-frontend)
     + ASP.NET Core Web API  -- フロントエンドとのやり取りは OpenAPI (JSON).
     + [新規] ASP.NET Core gRPC サービス -- <code>.proto</code> ファイルでサービスを定義する。

  - Webアプリケィション. Webブラウザからの HTTP リクエストに HTML を送り込む。
     + ASP.NET Core MVC  -- 普通のWebアプリケィションはこれ。
     + ▲ ASP.NET Core Razor Pages -- ページ指向 (PHPに似た形, classic ASP). <code>Pages/</code> 以下に <code>.cshtml</code> ファイルを置いていく。過去プロジェクトの移行など理由がない限り、MVCを使え。

  - SPA
     + [新規] Blazor Server アプリ -- フロントエンドと BFF が一体になったかのように書ける。フロントエンドの <code>.razor</code> ファイル内の C# から, バックエンドのデータを取得するメソッドを普通に呼び出すように書ける。実際には、サーバ側で動いて、DOM の部分更新をおこなう。
     + ▲ [新規] Blazor WebAssembly アプリ -- Webブラウザ上で .NET ランタイムを動かす。
       逆に、BFF 側を別に作らないといけない? (試していない.) 私(堀川) は WebAssembly は筋が悪い (なぜWebブラウザに拘るか? Googleにロックインされる.) と思っているので、Blazor Server のほうがよい。
     
     + React.js での ASP.NET Core -- フロントエンドの React 18 アプリと BFF のC# アプリの2つで一つのvsプロジェクトになる。<code>react</code>, <code>bootstrap</code>, <code>oidc-client</code>, <i>workbox</i> (PWAのためのjsライブラリ) 関連パッケージ。Node.js がフロントエンドのビルドと実行に必要。あくまで React プロジェクトのテンプレート。BFF は別プロジェクトとして追加すればよい。

  - 廃止
     + ASP.NET Web Forms (ASP.NET 4.x)
     + SignalR (ASP.NET 4.x) JavaScript RPC?


このサンプルは, ASP.NET Core Web アプリ (Model-View-Controller) を選んで、そのほか、次のようにする;

<dl compact>
  <dt>フレームワーク:
  <dd>.NET 6.0 LTS
  
  <dt>認証の種類:
  <dd>[個別のアカウント] (Individual User Accounts) を選択. ほかの選択肢は [Microsoft ID プラットフォーム], [Windows]

  <dt>最上位レベルのステートメントを使用しない:
  <dd>チェックを入れる. <code>Program.Main()</code> が作られる。
</dl>

翻訳が, 用語がフラフラして, いまいち・・・



### パッケージを追加

初期状態で、次がインストール済み:
 - Microsoft.AspNetCore.Diagnostics.EntityFrameworkCore
 - Microsoft.AspNetCore.Identity.EntityFrameworkCore
 - Microsoft.AspNetCore.Identity.UI -- ASP.NET Core Identity UI is the default Razor Pages built-in UI for the ASP.NET Core Identity framework.
 - Microsoft.EntityFrameworkCore.SqlServer
 - Microsoft.EntityFrameworkCore.Tools
 
次を追加する:
 - Microsoft.AspNetCore.Authentication.Google 6.0.15




## ASP.NET Core Identity カスタマイズ

デフォルトでは、パッケージに組み込みのページが使われる。なので、どのようなコードになっているか見えない。

カスタマイズしたいときは、ファイルを追加する:
  1. プロジェクトで右クリック -> 追加 -> [新規スキャフォールディングアイテム] (New Scaffolded Item)
      -> "ID" を選んで [追加]ボタン.
  2. オーバーライドするファイルの選択で <code>Account\Register</code>, <code>Account\Login</code> にチェックして [追加]

これで `Areas/Identity/Pages/Account/Register.cshtml` などページが作られるので、編集できるようになる。


### 設定変更

`Program.cs` ファイルでおこなう。
See <a href="https://learn.microsoft.com/en-us/aspnet/core/security/authentication/identity?view=aspnetcore-6.0&tabs=visual-studio">Introduction to Identity on ASP.NET Core</a>

デフォルトでは、実在確認のためのメールサーバが設定されていない。







## Topics

### Routing

まず, `Program.cs` ファイルに、原則を書く。

```csharp
app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");
app.MapRazorPages();
```

各コントローラクラスに、次のように書く:

```csharp
[Route("")] // こう書くことで, /Home ではアクセスできない。
public class HomeController : Controller
{
```



### モデル・クラス, Scaffolding

Code-First で, 先にモデルクラスを `Models/` 以下に単なるクラスとして手書きする。
そののち、データベーススキーマ、コントローラ・クラス、各ビューページを自動で作らせる。

1. プロジェクトで右クリック > 追加 > [新規スキャフォールディングアイテム]
2. [Entity Framework を使用した, ビューがある MVC コントローラー]

 -> これで、<code>ProductsController.cs</code>, <code>Views/Products/</code> 以下に各ページを作ってくれる。DbContext に追加してくれる。

In Visual Studio, use the Package Manager Console to scaffold a new migration for these changes and apply them to the database:

<pre>PM&gt; <kbd>Add-Migration create_products</kbd>
Build started...
Build succeeded.
Microsoft.EntityFrameworkCore.Infrastructure[10403]
      Entity Framework Core 6.0.15 initialized 'ApplicationDbContext' using provider 'Microsoft.EntityFrameworkCore.SqlServer:6.0.15' with options: None
To undo this action, use Remove-Migration.

PM&gt; <kbd>Update-Database</kbd></pre>

ここで一度、プログラムを実行してみてもよい。



### `ApplicationDbContext` クラス

タイムスタンプを更新する。

EFcore は, 保存時に自動で検証を「行わない」!!  な、何だってーーー!!  
そのため、ASP.NET Core 側が model binding のなかで検証を呼び出すようになっている。密結合になってしまっているし、下手くそ。



### `Controllers/`

コントローラごとにクラスをつくり、アクションがメソッドになる。

アクションメソッドの仮引数の名前によって,
URL path, query string, form fields の値を自動で設定してくれる。("Model binding")
レコードに値を設定する場合は、検証も自動で呼び出される。

どのビューを返すか、アクションメソッドから返す。
商品の値を表示するときは `return View(product)`, リダイレクトするときは `return RedirectToAction(nameof(Details), new {id = product.Id})`
not found のときは `return NotFound()`





### `Views/`

`@{...}` で C# 文を埋め込める。`<tr><td>@Html.DisplayFor(modelItem => item.Name)` のように式も埋め込める。

リンクは、次のように `asp-controller` と `asp-action` で、自動的に張ってくれる。
```html
<a class="nav-link text-dark" asp-area="" asp-controller="Home" asp-action="Privacy">Privacy</a>
```

フォームも `asp-for` などを使う

```html
  <div class="row mb-3">
    <label for="Description" class="col-sm-2 col-form-label">Description:</label>
    <div class="col-sm-10">
      <input asp-for="Description" class="form-control" />
      <span asp-validation-for="Description" class="text-danger"></span>
    </div>
  </div>
```





