
using System.ComponentModel.DataAnnotations;  // [Key], IValidatableObject
using System.ComponentModel.DataAnnotations.Schema; // [Table()]
using System.Text;

namespace WebApp_MVC.Models
{

public abstract class TSupportTimestamps
{
    [Column("created_at"), Required]
    public DateTime CreatedAt { get; set; }

    [Column("updated_at"), Required]
    public DateTime UpdatedAt { get; set; }
}

// C# 8.0 からインタフェイスのデフォルト実装が可能になった。CLI/CLR の変更。
// "Model binding" が自動で検証をおこなうため, この定義の仕方では不味い.
interface IValidationCallbacks: IValidatableObject
{
    // 暗黙に public. `virtual` を付けても, サブクラスでのメソッド定義が自動的
    // に override になるのを強制できない. うーむ.
    //void BeforeValidation() { }

    void BeforeSave() { }
}


// このクラスは手で書く
[Table("products")]
public class Product: TSupportTimestamps, IValidatableObject
{
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; protected set; }

    [Required]
    public string Name { get; set; }

    [Required(AllowEmptyStrings = true)]
    [DisplayFormat(ConvertEmptyStringToNull = false)] // これが必要!!
    public string Description { get; set; }

/*
    // 1. Interface のメソッドを実装するときは, public にするか, メソッドに
    //    interface 名を付けなければならない。`override` キーワードは、デフォルト
    //    実装があってもエラーになる。
    // 2. 後者の書き方なら `public` を省略できるが, インタフェイス経由で呼び出
    //    せてしまう。何なんだ
    // 3. デフォルト実装がある場合, public でなくインタフェイス名も付けないと,
    //    別メソッドになってしまう。分かりにくい!
    public void BeforeValidation()
    {
        Console.WriteLine("BeforeValidation() enter");
    }
*/

    // override
    // このメソッドは .NET Framework 4.0 からある。
    public IEnumerable<ValidationResult> Validate(ValidationContext context)
    {
        Console.WriteLine("Product#Validate() enter");

        // "Model binding" が自動で検証するため、このメソッド内で before
        // validation の内容を記述するしかない。 
        Name = Name.Normalize(NormalizationForm.FormKC);
        Description = Description.Normalize(NormalizationForm.FormC);

        if (Name == "NG") {
            yield return new ValidationResult("Name must not be 'NG'", new[] {"Name"});
        }
    }
}

}
