using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore; // IdentityDbContext クラス
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.ComponentModel.DataAnnotations;
using WebApp_MVC.Models;

namespace WebApp_MVC.Data
{

// IdentityDbContext クラスは中身がない。何重にも及ぶ派生関係
//    < IdentityDbContext<IdentityUser, IdentityRole, string>
//    < IdentityDbContext<TUser, TRole, TKey>   このクラスも中身なし
//          where TUser : IdentityUser<TKey>
//                TRole : IdentityRole<TKey>
//                TKey : IEquatable<TKey>
//    < IdentityDbContext<...>   このクラスが実体
//    < IdentityUserContext<...>
//    < DbContext
public class ApplicationDbContext :
                    IdentityDbContext<IdentityUser, IdentityRole, string>
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
    {
        // イベントの呼び出し順序を確認する。validation の「前」に呼び出される
        // のはどれか? -> 結果: どれでもない!
        SavingChanges += ApplicationDbContext_SavingChanges;
        SavedChanges += ApplicationDbContext_SavedChanges;
        SaveChangesFailed += ApplicationDbContext_SaveChangesFailed;
        ChangeTracker.Tracked += ChangeTracker_Tracked;
        ChangeTracker.StateChanged += ChangeTracker_StateChanged;
    }

    // Callback. 1. dbContext.Add() で, レコードごとに呼び出される.
    //              まだ SaveChangesAsync() を呼び出す前.
    private void ChangeTracker_Tracked(object? sender, EntityTrackedEventArgs e)
    {
        Console.WriteLine("_Tracked() enter"); 
    }

    // Callback. 2. SaveChangesAsync() に入ったタイミングで, 1回だけ呼び出される.
    //              before_save を呼び出すなら, ここ。
    private void ApplicationDbContext_SavingChanges(object? sender,
                                                    SavingChangesEventArgs e)
    {
        Console.WriteLine("ApplicationDbContext_SavingChanges() enter"); 
    }

    // Callback. 3. レコードが保存・更新された後に, レコードごとに呼び出される。
    //              `SavedChanges` イベントより前に呼び出される.
    private void ChangeTracker_StateChanged(object? sender, EntityStateChangedEventArgs e)
    {
        Console.WriteLine("_StateChanged() enter"); 
    }

    private void ApplicationDbContext_SaveChangesFailed(object? sender, SaveChangesFailedEventArgs e)
    {
        Console.WriteLine("_SaveChangesFailed() enter"); 
    }

    // Callback. 4. SaveChangesAsync() から抜けるタイミングで, 1回だけ呼び出される.
    private void ApplicationDbContext_SavedChanges(object? sender, SavedChangesEventArgs e)
    {
        //throw new NotImplementedException();
        Console.WriteLine("ApplicationDbContext_SavedChanges() enter"); 
    }


    // モデルクラスを作ったら、ここに追加していく.
    // [Entity Framework を使用した, ビューがある MVC コントローラー] だと、こ
    // れも自動で追加してくれる。(単数形になる)
    public DbSet<WebApp_MVC.Models.Product> Product { get; set; }

    public override Task<int> SaveChangesAsync(
                            CancellationToken cancellationToken = default)
    {
        Console.WriteLine("SaveChangesAsync() enter");

        UpdateTimestamps();  // => これを `SavingChanges` イベントに移してよい。
        return base.SaveChangesAsync(cancellationToken);
    }


    private void UpdateTimestamps()
    {
        DateTime now = DateTime.UtcNow;

        // entry はモデル・クラスではない。
        foreach ( EntityEntry entry in ChangeTracker.Entries() ) {
            if ( entry.Entity is TSupportTimestamps entity ) {
                switch (entry.State) {
                case EntityState.Modified:
                    entity.UpdatedAt = now;
                    break; // C# は fallthrough できない
                case EntityState.Added:
                    entity.CreatedAt = now;
                    entity.UpdatedAt = now;
                    break;
                }
            }
/*
EFcore は、保存時に自動で検証を「行わない」。な、何だってーーー!!!!
ASP.NET Core の model binding のなかで検証が行われる。チグハグ感.

TryValidateModel(e2); => これは ControllerBase のメソッド. EFcore ではない!

Validator.ValidateObject(e2, new ValidationContext(e2), true);
     => 検証に失敗したとき ValidationException 例外を発生. 複数のエラーを報告
        できない
TryValidateObject() にするとエラーリストを得る.
*/
            if (entry.Entity is IValidationCallbacks e2 ) {
                e2.BeforeSave();
            }
        }
    }
}

}
