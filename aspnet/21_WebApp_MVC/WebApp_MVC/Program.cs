using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Text;
using WebApp_MVC.Data;

namespace WebApp_MVC
{

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        //////////////////////////////////////
        // Add services to the container.
        
        var connectionString = builder.Configuration.GetConnectionString("DefaultConnection") ?? throw new InvalidOperationException("Connection string 'DefaultConnection' not found.");
        builder.Services.AddDbContext<ApplicationDbContext>(options =>
                        options.UseSqlServer(connectionString));
        builder.Services.AddDatabaseDeveloperPageExceptionFilter();

        // 認証
        builder.Services.AddDefaultIdentity<IdentityUser>( options => 
                        options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<ApplicationDbContext>();

        // Google?F??
        builder.Services.AddAuthentication().AddGoogle( googleOptions => {
                googleOptions.ClientId = builder.Configuration["Authentication:Google:ClientId"];
                googleOptions.ClientSecret = builder.Configuration["Authentication:Google:ClientSecret"];
        });

        builder.Services.AddControllersWithViews();

        //////////////////////////////////////

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment()) {
            app.UseMigrationsEndPoint();
            app.UseDeveloperExceptionPage();
        }
        else {
            app.UseExceptionHandler("/Home/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseStaticFiles();

        app.UseRouting();

        app.UseAuthentication();
        app.UseAuthorization();

        app.MapControllerRoute(
                name: "default",
                pattern: "{controller}/{action=Index}/{id?}");
        app.MapRazorPages();


// DEBUG  ●未了。かなり難しい。なんで最初から組込みで用意されてないんか?
        if ( app.Environment.IsDevelopment() ) {
            app.MapGet("/debug/routes", (IEnumerable<EndpointDataSource> endpointSources) => {
var sb = "";//new StringBuilder();
var endpoints = endpointSources.SelectMany(es => es.Endpoints);
foreach (Endpoint endpoint in endpoints) {
    if (endpoint is RouteEndpoint routeEndpoint) {
        sb += routeEndpoint.RoutePattern.RawText;
        sb += " " + routeEndpoint.RoutePattern.PathSegments;
        sb += " " + routeEndpoint.RoutePattern.Parameters;
        sb += " " + routeEndpoint.RoutePattern.InboundPrecedence;
        sb += " " + routeEndpoint.RoutePattern.OutboundPrecedence;
    }

    var routeNameMetadata = endpoint.Metadata.OfType<Microsoft.AspNetCore.Routing.RouteNameMetadata>().FirstOrDefault();
    sb += routeNameMetadata?.RouteName;

    var httpMethodsMetadata = endpoint.Metadata.OfType<HttpMethodMetadata>().FirstOrDefault();
    sb += httpMethodsMetadata?.HttpMethods; // [GET, POST, ...]
    sb += "\n";
}
return sb.ToString();
            });
        }

        app.Run();
    }
}

}
