using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp_MVC.Data;
using WebApp_MVC.Models;

namespace WebApp_MVC.Controllers
{

[Route("")] // こう書くことで, `/Home` ではアクセスできない。
public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly ApplicationDbContext _dbContext;

    // サービスを DI で注入する。(複数可)
    // ILogger<> は組み込みサービスの一つ。`appsettings.json` で LogLevel など設定.
    // コンストラクタは一つだけしか置けない。
    public HomeController(ILogger<HomeController> logger,
                          ApplicationDbContext dbContext)
    {
        _logger = logger;
        _dbContext = dbContext;
    }


    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }

    // GET /Privacy
    [HttpGet, Route("{action}")]
    public async Task<IActionResult> Privacy()
    {
        return View();
    }


    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, 
                   NoStore = true)]
    //[HttpGet, Route("{action}")]
    [NonAction]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}

}
