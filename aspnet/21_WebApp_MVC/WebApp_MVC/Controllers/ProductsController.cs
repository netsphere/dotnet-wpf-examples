using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApp_MVC.Data;
using WebApp_MVC.Models;

namespace WebApp_MVC.Controllers
{

// Controller < ControllerBase
public class ProductsController : Controller
{
    private readonly ApplicationDbContext _dbContext;

    // コントローラの構築子は一つのみ。(オーバロードすると実行時エラー)
    // Program.Main() で builder.Services に登録したオブジェクトを, dependency
    // injection (DI) で受け取れる. 
    // 構築子で注入するので "constructor injection" type. 
    public ProductsController(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    // GET Products
    [HttpGet]
    public async Task<IActionResult> Index()
    {
        return View(await _dbContext.Product.ToListAsync());
               // Problem("Entity set 'ApplicationDbContext.Product'  is null.");
    }


    // GET Products/Details/5
    // 仮引数の "名前" によって, 自動的に URL から抽出される。旧ASP.NET の
    // [FromUri] は廃止。
    [HttpGet]
    public async Task<IActionResult> Details( int id)
    {
        var product = await _dbContext.Product.FindAsync(id);
                //.FirstOrDefaultAsync(m => m.Id == id);
        if (product == null)
            return NotFound();

        return View(product);
    }


    // GET Products/New
    [HttpGet]
    public IActionResult New()
    {
        return View();
    }


    // POST Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(
                                [Bind("Name,Description")] Product product )
    {
        // アクションメソッドの仮引数の"名前"によって、query string, form fields
        // の値から設定される。
        // "Model binding" は, `[Bind]` で指示し, レコードの組み立てと, 自動で
        // 検証もおこなう。その結果が ModelState に入る。
        Console.WriteLine("get ModelState.IsValid");
        if ( !ModelState.IsValid ) {
            return View("New", product);
        }

        _dbContext.Add(product);
        await _dbContext.SaveChangesAsync();
        return RedirectToAction(nameof(Details), new {id = product.Id});
    }


    // GET Products/Edit/5
    [HttpGet]
    public async Task<IActionResult> Edit(int id)
    {
        var product = await _dbContext.Product.FindAsync(id);
        if (product == null)
            return NotFound();
            
        return View(product);
    }


    // POST Products/Update/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Update(int id,
                                [Bind("Name,Description")] Product product)
    {
        var upd_product = await _dbContext.Product.FindAsync(id);
        if (upd_product == null)
            return NotFound();

        if ( !ModelState.IsValid ) {
            return View("Edit", product);
        }

        try {
            upd_product.Name = product.Name; // ●下手くそ。もっといい方法ない?
            upd_product.Description = product.Description;
            //_dbContext.Update(product);
            await _dbContext.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException) {
            if (!ProductExists(upd_product.Id))
                return NotFound();
            throw;
        }

        return RedirectToAction(nameof(Details), new {id = upd_product.Id});
    }

/*
    // GET: Products/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
            if (id == null || _context.Product == null)
            {
                return NotFound();
            }

            var product = await _context.Product
                .FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }
*/

    // POST Products/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Delete(int id)
    {
        var product = await _dbContext.Product.FindAsync(id);
        if (product == null)
            return NotFound();

        _dbContext.Product.Remove(product);
        await _dbContext.SaveChangesAsync();

        return RedirectToAction(nameof(Index));
    }


    private bool ProductExists(int id)
    {
        return (_dbContext.Product?.Any(e => e.Id == id)).GetValueOrDefault();
    }
}

}
