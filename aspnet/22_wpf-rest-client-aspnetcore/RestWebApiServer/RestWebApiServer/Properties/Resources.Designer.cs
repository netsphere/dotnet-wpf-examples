﻿//------------------------------------------------------------------------------
// <auto-generated>
//     このコードはツールによって生成されました。
//     ランタイム バージョン:4.0.30319.42000
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。
// </auto-generated>
//------------------------------------------------------------------------------

namespace RestWebApiServer.Properties {
    using System;
    
    
    /// <summary>
    ///   ローカライズされた文字列などを検索するための、厳密に型指定されたリソース クラスです。
    /// </summary>
    // このクラスは StronglyTypedResourceBuilder クラスが ResGen
    // または Visual Studio のようなツールを使用して自動生成されました。
    // メンバーを追加または削除するには、.ResX ファイルを編集して、/str オプションと共に
    // ResGen を実行し直すか、または VS プロジェクトをビルドし直します。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   このクラスで使用されているキャッシュされた ResourceManager インスタンスを返します。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("RestWebApiServer.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   すべてについて、現在のスレッドの CurrentUICulture プロパティをオーバーライドします
        ///   現在のスレッドの CurrentUICulture プロパティをオーバーライドします。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   -----BEGIN PUBLIC KEY-----
        ///MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0pVxGw7AyxaJW+qsHRmt
        ///fh3gvBg7jSsnRCtH7qYVhtL2DNrcp/U+NyStqdMwHhQUF9iKHLqMQwlqRE5Rh9XG
        ///ss+lh4gsN/6UEz32D4hNxrlvIOAwD4pB/yBUz20PA4Wzly1I/igprNzQWPUsUi2y
        ///B7C54IEJYsv5UabpjnH6gzsJcFZUZLQAvV7QZTlzVMMxFBK9rtXLxcsMPz9C2LBD
        ///VrhPEL9HbWZfIX+rb4DvatnHRLeQyKGlhCoGYLfeQ212Ncj3CgVRODAHjXFQ5lBk
        ///9/4BnpALOBi7bnjJBgD8wyzeFmrP6e7ixqzQla2fcvOogfQTvmADtlQIve8vNqFM
        ///dQIDAQAB
        ///-----END PUBLIC KEY-----
        /// に類似しているローカライズされた文字列を検索します。
        /// </summary>
        internal static string issuerPub {
            get {
                return ResourceManager.GetString("issuerPub", resourceCulture);
            }
        }
    }
}
