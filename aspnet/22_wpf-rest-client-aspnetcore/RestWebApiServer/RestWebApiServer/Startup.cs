﻿﻿// -*- coding:utf-8-with-signature -*-

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using RestWebApiServer.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace RestWebApiServer
{
// class Program から呼び出される.
// クラス名は "Startup" でなければならない。でないと scaffolding が失敗。
public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }


    // This method gets called by the runtime. Use this method to add services
    // to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        // Microsoft.AspNetCore.Diagnostics.EntityFrameworkCore パッケージ
        // services.AddDatabaseDeveloperPageExceptionFilter();

        services.AddControllers();
        services.AddSwaggerGen( (SwaggerGenOptions c) => {
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "RestWebApiServer",
                                                     Version = "v1" });
            c.EnableAnnotations();

            // Enables support for nullable object properties
            // どういうのが生成されるかは、例えば,
            // https://stackoverflow.com/questions/66442081/nullable-property-is-not-presented-in-the-swashbuckle-aspnetcore-openapi-schema
            // OpenAPI 3.1 は, OpenAPI 3.0.x と互換性がなく, `nullable` が削除され, また書き方が変わる:
            // https://stackoverflow.com/questions/40920441/how-to-specify-a-property-can-be-null-or-a-reference-with-swagger/48114924#48114924
            // やっぱり, `nullable` + `allOf` はキモいので、コメントアウト.
            //c.UseAllOfToExtendReferenceSchemas();

            // セキュリティ "定義"
            // Web 上には "new ApiKeyScheme" というのも見えるが, 廃れた (.NET Core 2.x 時代?)
            // See https://www.c-sharpcorner.com/article/upgrade-net-core-2-12-2-application-to-net-core-3-1/
            c.AddSecurityDefinition("My-Bearer-JWT", // name
                new OpenApiSecurityScheme {
                        Name = "Bearer", // required. これがヘッダ名になる.
                        Type = SecuritySchemeType.Http, // こっちが正解。Authorization: Bearer ... になる
                        //Type = SecuritySchemeType.ApiKey, // required
                        In = ParameterLocation.Header, // required. Query, Header, ...
                        Scheme = "Bearer",  // required. "Basic", "Bearer", ...
                        BearerFormat = "JWT",
                        Description = "JWT Authorization header using the Bearer scheme."
                });

            // これも必要. セキュリティ "要求"
            c.AddSecurityRequirement(new OpenApiSecurityRequirement() {
                {
                    new OpenApiSecurityScheme {
                        Reference = new OpenApiReference {
                                        Type = ReferenceType.SecurityScheme,
                                        Id = "My-Bearer-JWT" },
                        Scheme = "oauth2",
                        Name = "Bearer",
                        In = ParameterLocation.Header,
                    },
                    new List<string>()
                }});
        });

        // ASP.NET では "認証" する整理になっている
        services.AddAuthentication( (options) => {
            //options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            // fallback. 共通のデフォルト値
            options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        })
        .AddJwtBearer( (JwtBearerOptions options) => {
            // Authority must use HTTPS unless disabled for development by setting
            // `RequireHttpsMetadata=false`.
            // OpenID Connect を呼び出す。(実際に接続しにいく)
            //   -> JWT 認可では設定してはいけない
            //options.Authority = "https://localhost:4001";

            // OpenID Connect IdP の "aud" と照合.
            //options.Audience = "hage"; // client_id

            options.TokenValidationParameters = new TokenValidationParameters() {
                RequireExpirationTime = true, // 設定必須. Web 上の解説はこれを忘れているのが多い。
                ValidateIssuer = true,     // 設定必須
                ValidateAudience = true,   // 設定必須
                ValidateLifetime = true,   // 設定必須. こちらは 'nbf' claim <= 'exp' claim のチェック
                ValidateIssuerSigningKey = true,

                ValidIssuer = "https://localhost:4001",
                ValidAudience = "hage",  // client_id, 完全一致
                IssuerSigningKeys = GetSecurityKeys(),
            };
            /* 認可エラーの原因を探るときは、次のコードを有効にすればよい.
            options.Events = new JwtBearerEvents() {
                    OnAuthenticationFailed = (context) => {
                        context.NoResult();
                        return Task.CompletedTask;
                    },
                    OnChallenge = (context) => {
                        context.HandleResponse();
                        return Task.CompletedTask;
                    } }; */
        });

        // データベースに接続する。
        services.AddDbContext<RestWebApiServerContext>( (options) =>
                options.UseSqlServer(Configuration.GetConnectionString("RestWebApiServerContext")));
    }


    private static IEnumerable<SecurityKey> GetSecurityKeys()
    {
        // 公開鍵を読み込むのも、いろいろな方法がある
        // https://www.scottbrady91.com/c-sharp/rsa-key-loading-dotnet
        var key = System.Security.Cryptography.RSA.Create();
        key.ImportFromPem(Properties.Resources.issuerPub);

        return new List<SecurityKey> { new RsaSecurityKey(key) };
    }


    // This method gets called by the runtime. Use this method to configure the
    // HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment()) {
            // 例外の表示.
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI( (c) => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RestWebApiServer v1"));
        }

        // Middleware を並べることができる。
        app.UseRouting();

        // ASP.NET Core では, JWT は "認証" の整理
        app.UseAuthentication();

        app.UseAuthorization();  // allows the use of `[Authorize]` on controllers and actions

        app.UseEndpoints( (endpoints) => {
            endpoints.MapControllers();
        });
    }
} // class MyStartup

}
