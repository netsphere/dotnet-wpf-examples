//using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc; // [Route], [ApiController], [HttpGet]
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Validations.Rules;
using RestWebApiServer.Data;
using RestWebApiServer.Models;
using Swashbuckle.AspNetCore.Annotations; // 注意! これだけでは有効にならない.
                                          // See Startup#ConfigureServices()

/*
ページネーションの方法として, offset pagination, cursor pagination がある。
ページネーションが id のみであれば、cursor pagination のほうが、非常に高速
   See https://note.com/tomo_program/n/nbb010ff6eede
       オフセット・ページネーションとカーソル・ページネーションの比較
   See https://khalidabuhakmeh.com/cursor-paging-with-entity-framework-core-and-aspnet-core
       Cursor Paging With Entity Framework Core and ASP.NET Core

 * */

namespace RestWebApiServer.Controllers
{

// リフレクションによって, プロパティ名が暗黙にクエリパラメータ名になる。
// クエリパラメータに記号があるときは Name= で指定する.
public class QueryFilter
{
    [FromQuery(Name ="$orderby")]
    public string OrderBy { get; set; }

    // 何個 skip するか。ページネーションではない。0 で先頭から取得.
    [FromQuery(Name = "$skip")]
    public int Skip { get; set; }

    // 何個取得するか
    [FromQuery(Name ="$top")]
    public int Top { get; set; }

    // Match するもの
    [FromQuery(Name = "$search")]
    public string Search { get; set; }

    // 全体の件数も返す `$count=true`
    [FromQuery(Name = "$count")]
    public bool Count { get; set; }

    public void check()
    {
        if (Skip < 0) Skip = 0;
        if (Top < 1) Top = 20; else if (Top > 100) Top = 100;
    }
}

/*
public class PagedResponse<Ty> : ResponseCacheAttribute<>
*/

// [Route("api/[controller]")]  これだと api/Books と大文字になる
// API は `ControllerBase` から派生させる。`Controller` はビューサポートあり.
[Route("api/books")]
[ApiController]
[Authorize]
public class BooksController : ControllerBase
{
    readonly RestWebApiServerContext _context;

    public BooksController(RestWebApiServerContext context)
    {
        _context = context;
    }

    // GET: api/books
    // Client クラスに押し込められるので, FindAllだけではどのリソースか分からない。
    // 複数の戻り値 (レコード or NotFound or その他) がある場合、戻り値の型は,
    // 次のいずれか:
    //   - IActionResult
    //   - async Task<IActionResult>
    [HttpGet]
    [SwaggerOperation(OperationId = "FindAllBooks")]
    public async Task<ActionResult<IEnumerable<Book>>> GetBook(
                                    [FromQuery] QueryFilter filter)
    {
        filter.check();
        var data = await _context.Book
                                .Skip( filter.Skip) .Take( filter.Top)
                                .ToListAsync();
        var count = await _context.Book.CountAsync();
        return data; // ●● TODO: 個数も返す.
    }


    // GET: api/books/5
    // 複数の戻り値 (レコード or NotFound など) がある場合, 戻り値の型は、次の
    // いずれか:
    //   - ActionResult<Ty>
    //   - async Task<ActionResult<Ty>>
    [HttpGet("{id}")]
    [SwaggerOperation(OperationId = "FindBook")]
    public async Task<ActionResult<Book>> GetBook(int id)
    {
        var book = await _context.Book.FindAsync(id);
        if (book == null)
            return NotFound();

        return book;
    }

    // PUT: api/books/5
    // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
    [HttpPut("{id}")]
    [SwaggerOperation(OperationId = "UpdateBook")]
    public async Task<IActionResult> PutBook(int id, [FromBody] Book book)
    {
        if (id != book.Id)
            return BadRequest();

        _context.Entry(book).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
    }

    // POST: api/Books
    // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
    [HttpPost]
    [SwaggerOperation(OperationId = "CreateBook")]
    // [ProducesResponseType(StatusCodes.Status201Created)]  // クライアントは 200 を期待しているので、指定不可.
    public async Task<ActionResult<Book>> PostBook([FromBody] Book book)
    {
        _context.Book.Add(book);
        await _context.SaveChangesAsync();

        return Ok(book);
    }

    // DELETE: api/books/5
    [HttpDelete("{id}")]
    [SwaggerOperation(OperationId = "DestroyBook")]
    public async Task<IActionResult> DeleteBook(int id)
    {
        var book = await _context.Book.FindAsync(id);
        if (book == null)
            return NotFound();

        _context.Book.Remove(book);
        await _context.SaveChangesAsync();

        return NoContent();
    }

    private bool BookExists(int id)
    {
        return _context.Book.Any(e => e.Id == id);
    }
}

}
