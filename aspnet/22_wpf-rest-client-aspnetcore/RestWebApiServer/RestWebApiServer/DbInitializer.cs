
using Microsoft.EntityFrameworkCore;
using RestWebApiServer.Data;

namespace RestWebApiServer
{

internal static class DbInitializer
{
    // Database が存在しなければ, 作成する。
    // Program.CreateDbIfNotExists() から呼び出される
    public static void Initialize(RestWebApiServerContext context)
    {
        // Database がなければ作成する。
        // すでに database があって、かつテーブルもあるときは、何もしない
        // (Auto-migration ではない)
        context.Database.EnsureCreated();

        // 自動 migration
        //context.Database.Migrate();

        // Seed があれば、ここで投入する
    }
}

}
