
# REST API Server (アプリケィションサーバ)

Web 上の解説は、内容が古かったり (ASP.NET Core 3.1 LTS より前は、内容が違うのが多い), 方針の誤りが多くて、見極めが難しい。







## How to run

"パッケージマネージャーコンソール" を開く.

<pre>
利用可能なすべての NuGet コマンドを参照するには、'get-help NuGet' を入力します。

PM&gt; <kbd>Add-Migration SetupTables</kbd>
Build started...
Build succeeded.
To undo this action, use Remove-Migration.
</pre>



DBMS は Microsoft SQL Server を利用.

Swagger UI (OpenAPI Spec 3)
http://localhost:33199/swagger/ で、人が試すことができる。
  -> API のレスポンスは JSON で返す







## ASP.NET Core で JWT を検証 verify する

Web 上の記事が盛大に混乱している。

class `Startup` の `ConfigureServices` メソッド内で設定する。ややこしいのは ASP.NET Core では JWT "認証" として整理されていて, `services.AddAuthentication()`, `.AddJwtBearer()` にまず設定を記述する。

OpenID Connect 認証 (これは認証で合っている) の設定を兼ねているので、それも混乱を助長する。

結局, 次の設定だけでよい。

```csharp
        .AddJwtBearer( (JwtBearerOptions options) => {
            // Authority must use HTTPS unless disabled for development by setting
            // `RequireHttpsMetadata=false`.
            // OpenID Connect を呼び出す。(実際に接続しにいく)
            //   -> JWT 認可では設定してはいけない
            //options.Authority = "https://localhost:4001";

            // OpenID Connect IdP の "aud" と照合.
            //options.Audience = "hage"; // client_id

            options.TokenValidationParameters = new TokenValidationParameters() {
                RequireExpirationTime = true, // 設定必須. Web 上の解説はこれを忘れているのが多い。
                ValidateIssuer = true,     // 設定必須
                ValidateAudience = true,   // 設定必須
                ValidateLifetime = true,   // 設定必須. こちらは 'nbf' claim <= 'exp' claim のチェック
                ValidateIssuerSigningKey = true,

                ValidIssuer = "https://localhost:4001",
                ValidAudience = "hage",  // client_id, 完全一致
                IssuerSigningKeys = GetSecurityKeys(),
            };
```

この中から呼び出している `GetSecurityKeys()` は次のようになる。

```csharp
    private static IEnumerable<SecurityKey> GetSecurityKeys()
    {
        // 公開鍵を読み込むのも、いろいろな方法がある
        // https://www.scottbrady91.com/c-sharp/rsa-key-loading-dotnet
        var key = System.Security.Cryptography.RSA.Create();
        key.ImportFromPem(Properties.Resources.issuerPub);

        return new List<SecurityKey> { new RsaSecurityKey(key) };
    }
```

ここまでちゃんとできた上で, OpenAPI (Swagger) の設定をおこなう

```csharp
        services.AddSwaggerGen( (c) => {
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "RestWebApiServer",
                                                     Version = "v1" });
            c.EnableAnnotations();

            // セキュリティ "定義"
            // Web 上には "new ApiKeyScheme" というのも見えるが, 廃れた (.NET Core 2.x 時代?)
            // See https://www.c-sharpcorner.com/article/upgrade-net-core-2-12-2-application-to-net-core-3-1/
            c.AddSecurityDefinition("My-Bearer-JWT", // name
                new OpenApiSecurityScheme {
                        Name = "Bearer", // required. これがヘッダ名になる.
                        Type = SecuritySchemeType.Http, // こっちが正解。Authorization: Bearer ... になる
                        //Type = SecuritySchemeType.ApiKey, // required
                        In = ParameterLocation.Header, // required. Query, Header, ...
                        Scheme = "Bearer",  // required. "Basic", "Bearer", ...
                        BearerFormat = "JWT",
                        Description = "JWT Authorization header using the Bearer scheme."
                });

            // これも必要. セキュリティ "要求"
            c.AddSecurityRequirement(new OpenApiSecurityRequirement() {
                {
                    new OpenApiSecurityScheme {
                        Reference = new OpenApiReference {
                                        Type = ReferenceType.SecurityScheme,
                                        Id = "My-Bearer-JWT" },
                        Scheme = "oauth2",
                        Name = "Bearer",
                        In = ParameterLocation.Header,
                    },
                    new List<string>()
                }});
        });
```
