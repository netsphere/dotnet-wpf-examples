using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Client_Web_API.Views
{
    /// <summary>
    /// books_page.xaml の相互作用ロジック
    /// </summary>
public partial class BooksEditWindow : Window
{
    private int _id;

    // DataContext に設定
    //private Book _book;

    public BooksEditWindow(int id)
    {
        this._id = id > 0 ? id : -1;
        //this._book = new Book();

        InitializeComponent();
    }

    private async void Page_Loaded(object sender, RoutedEventArgs e)
    {
        var client = ((MyApp) Application.Current).client;
        if (_id >= 1) { 
            Book book = await client.FindBookAsync(this._id);
            DataContext = book;
            okButton.Content = "Update";
        }
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {

    }

    // [更新] ボタン
    private async void okButton_Click(object sender, RoutedEventArgs e)
    {
        Book book = (Book) DataContext;
        var client = ((MyApp) Application.Current).client;
        if (_id >= 1) 
            await client.UpdateBookAsync(_id, book);
        else {
            // status 200 での返却を期待. 201 だと逆に例外発生.
            DataContext = await client.CreateBookAsync(book);
            _id = ((Book) DataContext).Id;
        }
    }


    private void Button_Click_2(object sender, RoutedEventArgs e)
    {

    }

    private void Button_Click_3(object sender, RoutedEventArgs e)
    {

    }

}

}
