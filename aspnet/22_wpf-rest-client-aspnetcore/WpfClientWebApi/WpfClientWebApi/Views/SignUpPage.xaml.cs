using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Client_Web_API.Views
{
    /// <summary>
    /// HomePage.xaml の相互作用ロジック
    /// </summary>
public partial class SignUpPage : Page
{
    public SignUpPage()
    {
        InitializeComponent();
    }

    void btnLogIn_Click(object sender, RoutedEventArgs e)
    {
        // TODO: impl.    
    }

    // JWT token を更新
    private void btnSetToken_Click(object sender, RoutedEventArgs e)
    {
        var app = (MyApp) Application.Current;
        app.accessToken =  txtToken.Text;
        txtToken.Text = "";
    }

} // class SignUpPage

}
