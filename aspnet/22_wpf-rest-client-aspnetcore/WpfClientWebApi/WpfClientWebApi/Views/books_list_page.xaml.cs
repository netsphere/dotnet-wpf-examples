using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Client_Web_API.Views
{

class BooksListPageViewModel : DependencyObject
{
    public string Page { get; private set; }

    // DataGrid.ItemsSource -> StaticResource booksSource
    // CollectionViewSource.Source -> "GridItems"
    public static readonly DependencyProperty GridItemsProperty =
                    DependencyProperty.Register("GridItems",
                                typeof(ExObservableCollection<Book>),
                                typeof(BooksListPageViewModel));

    public BooksListPageViewModel()
    {
        Page = "";

        SetValue(GridItemsProperty, new ExObservableCollection<Book>());
        itemsRefresh();
    }

    async void itemsRefresh()
    {
        var items = (ExObservableCollection<Book>) GetValue(GridItemsProperty);
        items.Clear();

        var app = Application.Current as MyApp;
        if (app != null) {  // XAMLエディタ対策
            var client = app.client;
            try { 
                var data = await client.FindAllBooksAsync("", 0, 100, "");
                items.AddRange(data);
            } catch (HttpRequestException ex) {
                MessageBox.Show("http error");
            }
        }
    }
}


    /// <summary>
    /// books_list_page.xaml の相互作用ロジック
    /// </summary>
public partial class BooksListPage : Page
{
    public BooksListPage()
    {
        InitializeComponent();
    }

    private void btnPrevious_Click(object sender, RoutedEventArgs e)
    {

    }

    private void btnNext_Click(object sender, RoutedEventArgs e)
    {

    }

    // [新しい本...]
    private void btnNewItem(object sender, RoutedEventArgs e)
    {
        var w = new BooksEditWindow(-1);
        w.Show();
    }
}

}
