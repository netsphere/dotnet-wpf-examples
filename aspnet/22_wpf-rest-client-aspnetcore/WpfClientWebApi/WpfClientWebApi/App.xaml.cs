using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;

namespace WPF_Client_Web_API
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
public partial class MyApp : Application
{
    public string? accessToken { private get; set; }

    // http://localhost:33199/api/books/1 
    private System.Net.Http.HttpClient _httpClient = new System.Net.Http.HttpClient();

    private Client? _client;
    public Client client { get {
        if (_client == null)
            _client = new Client("http://localhost:33199/", _httpClient);
        _httpClient.DefaultRequestHeaders.Remove("Authorization");
        _httpClient.DefaultRequestHeaders.Add("Authorization",
                                              "Bearer " + accessToken);

        return _client;
    } }


    private void Application_Startup(object sender, StartupEventArgs e)
    {
        var wnd = new MainWindow();
        wnd.Show();
    }
}

}
