
using NSwag;
using NSwag.CodeGeneration.CSharp;

namespace client_code_gen
{

internal class Program
{
    // .NET Framework 4.5で導入
    private static System.Net.Http.HttpClient wclient = new HttpClient();

    // 非同期 Main メソッドは, Task を返すこと
    static async Task Main(string[] args)
    {
        // .NET Framework 1.1時代のAPI. 非推奨
        //System.Net.WebClient wclient = new System.Net.WebClient();

        // 静的に保存する
        //var wclient = new System.Net.Http.HttpClient();
        string res_body = await wclient.GetStringAsync(@"http://localhost:33199/swagger/v1/swagger.json");
        var document = await OpenApiDocument.FromJsonAsync(res_body);
        //wclient.Dispose();

        var settings = new CSharpClientGeneratorSettings {
            ClassName = "Client", 
            CSharpGeneratorSettings = {
                Namespace = "WPF_Client_Web_API"
            }
        };

        var generator = new CSharpClientGenerator(document, settings);	
        var code = generator.GenerateFile();
        var directory = System.IO.Directory.GetCurrentDirectory();
        var filePath = $"{directory}/swagger.cs";

        if (System.IO.File.Exists(filePath)) { 
            System.IO.File.Delete(filePath);
            Console.WriteLine($"Deleted {filePath}");
        }
        System.IO.File.WriteAllText(filePath, code);
        Console.WriteLine($"Wrote {filePath}");
    }
}

}
