# -*- coding: utf-8 -*-

require 'jwt'

rsa_private = OpenSSL::PKey::RSA.new(File.read("hoge"))

# 必須クレームは RFC 7523 を見よ. 署名の求めもある
# https://www.rfc-editor.org/rfc/rfc7523.html#section-3

payload = {
  id: 1,
  iss: 'https://localhost:4001',   # 必須 issuer. 完全一致    -> options.Authority を有効にすると, 名前解決しようとして失敗
  sub: 'AccessToken',              # 必須 subject. 値は issuer が決めればよい。
  aud: 'hage',                     # 必須 audience. 完全一致
  nbf: (Time.now - 10).to_i,            # 任意
  exp: (Time.now + 60 * 60 * 24).to_i,  # 必須. この例では 1 day

  nameid: 'user-name@hoge',
}

p payload

token = JWT.encode(payload, rsa_private, 'RS256')
puts token

# 復号テスト
# エラー時: Signature verification failed (JWT::VerificationError)
#p JWT.decode(token, rsa_private, true, { algorithm: 'RS256' })
