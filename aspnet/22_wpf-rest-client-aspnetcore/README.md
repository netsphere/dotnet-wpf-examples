
# OpenAPI による API 連携

## サーバ側

ASP.NET Core






## クライアント側

コードの自動生成
  Deprecated  <s><kbd>nswag webapi2openapi</kbd></s>


Generate C# client code from a Swagger specification

<pre>
<kbd>nswag openapi2csclient /input:MyWebService.json /classname:MyServiceClient 
                       /namespace:MyNamespace  /output:MyServiceClient.cs</kbd>
</pre>





## JWT 認可

### 全体像

REST API サーバでは, JWT でリソースへのアクセスを認可する。認証ではない.

この記事がまとまっている; <a href="https://koduki.hatenablog.com/entry/2019/11/03/163014">そもそもJWTに関する私の理解は完全に間違っていた！ - ブログなんだよもん</a>



よくある解説では、アプリケィションサーバが、認証をおこなって JWT も発行する。これは誤り。

この建付けでは、認証部分だけ Web ページを持たせるか、クライアントアプリ上に認証画面を作る形になる。前者はアプリケィションサーバに不要なミドルウェアが必要になるし、後者はユーザがWebブラウザなどのパスワードマネジャを使えず、ユーザ体験が悪い。

JWT は, クッキーによらない、ただのアクセストークンなので、次のように構成しないといけない。図の resource server がアプリケィションサーバ.

![access token](AccessToken.jpg)   ![refresh token](RefreshToken.jpg)
図の出典 https://www.c-sharpcorner.com/blogs/access-token-and-refresh-token-in-web-api

クライアントアプリは、定期的に、リフレッシュトークンでアクセストークンを更新する。

RFC 8252 <i>OAuth 2.0 for Native Apps</i> の図を載せる。ユーザ認証はWebブラウザと IdP サーバが行い、client app は IdP からトークンの発行を受け, (前図のように) アクセストークンでアプリケィションサーバにアクセスする。

<pre>
  +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
  |          User Device          |
  |                               |
  | +--------------------------+  | (5) Authorization  +---------------+
  | |                          |  |     Code           |               |
  | |        Client App        |---------------------->|     Token     |
  | |                          |<----------------------|    Endpoint   |
  | +--------------------------+  | (6) Access Token,  |               |
  |   |             ^             |     Refresh Token  +---------------+
  |   |             |             |
  |   |             |             |
  |   | (1)         | (4)         |
  |   | Authorizat- | Authoriza-  |
  |   | ion Request | tion Code   |
  |   |             |             |
  |   |             |             |
  |   v             |             |
  | +---------------------------+ | (2) Authorization  +---------------+
  | |                           | |     Request        |               |
  | |          Browser          |--------------------->| Authorization |
  | |                           |<---------------------|    Endpoint   |
  | +---------------------------+ | (3) Authorization  |               |
  |                               |     Code           +---------------+
  +~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
﻿</pre>



### JWT の署名 sign と検証 verify

前述のように、認証サーバとアプリケィションサーバを分ける以上、アクセストークンである JWT は, IdP が秘密鍵で署名し、アプリケィションサーバ側はその公開鍵で検証しなければならない。

これも、よくある解説やサンプルでは、雑に共通鍵を使っており、誤りの記事が多い。

このサンプルプログラムでは, IdP までは作らない。秘密鍵から JWT を作る小さなスクリプトを用意する。



### 秘密鍵・公開鍵の生成

秘密鍵 hoge と 公開鍵 hoge.pub を生成しよう。まず秘密鍵を作る

<pre>
$ <s><kbd>ssh-keygen -f hoge</kbd></s> こうじゃない。PEMファイルを作る
</pre>

<pre>
$ <kbd>openssl genrsa -out hoge</kbd>
Generating RSA private key, 2048 bit long modulus (2 primes)
..............................................................+++++
............+++++
e is 65537 (0x010001)
</pre>

公開鍵を作る

<pre>
$ <kbd>openssl rsa -in hoge -pubout -out hoge.pub</kbd>
writing RSA key
</pre>

内容を表示

<pre>
$ <kbd>openssl rsa -text -pubin < hoge.pub</kbd>
RSA Public-Key: (2048 bit)
以下略
</pre>

