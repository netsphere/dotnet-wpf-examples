﻿﻿-*- coding:utf-8-with-signature -*-

# Hosted-Blazor-Wasm + SignalR + Radzen.Blazor sample


● TODO: 
 - メソッド名が文字列でのやり取りが難点。
   <a href="https://blog.neno.dev/entry/2022/12/21/100259">【C#】SignalR にも SwaggerUI 的なのがほしい！</a> の方法を試すこと。

 - 認証情報を利用したリソース認可

 - UI components の利用。特に、表やチャート、CRUD など。



## .NET 8 からの Blazor プロジェクト

ASP.NET Core 8.0 で, Blazor プロジェクトテンプレートがリニューアルされた。.NET 7 以前の解説がそのままでは使えない場合がある。

選択肢は多いものの, 制限が多い。
 - ▲ スタンドアロン Blazor WebAssemblyは、起動時に長時間待たされ、ユーザ体験がよくない。

 - 混合モデル: コンポーネント単位での, 静的SSR, `InteractiveServer`, `InteractiveWebAssembly` の組み合わせ。
   + `@rendermode="InteractiveServer"` はキワモノ枠。クライアント側でのイベント発生のつど SignalR でサーバのメソッドを呼び出す。現実的でない。
   + `InteractiveAuto` は, コンポーネントをサーバでもクライアントでも動くようにしなければならず、これも現実的でない。
   + 消去法で `InteractiveWebAssembly`.

React Server Components のように、親子コンポーネントでサーバ側とクライアント側を混ぜられる (Islands Architecture) といいが、
Blazor はそのようになっていない。`InteractiveServer`, `InteractiveWebAssembly` コンポーネントには, それぞれ同じレンダモードのコンポーネントしか置けない。そうすると, root を静的SSRにして, interactive にしたいコンポーネントをどちらかにする形になる。

また, Per page/component は, 次のファイルがサーバ側に置かれる; `Routes.razor` -> `MainLayout.razor` -> `NavMenu.razor`.
やはり Islands Architecture でないため, 狙った挙動にしにくい。

Interactive location: Global にする。結局, .NET 7 以前の ASP.NET Core Hosted に似た構成。




## Topic: RPC

Hosted-Blazor-Wasm + SignalR で簡単に成功. これでダメなら, もう Swagger/OpenAPI か OData ぐらいだった

公式のチュートリアルに沿って実装するだけ.
<a href="https://learn.microsoft.com/en-us/aspnet/core/blazor/tutorials/signalr-blazor">Use ASP.NET Core SignalR with Blazor</a> 



### hosted サーバ

`Microsoft.AspNetCore.SignalR` 名前空間, Microsoft.AspNetCore.App フレームワークに含まれる。追加パッケージ不要。

`Microsoft.AspNetCore.SignalR.Hub` クラスを継承したクラスを作る。例えば `ChatHub` クラス。

`Main()` 内で, 次のようにしてサービスを登録。型はコンクリートクラス。
```csharp
    app.MapHub<ChatHub>("/chathub");
```

サーバ側は、標準で、型を利用した形にできる。
```csharp
public class ChatHub : Hub<IChatResponse>, IChatHubContract
```



### クライアント - Blazor Wasm

<s>Microsoft.AspNetCore.SignalR.Client パッケージを追加。</s> 上手くいかず。もう一度試すか




### gRPC (失敗)

このサンプルは動いたが, 
https://github.com/grpc/grpc-dotnet/tree/master/examples/Blazor/

自分でプロジェクトを作ると、どうしても動かず。何かごく簡単な誤認がなるのだろうが、解消できず。



code first

<a href="https://learn.microsoft.com/ja-jp/aspnet/core/grpc/code-first">.NET を使用したコードファーストの gRPC サービスとクライアント</a>
  protobuf-net.Grpc を使う

もうちょっと詳しいの
https://github.com/hakenr/BlazorGrpcWebCodeFirst




## Topic: UI components

Blazor の UIコンポーネントライブラリは、3択;
 - MudBlazor
 - Ant Design Blazor
 - Radzen Blazor Components

The Fluent UI Blazor library はあまりよくなさそう。



### Radzen.Blazor

Hosted サーバとクライアントの両方に Radzen.Blazor パッケージをインストールしないといけない。`Program.cs` についても, サーバとクライアントの両方で次が必要;
```csharp
    builder.Services.AddRadzenComponents(); 
```



### ▲ MudBlazor (失敗)

最初インストールはすんなり成功したが, 狙った挙動にしづらい。





