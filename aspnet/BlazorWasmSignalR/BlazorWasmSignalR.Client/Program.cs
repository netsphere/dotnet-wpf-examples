﻿using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Radzen;
//using MudBlazor.Services;

namespace BlazorApp1.Client
{

internal class Program
{
    static async Task Main(string[] args)
    {
        var builder = WebAssemblyHostBuilder.CreateDefault(args);

            builder.Services.AddAuthorizationCore();
            builder.Services.AddCascadingAuthenticationState();
            builder.Services.AddAuthenticationStateDeserialization();

        // 追加. サーバとクライアントの両方に追加が必要!
        //builder.Services.AddMudServices();
        builder.Services.AddRadzenComponents(); // クライアントも必要。インタラクティブにならない。

        await builder.Build().RunAsync();
    }
}

}
