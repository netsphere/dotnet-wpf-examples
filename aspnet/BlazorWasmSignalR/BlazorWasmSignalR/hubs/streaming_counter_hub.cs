﻿using Microsoft.AspNetCore.SignalR;
using System.Runtime.CompilerServices;

namespace BlazorApp1.hubs
{

public interface IStreamingCounter
{
    public IAsyncEnumerable<int> CounterEnumerable(
                    int count, 
                    int delay,  // ミリ秒
                    CancellationToken cancellationToken);
}


public class StreamingCounterHub : Hub, IStreamingCounter
{
    // @override IStreamingCounter
    public async IAsyncEnumerable<int> CounterEnumerable(
            int count, int delay, 
            [EnumeratorCancellation] CancellationToken cancellationToken)
    {
        for (int i = 0; i < count; ++i) {
            // Check the cancellation token regularly so that the server will
            // stop producing items if the client disconnects.
            // System.OperationCanceledException 例外 -> クライアントで受ける
            cancellationToken.ThrowIfCancellationRequested();

            // Use the cancellationToken in other APIs that accept cancellation
            // tokens so the cancellation can flow down to them.
            // cancellationToken を渡すと, キャンセルされていたら例外発生
            await Task.Delay(delay) ; //, cancellationToken);

            yield return i;
        }
    }
}

}

