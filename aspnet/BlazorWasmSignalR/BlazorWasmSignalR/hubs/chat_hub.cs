﻿
using Microsoft.AspNetCore.SignalR;
using SharedLibrary;

namespace BlazorApp1.hubs
{

// - メソッド呼び出しの度に, 新しい Hub インスタンスが生成される。
//     -> プロパティに状態を格納してはいけない.
// - `Hub` は抽象クラス, 複数の Hub<TClient> から派生できない。`Clients` プロパティは
//   base に依存。
//     -> レスポンスは、固定の型一つだけになる.
public class ChatHub : Hub<IChatResponse>, IChatHubContract
{
    // @override IChatHubContract
    // 型指定なし:
    //   クライアントからは, 次のように呼び出す.
    //     await hubConnection.SendAsync("SendMessage", userInput, messageInput);
    //   第1引数がメソッド名。なのでインタフェイス定義がいらない。
    // 型指定あり:
    //   Hub<クライアント側の型> から派生させる
    public async Task SendMessage(string user, string message)
    {
        // クライアントへの broadcast
        // `SendAsync()` は必ず `await` で受けないといけない。メソッドから先に抜けると
        // 失敗する可能性がある。
        await this.Clients.All.ReceiveMessage(user, message);
    }
}

}
