﻿
namespace SharedLibrary
{

public interface IChatResponse
{
    Task ReceiveMessage(string user, string message);
}


public interface IChatHubContract
{
    // Return type must be `Task` or `Task<T>`
    Task SendMessage(string user, string message);
}

}
