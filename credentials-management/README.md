﻿﻿ -*- coding:utf-8-with-signature -*-

# Credential Manager (資格情報マネージャー)

コントロールパネルからユーザが操作できる。次の区分がある

 - Web 資格情報
 - Windows 資格情報
   + Windows 資格情報
   + 証明書に基づいた資格情報
   + 汎用資格情報




## Credentials を列挙する

`CredRead()`, `CredWrite()` 関数で読み書きする。
Type = `CRED_TYPE_GENERIC` の場合, ほかのアプリケィションから普通に読み取れる。

Windows は、アプリケィション単位の隔離ができない (その考えがない). パスワードをこの方法で保存してはならない。

接続パスワードを格納するための Type = `CRED_TYPE_DOMAIN_PASSWORD` では、アプリケィションからパスワードを読み取れない。おそらく、書き込めるが, 読み出せない ●要確認

▲ パスワードを一括で抜くツールがあるので、やはりアプリケィション隔離できないようだ。 https://github.com/AlessandroZ/LaZagne/

UWP には the Credential Locker がある。`PasswordVault` クラスを使う。UWP アプリは (そのアプリの内容 + 現在のユーザ) にのみアクセスできるようだが、通常のデスクトップアプリはそうではない。



参考:
  [【C#】Windows資格情報を操作する](https://outofmem.hatenablog.com/entry/2014/07/03/065349)
  リポジトリ: https://github.com/tumbling-dice/snippets/tree/master/C%20Sharp/ManagedCred
  `CredUIPromptForWindowsCredentials()` の使い方を兼ねる





## ▲ `CredUIPromptForCredentials()` + `CredUIConfirmCredentials()` - Windows XP

この関数は古い。`CredUIPromptForWindowsCredentials()` を使え.

ダイアログボックスを表示して、ユーザにパスワード入力を求め、そこで得られたパスワードを使う。

[ユーザーに資格情報の要求](https://learn.microsoft.com/ja-jp/windows/win32/secbp/asking-the-user-for-credentials)

日本語の解説付き
https://eternalwindows.jp/security/credman/credman01.html
`CredUIConfirmCredentials()` を呼び出すことで保存される



## `CredUIPromptForWindowsCredentials()` - Windows Vista 以降

この関数は credential を保存しない。●どうすると??

[How to validate credentials with `CredUIPromptForWindowsCredentials`](https://stackoverflow.com/questions/52786299/how-to-validate-credentials-with-creduipromptforwindowscredentials?rq=3)

日本語の解説
https://eternalwindows.jp/security/credman/credman02.html

生体認証のサンプル
https://learn.microsoft.com/ja-jp/windows/win32/secbiomet/creating-client-applications


