﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    
static class NativeMethods
{
    // @return 成功した場合 true
    [DllImport("advapi32", SetLastError = true, CharSet = CharSet.Unicode)]
    public static extern bool CredEnumerate(
                string filter,          // flags = CRED_ENUMERATE_ALL_CREDENTIALS のとき, NULL にする
                uint flags,             // 0 or CRED_ENUMERATE_ALL_CREDENTIALS
                out uint count,         // 返される資格情報の数
                out IntPtr pCredentials);  // CredFree() で解放すること.

    public const uint CRED_ENUMERATE_ALL_CREDENTIALS = 0x1;

    [DllImport("advapi32", SetLastError = false, CharSet = CharSet.None)]
    public static extern void CredFree(IntPtr buffer);

    public enum CRED_TYPE : uint
    {
        GENERIC = 1,
        DOMAIN_PASSWORD = 2,
        DOMAIN_CERTIFICATE = 3,
        DOMAIN_VISIBLE_PASSWORD = 4,  // no longer supported
        GENERIC_CERTIFICATE = 5,
        DOMAIN_EXTENDED = 6,
        MAXIMUM = 7,
        MAXIMUM_EX = (MAXIMUM + 1000),
    }

    public enum CRED_PERSIST : uint
    {
        SESSION = 1,     // ログオフしたら削除される
        LOCAL_MACHINE = 2,
        ENTERPRISE = 3,  // 永続的に保存される
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)] // "W" 固定
    public struct CREDENTIAL_ATTRIBUTE
    {
        string Keyword;
        uint Flags;
        uint ValueSize;
        IntPtr Value;  // バイト列へのポインタ
    }

        /// <summary>
        /// アンマネージドなCredential
        /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)] // "W" 固定
    public struct CREDENTIAL
    {
        public uint Flags;  // bit member
        public CRED_TYPE Type;
        public string TargetName;
        public string Comment;
        public System.Runtime.InteropServices.ComTypes.FILETIME LastWritten;
        public uint CredentialBlobSize;

        // バイト列
        public IntPtr CredentialBlob;

        public CRED_PERSIST Persist;

        public uint AttributeCount;
        public IntPtr Attributes;
        public string TargetAlias;
        public string UserName;
    }

        /// <summary>
        /// マネージコードに置き換えたCredential
        /// </summary>
    public class ManagedCredential
    {
        public uint Flags { get; set; }
        public CRED_TYPE Type { get; set; }
        public string TargetName { get; set; }
        public string Comment { get; set; }
        public System.Runtime.InteropServices.ComTypes.FILETIME LastWritten { get; set; }

        public byte[] CredentialBlob { get; set; }
            
        public CRED_PERSIST Persist { get; set; }

        public CREDENTIAL_ATTRIBUTE[] Attributes { get; set; }

        public string TargetAlias { get; set; }
        public string UserName { get; set; }

        // 変換する
        public ManagedCredential(in CREDENTIAL unCred)
        {
            Flags = unCred.Flags;
            Type = unCred.Type;
            TargetName = unCred.TargetName;
            Comment = unCred.Comment;
            LastWritten = unCred.LastWritten;
            CredentialBlob = new byte[unCred.CredentialBlobSize];
            if (unCred.CredentialBlobSize > 0) { 
                Marshal.Copy(unCred.CredentialBlob, CredentialBlob, 0, 
                             (int) unCred.CredentialBlobSize);
            }

            Persist = unCred.Persist;
            Attributes = new CREDENTIAL_ATTRIBUTE[unCred.AttributeCount];
            for (int i = 0; i < unCred.AttributeCount; i++) {
                Attributes[i] = Marshal.PtrToStructure<CREDENTIAL_ATTRIBUTE>(
                                        unCred.Attributes + Marshal.SizeOf(typeof(CREDENTIAL_ATTRIBUTE)));
            }

            TargetAlias = unCred.TargetAlias;
            UserName = unCred.UserName;
        }

        // パスワードが平文で入っている!
        public string? Password {
            get {
                if (CredentialBlob.Length == 0)
                    return null;

                if (Type == CRED_TYPE.DOMAIN_PASSWORD)
                    return Encoding.UTF8.GetString(CredentialBlob); // the plaintext Unicode password for `UserName`.
                else if (Type == CRED_TYPE.DOMAIN_CERTIFICATE)
                    return Encoding.UTF8.GetString(CredentialBlob); // the clear test Unicode PIN for `UserName`.

                return null;
            }
            set {
                if ( !string.IsNullOrEmpty(value) )
                    CredentialBlob = Encoding.UTF8.GetBytes(value);
            }
        }
    }
}

}
