﻿
using static ConsoleApp1.NativeMethods;
using System.Runtime.InteropServices;

// 資格情報を列挙する

namespace ConsoleApp1
{

internal class Program
{
    static void Main(string[] args)
    {
        uint count = 0;
        IntPtr pCredentials = IntPtr.Zero;

        bool ret = CredEnumerate(null, 
                        0, //NativeMethods.CRED_ENUMERATE_ALL_CREDENTIALS, 
                        out count, 
                        out pCredentials); // ここがポインタの配列
        if ( !ret ) {
            Console.WriteLine("CredEnumerate() failed.");
            return;
        }

        Console.WriteLine("count = {0}", count);

        ManagedCredential[] credentials = new ManagedCredential[count];
        // Managed に変換する
        for (int n = 0; n < count; n++) { 
            // CredEnumerate() で取得した IntPtr を構造体に変換する
            IntPtr unCredPtr = Marshal.ReadIntPtr(pCredentials, n * Marshal.SizeOf(typeof(IntPtr)));
            CREDENTIAL unCred = Marshal.PtrToStructure<CREDENTIAL>(unCredPtr);

            credentials[n] = new ManagedCredential(unCred);
        }
        // もういらん
        NativeMethods.CredFree(pCredentials);

        foreach (var cred in credentials) { 
            Console.WriteLine("Flags:{0}", cred.Flags);
            Console.WriteLine("Type:{0}", cred.Type);
            Console.WriteLine("TargetName:{0}", cred.TargetName);
            Console.WriteLine("Comment:{0}", cred.Comment);
            Console.WriteLine("Persist:{0}", cred.Persist);
            Console.WriteLine("TargetAlias:{0}", cred.TargetAlias);
            Console.WriteLine("UserName:{0}", cred.UserName);
            Console.WriteLine("Password:{0}", cred.Password); // これは取れない. そりゃそうだ。
            Console.WriteLine("* * * * * * * * * *");
        }

        Console.ReadKey();
    }
}
}
