
# wpf-samples

.NET Core 3.0 [2019年9月] から再び WPF が使えるようになり, .NET 5.0 でも普通に動いている。WPF はアプリケーション開発の選択肢として選べる。

<i>Silverlight</i>, UWP などに移行したほうがいいのか, 先行きに不安があった。Silverlight は単に終了した。UWP もデスクトップ分野では主流にならない。デスクトップアプリに回帰し, 畳むのが確かになった。

WPF は多様な書き方が可能だが、かえってどのように書くべきなのかがさっぱり分からない。整理しておく。

Website:
 - <a href="https://www.nslabs.jp/dotnet-examples.rhtml">.NET WPF サンプル集 [.NET 6, netFx47 対応]</a>
 - <a href="https://www.nslabs.jp/dotnet-examples-asp.rhtml">ASP.NET Core サンプル集 [.NET 6 対応]</a>





## CSV ライブラリ

<a href="https://www.joelverhagen.com/blog/2020/12/fastest-net-csv-parsers">The fastest CSV parser in .NET</a>

 1. <i>Sep</i> がぶっちぎり。`Unescape = true` オプションを渡さないとダブルクォーテーションを unescape しない。.NET 7.0 以降でしか動かない。
 2. <i>RecordParser</i>  .NET 6.0, .NET Standard 2.1. 
 4. <i>Sylvan.Data.Csv</i>  .NET 6.0, .NET Standard 2.0.
 
 8. ☆ <i>CsvHelper</i> - もっともメジャ. .NET 6.0, .NET Standard 2.0, .NET Framework 4.6.2. 単にこれで問題ない。


そのほか .NET 6 でも使えるもの。ただ、遅い。
 21. ▲ ServiceStack.Text           JSON, JSV and CSV. 商用ライブラリ
 26. CsvTextFieldParser    MIT     Microsoft.VisualBasic アセンブリの置き換え.
 30. ▲ LumenWorksCsvReader          最新が 2018年リリース。開発終了
 32. TinyCsvParser         MIT
 36. ▲ Csv                   MIT     ずいぶん遅い
