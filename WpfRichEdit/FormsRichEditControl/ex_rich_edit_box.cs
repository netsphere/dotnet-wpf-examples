﻿
using System;
using System.Runtime.InteropServices;
using WpfApp1;


namespace WindowsFormsControlLibrary1
{

public class ExRichEditBox : System.Windows.Forms.RichTextBox, IDisposable
{
    // Release() が必要
    protected IRichEditOle _richEditOle = null;

    // Track whether `Dispose()` has been called.
    private bool _disposed = false;

    public IRichEditOle GetRichEditOle()
    {
        if (_richEditOle != null)
            return _richEditOle;

        int r = NativeMethods.SendMessage(this.Handle,
                                          NativeMethods.Message.EM_GETOLEINTERFACE,
                                          IntPtr.Zero,
                                          out _richEditOle); // これが簡単
        return _richEditOle;
    }

    // @implements IDisposable
    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }

    protected override void Dispose(bool disposing)
    {
        if (_disposed)
            return;

        if (disposing) {
            // Here, dispose managed resources.
            // _stream.Dispose();
        }

        // Free unmanaged resources here.
        if (_richEditOle != null ) {
            Marshal.ReleaseComObject(_richEditOle);
            _richEditOle = null;
        }

        _disposed = true;

        base.Dispose(disposing); // これが必須
    }

    // Unmanaged resources があるとき、ファイナライザが必要。
    ~ExRichEditBox()
    {
        // 二重解放を避ける
        Dispose(disposing: false);
    }
}

}
