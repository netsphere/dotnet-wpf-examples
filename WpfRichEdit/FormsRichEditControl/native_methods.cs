﻿
using System;
using System.Runtime.InteropServices;


namespace WpfApp1
{

static class NativeMethods
{ 
    [DllImport("User32.dll", SetLastError=true, CharSet=CharSet.Auto)]
    public static extern int SendMessage(IntPtr hWnd, Message message, 
                                         IntPtr wParam,  // 64bit環境では64bit
                                         IntPtr lParam); // 64bit環境では64bit

    // 特製
    [DllImport("User32.dll", SetLastError=true, CharSet=CharSet.Auto)]
    public static extern int SendMessage(IntPtr hWnd, Message message, 
                                         IntPtr wParam,  // 64bit環境では64bit
                                         out IRichEditOle lParam); // 64bit環境では64bit
	
    public enum Message { 
        WM_USER = 0x0400,
        EM_GETOLEINTERFACE = WM_USER + 60, // magic!
    }
}
}
