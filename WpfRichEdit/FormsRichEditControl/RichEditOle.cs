﻿
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using Vanara.PInvoke;
using static Vanara.PInvoke.Ole32;


// C++
// c:/Program Files (x86)/Windows Kits/10/Include/10.0.22621.0/um/RichOle.h

// .NET でのやり方
// See https://lets-csharp.com/richtextbox-bmp-insert/
//     RichTextBoxにBitmapを挿入する方法

// Visual Studio 2022
// アセンブリ参照から NuGet パッケージに変更 -> Vanara があれば Microsoft.VisualStudio.OLE.Interop パッケージは不要

// [PreserveSig] attribute を付けると、例外ではなく、戻り値でエラーを通知.
// DllImport("ole32.dll", PreserveSig = false) のようにすると、戻り値の型は void,
// エラーを例外として送出
// `HRESULT` は Vanara.PInvoke で定義される

namespace WpfApp1
{

[ComImport]
[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
[Guid("00020D00-0000-0000-c000-000000000046")]
public interface IRichEditOle
{
    // STDMETHOD(GetClientSite) (THIS_ LPOLECLIENTSITE FAR * lplpolesite) PURE;
    //[return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    HRESULT GetClientSite(out IOleClientSite lplpolesite);

    [return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    int GetObjectCount();

    [return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    int GetLinkCount();

    //[return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    HRESULT GetObject(int iob, [In,Out] REOBJECT lpreobject,
                  [MarshalAs(UnmanagedType.U4)]GETOBJECTOPTIONS flags);

    //[return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    HRESULT InsertObject(REOBJECT lpreobject);

    //[return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    HRESULT ConvertObject(int iob, Guid rclsidNew, string lpstrUserTypeNew);

    //[return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    HRESULT ActivateAs(Guid rclsid, Guid rclsidAs);

    //[return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    HRESULT SetHostNames(string lpstrContainerApp, string lpstrContainerObj);

    //[return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    HRESULT SetLinkAvailable(int iob, bool fAvailable);

    //[return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    HRESULT SetDvaspect(int iob, uint dvaspect);

    //[return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    HRESULT HandsOffStorage(int iob);

    //[return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    HRESULT SaveCompleted(int iob, IStorage lpstg);

    //[return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    HRESULT InPlaceDeactivate();

    //[return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    HRESULT ContextSensitiveHelp(bool fEnterMode);

    // STDMETHOD(GetClipboardData) (THIS_ CHARRANGE FAR * lpchrg, DWORD reco,
    //    							LPDATAOBJECT FAR * lplpdataobj) PURE;
    //[return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    HRESULT GetClipboardData([In,Out] ref CHARRANGE lpchrg,
                         [MarshalAs(UnmanagedType.U4)] GETCLIPBOARDDATAFLAGS reco,
                         out IDataObject lplpdataobj);

    // STDMETHOD(ImportDataObject) (THIS_ LPDATAOBJECT lpdataobj,
    // 								CLIPFORMAT cf, HGLOBAL hMetaPict) PURE;
    //[return: MarshalAs(UnmanagedType.I4)]
    [PreserveSig]
    HRESULT ImportDataObject(IDataObject lpdataobj, int cf, IntPtr hMetaPict);
}

    public enum GETOBJECTOPTIONS
	{
		REO_GETOBJ_NO_INTERFACES	= 0x00000000,
		REO_GETOBJ_POLEOBJ			= 0x00000001,
		REO_GETOBJ_PSTG				= 0x00000002,
		REO_GETOBJ_POLESITE			= 0x00000004,
		REO_GETOBJ_ALL_INTERFACES	= 0x00000007,
	}

    [StructLayout(LayoutKind.Sequential)]
    public struct CHARRANGE {
        public int cpMin;
        public int cpMax;
    }

    public enum GETCLIPBOARDDATAFLAGS {
        RECO_PASTE = 0,
        RECO_DROP = 1,
        RECO_COPY = 2,
        RECO_CUT = 3,
        RECO_DRAG = 4
    }

// これがキモ!
[StructLayout(LayoutKind.Sequential)]
public class REOBJECT
{
    public uint           cbStruct = (uint) Marshal.SizeOf(typeof(REOBJECT));		// Size of structure
    public int            cp = 0;					    // Character position of object
    public Guid           clsid;						// Class ID of object
    public IOleObject     poleobj; 						// OLE object interface
    public IStorage       pstg;							// Associated storage interface
    public IOleClientSite polesite;						// Associated client site interface
    public Size           sizel = new Size();			// Size of object (may be 0,0)
    public uint           dvaspect = 0;					// Display aspect to use
    public uint           dwFlags = 0;					// Object status flags
    public uint           dwUser = 0;					// Dword for user's use

    public const uint REO_RESIZABLE     = 0x00000001; // Object may be resized
    public const uint REO_BELOWBASELINE = 0x00000002; // Object sits below the baseline

    // Place object at selection
    public const int REO_CP_SELECTION = -1;
}

}
