﻿
# WPF から `Forms.RichTextBox` を使う

WPF の `RichTextBox` コントロールは Win32 RichEdit control ではない独自実装。RTF ファイルは読み込めるが, OLE オブジェクトは表示されない。新たに埋め込むこともできない。

このサンプルは, WPF ウィンドウに `Forms.RichTextBox` コントロールを置いて, OLE オブジェクトを埋め込む。



 1. WPF ウィンドウに WinForms コントロールを置く

`<WindowsFormsHost>` コントロールを置けばよい。ただし、Forms コントロールは WPF コントロールより手前に表示サれるので、レイアウトに注意が必要。

```xaml
    <WindowsFormsHost x:Name="formsHost" HorizontalAlignment="Stretch" 
                      VerticalAlignment="Stretch" />
```

先行プロジェクト
 - これはよい. WPF から使うサンプル: <a href="https://github.com/garakutanokiseki/RTFEditor/">garakutanokiseki/RTFEditor: Rich Text Editor (WinForms) control for WPF.</a>
   そのオリジナル: https://www.codeproject.com/Articles/50139/WPF-RichTextEditor-with-Toolbar



 2. [オブジェクトの挿入] ダイアログ

`OleUIInsertObject()` 関数を呼び出すだけ.

とはいえ `NativeMethods` クラスを手書きするのは地味に面倒。Vanara.PInvoke.OleDlg パッケージを使った。これなら簡単。net45 ～ net8.0-windows まで対応。不足する定数などは自分で定義。

Microsoft.VisualStudio.OLE.Interop パッケージは不要。





 3. OLEオブジェクトを生成して、埋め込む

キモは `REOBJECT` 構造体。これをつくって `IRichEditOle::InsertObject()` を呼び出す。


参考:
  ほぼこの通りを実装した <a href="https://lets-csharp.com/richtextbox-bmp-insert/">RichTextBoxにBitmapを挿入する方法 | 鳩でもわかるC#</a>


関連
 - <a href="https://www.codeproject.com/Articles/12135/Inserting-images-into-a-RichTextBox-control-the-OL">Inserting images into a RichTextBox control (the OLE way) - CodeProject</a>


