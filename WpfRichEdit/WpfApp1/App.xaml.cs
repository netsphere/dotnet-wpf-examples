﻿
using System.Windows;
using System.Windows.Input;

namespace WpfApp1
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
public partial class MyApp : Application
{
    void Application_Startup(object sender, StartupEventArgs e)
    {
        var wnd = new MainWindow();
        wnd.Show();
    }
}


static class MyCommands 
{
    public static readonly RoutedUICommand InsertOleObject =
                new RoutedUICommand("Insert OLE object", nameof(InsertOleObject), typeof(MyCommands));

}

}
