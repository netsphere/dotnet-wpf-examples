
using Microsoft.Win32;
using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Windows;
using System.Windows.Input;
using Vanara.PInvoke;
using WindowsFormsControlLibrary1;
using static Vanara.PInvoke.Ole32;
using static Vanara.PInvoke.OleDlg;
using static Vanara.PInvoke.STGM;


namespace WpfApp1
{

static class NativeMethods
{
    // `OleUIPasteSpecialW()` 関数でも使われる.
    // Vanara 内に定義なし. これらの定数は <OleDlg.h> 内で定義されている.
    public enum OLEUI {
        //OLEUI_SUCCESS = 1,     // No error, same as OLEUI_OK
        OLEUI_OK      = 1,     // OK button pressed
        OLEUI_CANCEL  = 2,     // Cancel button pressed
    }

    // Vanara.PInvoke.Shared パッケージで定義される:
    //const uint STGM_READWRITE = 0x2;
    //const uint STGM_CREATE = 0x1000;
    //const uint STGM_SHARE_EXCLUSIVE = 0x10;
}


    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
public partial class MainWindow : Window
{
    //IRichEditOle _richEditOle;

    // コンストラクタ
    public MainWindow()
    {
        InitializeComponent();

        CommandBindings.Add(
                    new CommandBinding(ApplicationCommands.Open, onFileOpen));
        CommandBindings.Add(
                    new CommandBinding(MyCommands.InsertOleObject, onInsertOleObject));
    }


    static Guid IID_IOleObject = new Guid("{00000112-0000-0000-C000-000000000046}");

    // @return 失敗したとき false
    // Note. ほぼ、ここに書いてあるとおり (C#):
    //        - https://lets-csharp.com/richtextbox-bmp-insert/
    //          RichTextBoxにBitmapを挿入する方法
    //       C++ での手順
    //        - https://learn.microsoft.com/en-us/windows/win32/controls/using-rich-edit-com
    //          How to Use OLE in Rich Edit Controls
    static bool insertObjectOrLink(IRichEditOle richEditOle,
                            OLEUIINSERTOBJECT uiInsertObj)
    {
        ILockBytes pLockBytes = null;
        HRESULT hr = CreateILockBytesOnHGlobal(IntPtr.Zero, // hGlobal
                                               true,        // fDeleteOnRelease
                                               out pLockBytes);
        if (hr.Failed)
            return false;

        IStorage pStorage = null;
        hr = StgCreateDocfileOnILockBytes(pLockBytes,
                        (STGM_SHARE_EXCLUSIVE | STGM_CREATE | STGM_READWRITE),
                        0,
                        out pStorage);
        if (hr.Failed) {
            Marshal.ReleaseComObject(pLockBytes);
            return false;
        }

        FORMATETC formatEtc = new FORMATETC() {
                    cfFormat = 0,
                    ptd = IntPtr.Zero,
                    dwAspect = DVASPECT.DVASPECT_CONTENT,
                    lindex = -1,
                    tymed = TYMED.TYMED_NULL };

        IOleClientSite pOleClientSite;
        hr = richEditOle.GetClientSite(out pOleClientSite);
        if (hr.Failed) {
            Marshal.ReleaseComObject(pLockBytes);
            Marshal.ReleaseComObject(pStorage);
            return false;
        }

        object ppvObj = null;

        switch (insertionType(uiInsertObj))
        {
        case InsertionType.CREATE_NEW_ITEM:
            string progId;
            hr = ProgIDFromCLSID(uiInsertObj.clsid, out progId);
            if (hr.Failed) {
                MessageBox.Show("ProgID not found. clsid = " + uiInsertObj.clsid);
                return false;
            }

            hr = OleCreate(uiInsertObj.clsid,
                           IID_IOleObject,
                           OLERENDER.OLERENDER_DRAW, // render,
                           formatEtc,
                           pOleClientSite,
                           pStorage,
                           out ppvObj);
            if ( hr.Failed) {
                Marshal.ReleaseComObject(pLockBytes);
                Marshal.ReleaseComObject(pStorage);
                Marshal.ReleaseComObject(pOleClientSite);
                return false;
            }
            break;

        case InsertionType.CREATE_FROM_FILE_COPY:
            hr = OleCreateFromFile(Guid.Empty, // reserved. Must be `CLSID_NULL`
                                   uiInsertObj.lpszFile,
                                   IID_IOleObject, // 通常は `IID_IOleObject`
                                   OLERENDER.OLERENDER_DRAW,
                                   formatEtc,
                                   pOleClientSite, pStorage,
                                   out ppvObj);
            break;

        case InsertionType.LINK_TO_FILE:
            hr = OleCreateLinkToFile(uiInsertObj.lpszFile,
                                     IID_IOleObject, // 通常は `IID_IOleObject`
                                     OLERENDER.OLERENDER_DRAW,
                                     formatEtc,
                                     pOleClientSite, pStorage,
                                     out ppvObj);
            break;
        }

        IOleObject oleObject = (IOleObject) ppvObj;
        Guid guid;
        oleObject.GetUserClassID(out guid);

        // やっと埋め込む!
        REOBJECT reoObject = new REOBJECT() {
                    cp = REOBJECT.REO_CP_SELECTION, // position を指定しなくてよい
                    clsid = guid,
                    pstg = pStorage,
                    poleobj = oleObject,
                    polesite = pOleClientSite,
                    dvaspect = (uint)DVASPECT.DVASPECT_CONTENT,
                    dwFlags = (REOBJECT.REO_RESIZABLE | REOBJECT.REO_BELOWBASELINE),
                    dwUser = 0 };
        hr = richEditOle.InsertObject(reoObject);

        // 後片付け
        Marshal.ReleaseComObject(pLockBytes);
        Marshal.ReleaseComObject(pStorage);
        Marshal.ReleaseComObject(pOleClientSite);
        Marshal.ReleaseComObject(oleObject);

        return hr.Failed ? false : true;
    }


    enum InsertionType {
        LINK_TO_FILE = 1,
        CREATE_FROM_FILE_COPY = 2,
        CREATE_NEW_ITEM = 3,
    }

    static InsertionType insertionType(OLEUIINSERTOBJECT uiInsertObj)
    {
        if ((uiInsertObj.dwFlags & IOF.IOF_SELECTCREATEFROMFILE) != 0) {
            if ( (uiInsertObj.dwFlags & IOF.IOF_CHECKLINK) != 0 )
                return InsertionType.LINK_TO_FILE;
            else
                return InsertionType.CREATE_FROM_FILE_COPY;
        }
        if ((uiInsertObj.dwFlags &
             (IOF.IOF_SELECTCREATENEW | IOF.IOF_SELECTCREATECONTROL)) != 0)
            return InsertionType.CREATE_NEW_ITEM;

        throw new ArgumentException(nameof(uiInsertObj));
    }


    /// ///////////////////////////////////////////////////////////////////
    // Command Handlers

    // mfc: `ID_OLE_INSERT_NEW` コマンド. 自分で実装が必要。
    //      See https://learn.microsoft.com/en-us/cpp/mfc/tn022-standard-commands-implementation
    void onInsertOleObject(object sender, ExecutedRoutedEventArgs e)
    {
        // 最終的に IRichEditOle::InsertObject() に流し込めばよい.

        var wih = new System.Windows.Interop.WindowInteropHelper(this);

        OLEUIINSERTOBJECT uiInsertObj = new OLEUIINSERTOBJECT() {
                cbStruct = (uint) Marshal.SizeOf(typeof(OLEUIINSERTOBJECT)),
                dwFlags = IOF.IOF_SELECTCREATENEW,
                hWndOwner = wih.Handle,
                lpszFile = new string(' ', 256) };
        uiInsertObj.cchFile = (uint) uiInsertObj.lpszFile.Length;

        // 標準の [オブジェクトの挿入] ダイアログを表示
        // 戻り値: OLEUI_OK, OLEUI_SUCCESS = 成功, OLEUI_CANCEL = 中止
        var ret = (NativeMethods.OLEUI) OleUIInsertObject(ref uiInsertObj);
        if (ret != NativeMethods.OLEUI.OLEUI_OK)
            return;
/*
  `IOF_CREATENEWOBJECT` フラグを与えると, ダイアログの中でオブジェクトまで
  つくってくれる。なんですと?
    - IOF_SELECTCREATENEW|IOF_SELECTCREATECONTROL -> OleCreate()
    - IOF_SELECTCREATEFROMFILE ->
        + コピーを埋め込み -> OleCreateFromFile()
        + LinkToFile       -> OleCreateLinkToFile()

  しかし, 実際に呼び出されている例が、まったく見当たらない。
*/
        ExRichEditBox richEdit = (ExRichEditBox) formsHost.Child;
        int position = richEdit.SelectionStart;
        var richEditOle = richEdit.GetRichEditOle();

        insertObjectOrLink(richEditOle, uiInsertObj);
    }


    void onFileOpen(object sender, ExecutedRoutedEventArgs e)
    {
        var ofd = new OpenFileDialog() { Filter = "RichText files (*.rtf)|*.rtf|All files (*.*)|*.*" };
        if (ofd.ShowDialog() == true) {
            var textBox = (ExRichEditBox) formsHost.Child;
            textBox.LoadFile(ofd.FileName);
        }
    }


    /// ///////////////////////////////////////////////////////////////////
    // Event Handlers

    // Event `Loaded`
    void Window_Loaded(object sender, RoutedEventArgs e)
    {
        //UserRichEditControl x = new UserRichEditControl();
        ExRichEditBox x = new ExRichEditBox();
        formsHost.Child = x;
    }
}

}
